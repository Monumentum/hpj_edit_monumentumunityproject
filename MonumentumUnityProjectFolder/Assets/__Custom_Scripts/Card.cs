﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Card : MonoBehaviour 
{
	public enum CardState{Hand, Table, Cooldown, Recharge}; //the positions a card can be in
	public enum CardType{Familiar, Minion, Action, Trap, VoidBreacher}; //what type of card it is
	public enum CardDominion{Irotar,Vosira,Rhavik}; //what dominion the card is

	public CardState cardState;
	public CardType cardType;
	public GameObject summonedGameObject;
	public int sourceStoneCost;
	public string cardName;
	public CardDominion cardDominion;
	public int playerNumber;
	public int handNumber;
    public Characterhpj mTargetedCharacterhpj;
	public PlayerHandManager handManager;
	public PlayerClickController handClickController;

	public string mCardDescription = "";

	public Texture2D mCardArt;

	public string mCardIDCode = "";

	//Icon that appears in the top-left of the screen for showing whose turn it is.  
	//Assign in the prefab, but only if this card is for a VoidBreacher, otherwise leave it null.
	public Texture2D[] mVoidBreacherIcon;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(cardState != CardState.Table)
		{
			mTargetedCharacterhpj = null;
		}

	}

	public virtual void CardEffect()
	{
		Debug.Log("Doing effect of " + cardName);
		cardState = CardState.Cooldown;
		handManager.playingActionCard = false;
		handManager.actionCardToPlay = null;
	}

	public virtual void CardTarget()
	{
		
	}

	public void TargetAllFriendlyCreaturehpjs()
	{
		Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();

		foreach (Characterhpj characterhpj in characterhpjs)
		{
			if ((characterhpj.controllingPlayer == this.playerNumber) && characterhpj.creaturehpj)
			{
				characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
			}
		}
	}

    public void TargetAllEnemyCreaturehpjs()
    {
        Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();

        foreach (Characterhpj characterhpj in characterhpjs)
        {
            if ((characterhpj.controllingPlayer != this.playerNumber) && characterhpj.creaturehpj)
            {
                characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
            }
        }
    }

    public void TargetAllCreaturehpjsInLOS(int range)
    {
        //Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();

        //for (int i = 1; i <= range; i++)
        //{
        //    foreach (Characterhpj characterhpj in characterhpjs)
        //    {
        //        if ((characterhpj.controllingPlayer != this.playerNumber) && characterhpj.creaturehpj)
        //        {
        //            if (Vector3.Distance(characterhpj.currentSpace.gameObject.transform.position, mTargetedCharacterhpj.currentSpace.gameObject.transform.position) == i)
        //            {
        //                characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
        //            }
        //        }
        //    }
        //}

        for (int i = 1; i <= range; i++)
        {
            //highlight adjacent squares
            if (i == 1)
            {
                //Debug.Log("Highlighting the first set of squares");
                foreach (BoardSquare potentialTarget in mTargetedCharacterhpj.currentSpace.GetComponent<BoardSquare>().neighborSquares)
                {
                    potentialTarget.hasAttackableTarget = true;
                }
            }
            //Highlight other squares in range
            else
            {
                //Debug.Log("Highlighting another set of squares");
                BoardSquare[] highlightedTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
                List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
                foreach (BoardSquare newCombatTarget in highlightedTargets)//Add any already highlighted squares to the list
                {
                    if (newCombatTarget.hasAttackableTarget == true)
                    {
                        highlightedSquareList.Add(newCombatTarget);
                    }
                }
                foreach (BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
                {
                    foreach (BoardSquare potentialTarget in listedSqaure.neighborSquares)
                    {
                        if (listedSqaure.occupier == null ||
                            (listedSqaure.occupier.GetComponent<Characterhpj>() != null && listedSqaure.occupier.GetComponent<Characterhpj>().controllingPlayer == mTargetedCharacterhpj.controllingPlayer))
                        {
                            if (Vector3.Distance(potentialTarget.gameObject.transform.position, mTargetedCharacterhpj.currentSpace.gameObject.transform.position) == i)
                            {
                                potentialTarget.hasAttackableTarget = true;
                            }
                        }
                    }
                }

            }
            //unhighlight unoccupied/friendly squares in range
            if (i == range)
            {
                BoardSquare[] targetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
                foreach (BoardSquare invalidTarget in targetToUnHighlight)//Unhighlight any squares that are Highlighted
                {
                    if (invalidTarget.occupier == null || (invalidTarget.occupier.GetComponent<Characterhpj>() != null && invalidTarget.occupier.GetComponent<Characterhpj>().tag == "VoidBreacher"))
                    {
                        invalidTarget.hasAttackableTarget = false;
                    }
                }

            }
        }
            
    }

	public void UnTargetAllSpaces()
	{
		Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
		
		foreach (Characterhpj characterhpj in characterhpjs)
		{
			characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
			characterhpj.currentSpace.GetComponent<BoardSquare>().hasAttackableTarget = false;
			characterhpj.currentSpace.GetComponent<BoardSquare>().validSummonSpot = false;
		}

        BoardSquare[] squares = FindObjectsOfType<BoardSquare>();

        foreach (BoardSquare square in squares)
        {
            square.GetComponent<BoardSquare>().inMovementRange = false;
            square.GetComponent<BoardSquare>().hasAttackableTarget = false;
            square.GetComponent<BoardSquare>().validSummonSpot = false;
        }
	}

	public void SummonRune()
	{
		GameObject summonedRune = (GameObject)Network.Instantiate(summonedGameObject, Vector3.zero, Quaternion.identity,0);
		
		summonedRune.GetComponent<Rune>().EquipRune(handClickController.selectedCharacterhpj.GetComponent<NetworkView>().viewID);
		summonedRune.GetComponent<Rune>().mAssociatedCard = this;
		handManager.playingActionCard = false;
		handManager.actionCardToPlay = null;
	}

    public void PlaceTrap()
    {
        Vector3 trapPosition = handClickController.mClickedSquare.transform.position + new Vector3(0, 0.5f, 0);
        GameObject setTrap = (GameObject)Network.Instantiate(summonedGameObject, trapPosition, Quaternion.identity, 0);
        Trap trap = setTrap.GetComponent<Trap>();
        //trap.mOccupiedSquare = handClickController.mClickedSquare;
        trap.mAssociatedCard = this;
        //trap.mControllingPlayer = this.playerNumber;
		trap.GetComponent<NetworkView>().RPC("SetTrap", RPCMode.All, this.playerNumber, cardName, mCardDescription);
        handManager.playingActionCard = false;
        handManager.mTargetting = false;
        handManager.mActivatingActionCard = false;
        handManager.actionCardToPlay = null;
    }

    public virtual void TargetAcquired()
    {

    }

    [RPC] public virtual void ActivateOnTarget()
    {

    }

	public void HighlightSquaresOnlyinRangeofCharacterhpj(Characterhpj newNeighbor, int range)
	{
		for (int i = 1; i <= range; i++)
		{
			//highlight adjacent squares
			if (i == 1)
			{
				//Debug.Log("Highlighting the first set of squares");
				foreach (BoardSquare potentialTarget in newNeighbor.currentSpace.GetComponent<BoardSquare>().neighborSquares)
				{
					potentialTarget.inMovementRange = true;
					potentialTarget.mDistanceFromMover = i;
				}
			}
			//Highlight other squares in range
			else
			{
				//Debug.Log("Highlighting another set of squares");
				BoardSquare[] highlightedTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
				foreach (BoardSquare newCombatTarget in highlightedTargets)//Add any already highlighted squares to the list
				{
					if (newCombatTarget.inMovementRange == true)
					{
						highlightedSquareList.Add(newCombatTarget);
					}
				}
				foreach (BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
				{
					foreach (BoardSquare potentialTarget in listedSqaure.neighborSquares)
					{
						if (listedSqaure.occupier == null ||
						    (listedSqaure.occupier.GetComponent<Characterhpj>() != null && listedSqaure.occupier.GetComponent<Characterhpj>().controllingPlayer == mTargetedCharacterhpj.controllingPlayer))
						{
							potentialTarget.inMovementRange = true;
							if (potentialTarget.mDistanceFromMover == 99)
							{
								potentialTarget.mDistanceFromMover = i;
							}
						}
					}
				}
				
			}
			//unhighlight occupied squares in range
			if (i == range)
			{
				BoardSquare[] targetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				foreach (BoardSquare invalidTarget in targetToUnHighlight)//Unhighlight any squares that are Highlighted
				{
					if (invalidTarget.occupier != null)
					{
						invalidTarget.inMovementRange = false;
					}
				}
				
			}
		}
	}//END of HighlightSquaresOnlyinRangeofCharacterhpj()


    public void HighlightSquaresOnlyinLOSofCharacterhpj(Characterhpj newNeighbor, int range)
    {
        for (int i = 1; i <= range; i++)
        {
            //highlight adjacent squares
            if (i == 1)
            {
                //Debug.Log("Highlighting the first set of squares");
                foreach (BoardSquare potentialTarget in newNeighbor.currentSpace.GetComponent<BoardSquare>().neighborSquares)
                {
                    potentialTarget.hasAttackableTarget = true;
                }
            }
            //Highlight other squares in range
            else
            {
                //Debug.Log("Highlighting another set of squares");
                BoardSquare[] highlightedTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
                List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
                foreach (BoardSquare newCombatTarget in highlightedTargets)//Add any already highlighted squares to the list
                {
                    if (newCombatTarget.hasAttackableTarget == true)
                    {
                        highlightedSquareList.Add(newCombatTarget);
                    }
                }
                foreach (BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
                {
                    foreach (BoardSquare potentialTarget in listedSqaure.neighborSquares)
                    {
                        if (listedSqaure.occupier == null ||
                            (listedSqaure.occupier.GetComponent<Characterhpj>() != null && listedSqaure.occupier.GetComponent<Characterhpj>().controllingPlayer == mTargetedCharacterhpj.controllingPlayer))
                        {
                            if (Vector3.Distance(potentialTarget.gameObject.transform.position, mTargetedCharacterhpj.currentSpace.gameObject.transform.position) == i)
                            {
                                potentialTarget.hasAttackableTarget = true;
                            }
                        }
                    }
                }

            }
            //unhighlight occupied squares in range
            if (i == range)
            {
                BoardSquare[] targetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
                foreach (BoardSquare invalidTarget in targetToUnHighlight)//Unhighlight any squares that are Highlighted
                {
                    if (invalidTarget.occupier != null)
                    {
                        invalidTarget.hasAttackableTarget = false;
                    }
                }

            }
        }
	}//END of HighlightSquaresOnlyinLOSofCharacterhpj()


	public void HighlightSquaresThroughEnemiesFromCharacterhpj(Characterhpj newNeighbor, int range)
	{
		for (int i = 1; i <= range; i++)
		{
			//highlight adjacent squares
			if (i == 1)
			{
				//Debug.Log("Highlighting the first set of squares");
				foreach (BoardSquare potentialTarget in newNeighbor.currentSpace.GetComponent<BoardSquare>().neighborSquares)
				{
					potentialTarget.inMovementRange = true;
					potentialTarget.mDistanceFromMover = i;
				}
			}
			//Highlight other squares in range
			else
			{
				//Debug.Log("Highlighting another set of squares");
				BoardSquare[] highlightedTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
				foreach (BoardSquare newCombatTarget in highlightedTargets)//Add any already highlighted squares to the list
				{
					if (newCombatTarget.inMovementRange == true)
					{
						highlightedSquareList.Add(newCombatTarget);
					}
				}
				foreach (BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
				{
					foreach (BoardSquare potentialTarget in listedSqaure.neighborSquares)
					{
						if (listedSqaure.occupier == null ||
						    (listedSqaure.occupier.GetComponent<Characterhpj>() != null))
						{
							potentialTarget.inMovementRange = true;
							if (potentialTarget.mDistanceFromMover == 99)
							{
								potentialTarget.mDistanceFromMover = i;
							}

						}
					}
				}
				
			}
			//unhighlight occupied squares in range
			if (i == range)
			{
				BoardSquare[] targetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				foreach (BoardSquare invalidTarget in targetToUnHighlight)//Unhighlight any squares that are Highlighted
				{
					if (invalidTarget.occupier != null)
					{
						invalidTarget.inMovementRange = false;
					}
				}
				
			}
		}
	}//END of HighlightSquaresThroughEnemiesFromCharacterhpj()


	public void HighlightSquaresBetweenCharacterhpjs(Characterhpj originCharacterhpj, Characterhpj furtherCharacterhpj, int range)
	{

		List<BoardSquare> squaresBetweenList = new List<BoardSquare>();

		//Highlight squares around the originCharacterhpj
		for (int i = 1; i <= range; i++)
		{
			//highlight adjacent squares
			if (i == 1)
			{
				//Debug.Log("Highlighting the first set of squares");
				foreach (BoardSquare potentialTarget in originCharacterhpj.currentSpace.GetComponent<BoardSquare>().neighborSquares)
				{
					potentialTarget.hasAttackableTarget = true;
				}
			}
			//Highlight other squares in range
			else
			{
				//Debug.Log("Highlighting another set of squares");
				BoardSquare[] highlightedTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
				foreach (BoardSquare newCombatTarget in highlightedTargets)//Add any already highlighted squares to the list
				{
					if (newCombatTarget.hasAttackableTarget == true)
					{
						highlightedSquareList.Add(newCombatTarget);
					}
				}
				foreach (BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
				{
					foreach (BoardSquare potentialTarget in listedSqaure.neighborSquares)
					{
						if (listedSqaure.occupier == null ||
						    (listedSqaure.occupier.GetComponent<Characterhpj>() != null && listedSqaure.occupier.GetComponent<Characterhpj>().controllingPlayer == mTargetedCharacterhpj.controllingPlayer))
						{
							if (Vector3.Distance(potentialTarget.gameObject.transform.position, mTargetedCharacterhpj.currentSpace.gameObject.transform.position) == i)
							{
								potentialTarget.hasAttackableTarget = true;
							}
						}
					}
				}
				
			}
			//unhighlight occupied squares in range
			if (i == range)
			{
				BoardSquare[] targetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				foreach (BoardSquare invalidTarget in targetToUnHighlight)//Unhighlight any squares that are Highlighted
				{
					if (invalidTarget.occupier != null)
					{
						invalidTarget.hasAttackableTarget = false;
					}
				}
				
			}
		}//End of Highlighting squares around the originCharacterhpj

		//Add the highlighted squares to the squaresBetweenList
		BoardSquare[] targetToAddBetween = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
		foreach (BoardSquare betweenSquare in targetToAddBetween)//Unhighlight any squares that are Highlighted
		{
			if (betweenSquare.hasAttackableTarget)
			{
				squaresBetweenList.Add(betweenSquare);
			}
			betweenSquare.hasAttackableTarget = false;
		}


		//Highlight squares around the furtherCharacterhpj
		for (int i = 1; i <= range; i++)
		{
			//highlight adjacent squares
			if (i == 1)
			{
				//Debug.Log("Highlighting the first set of squares");
				foreach (BoardSquare potentialTargetFurther in furtherCharacterhpj.currentSpace.GetComponent<BoardSquare>().neighborSquares)
				{
					potentialTargetFurther.hasAttackableTarget = true;
				}
			}
			//Highlight other squares in range
			else
			{
				//Debug.Log("Highlighting another set of squares");
				BoardSquare[] highlightedTargetsFurther = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				List<BoardSquare> highlightedSquareListFurther = new List<BoardSquare>();//Make a list to contain squares
				foreach (BoardSquare newCombatTarget in highlightedTargetsFurther)//Add any already highlighted squares to the list
				{
					if (newCombatTarget.hasAttackableTarget == true)
					{
						highlightedSquareListFurther.Add(newCombatTarget);
					}
				}
				foreach (BoardSquare listedSqaure in highlightedSquareListFurther)//Use the list to highlight suqares adjacent to the ones already highlighted
				{
					foreach (BoardSquare potentialTargetFurther in listedSqaure.neighborSquares)
					{
						if (listedSqaure.occupier == null ||
						    (listedSqaure.occupier.GetComponent<Characterhpj>() != null && listedSqaure.occupier.GetComponent<Characterhpj>().controllingPlayer == mTargetedCharacterhpj.controllingPlayer))
						{
							if (Vector3.Distance(potentialTargetFurther.gameObject.transform.position, furtherCharacterhpj.currentSpace.gameObject.transform.position) == i)
							{
								potentialTargetFurther.hasAttackableTarget = true;
							}
						}
					}
				}
				
			}
			//unhighlight occupied squares in range
			if (i == range)
			{
				BoardSquare[] targetToUnHighlightFurther = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				foreach (BoardSquare invalidTargetFurther in targetToUnHighlightFurther)//Unhighlight any squares that are Highlighted
				{
					if (invalidTargetFurther.occupier != null)
					{
						invalidTargetFurther.hasAttackableTarget = false;
					}
				}
				
			}
		}//End of Highlighting squares around the furtherCharacterhpj

		//Unhighlight the squares that don't overlap
		foreach (BoardSquare betweenSquare in targetToAddBetween)//Unhighlight any squares that are Highlighted
		{
			if (!squaresBetweenList.Contains(betweenSquare))
			{
				betweenSquare.hasAttackableTarget = false;
			}
		}
	}//END of HighlightSquaresBetweenCharacterhpjs()

    public virtual void BattleEffect(Characterhpj targetedCharacterhpj)
    {

    }

    public virtual void AfterBattleEffect()
    {

    }
}
