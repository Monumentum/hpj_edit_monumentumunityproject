﻿using UnityEngine;
using System.Collections;

public class Monument : MonoBehaviour {


    public BoardSquare mOccupiedSquare;
    public int mAttacksLeftToWithstand = 3;
    //public PlayerClickController mOwner;
    public int mOwner;


	MouseOverInfoManager mMouseOverInfoManager;

	// Use this for initialization
	void Start () 
    {
		mMouseOverInfoManager = FindObjectOfType(typeof(MouseOverInfoManager)) as MouseOverInfoManager;

	}
	
	// Update is called once per frame
	void Update () 
	{
	    if (mOccupiedSquare)
        {
            foreach (BoardSquare square in mOccupiedSquare.neighborSquares)
            {
                if (square.occupier)
                {
                    Characterhpj attacker = square.occupier.GetComponent<Characterhpj>();
                    if (!attacker.lockedInCombat)
                    {
                        attacker.mCanAttackMonument = true;
                        attacker.mAttackableMonument = this;
                    }
                    else
                    {
                        attacker.mCanAttackMonument = false;
                        attacker.mAttackableMonument = null;
                    }
                }
            }
        }

        if (mAttacksLeftToWithstand <= 0)
        {
            //mOwner.mRemainingMonuments -= 1;
            //mOwner.networkView.RPC("UpdateMonumentCount", RPCMode.All, this);
            PlayerClickController aController = FindObjectsOfType<PlayerClickController>()[0];
            aController.GetComponent<NetworkView>().RPC("UpdateMonumentCount", RPCMode.All, this.name);
        }
	}//END of Update()

	void OnMouseEnter()
	{
		mMouseOverInfoManager.SetCharacterhpjToolTip(true, "Player " + mOwner + "'s monument\n" + mAttacksLeftToWithstand + " hits remaining");
	}//END of OnMouseEnter()
	
	void OnMouseExit()
	{
		mMouseOverInfoManager.SetCharacterhpjToolTip(false, "");		
	}//END of OnMouseExit()

}
