﻿using UnityEngine;
using System.Collections;

//Characterhpj description: Retaliate: After this characterhpj is attacked in melee, target the attacking characterhpj. This characterhpj must attack the target.

public class Behemoth : Characterhpj 
{
    public override void Start()
    {
        base.Start();

        if (sourceStoneCost == 0)
        {
            sourceStoneCost = 6;
        }

        if (strength == 0)
        {
            strength = 6;
        }

        if (fortitude == 0)
        {
            fortitude = 12;
        }

        characterhpjDominion = CharacterhpjDominion.Rhavik;
    }

	//Override the TakeDamage function to counter-attack
    [RPC] public override void TakeDamage(NetworkViewID whoHitMe, bool didHit = true) //Overrides to retaliate against characterhpjs in melee when damaged
	{
        base.TakeDamage(whoHitMe, didHit);

		if (whoHitMe!=null && hitPoints > 0 && Vector3.Distance(this.transform.position, NetworkView.Find(whoHitMe).observed.transform.position) == 1 && GetComponent<NetworkView>().isMine)
		{	
			if (NetworkView.Find(whoHitMe).observed.GetComponent<Characterhpj>().tag != "VoidBreacher")
			{
				Debug.Log( name + " Retaliated against " + NetworkView.Find(whoHitMe).observed.name);
				AttackTarget(NetworkView.Find(whoHitMe).observed.GetComponent<Characterhpj>());//RetaliateEffect
			}
		}
	}
}
