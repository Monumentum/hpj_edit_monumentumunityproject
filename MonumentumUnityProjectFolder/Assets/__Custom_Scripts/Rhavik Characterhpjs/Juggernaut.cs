﻿using UnityEngine;
using System.Collections;

public class Juggernaut : Characterhpj
{
    public override void Start()
    {
        base.Start();

        if (hitPoints == 2)
        {
            hitPoints = 3;
        }

        if (sourceStoneCost == 0)
        {
            sourceStoneCost = 6;
        }

        if (strength == 0)
        {
            strength = 6;
        }

        if (fortitude == 0)
        {
            fortitude = 12;
        }

        characterhpjDominion = CharacterhpjDominion.Rhavik;
    }
	//Only thing special about this guy is the extra Hit Points
}
