﻿using UnityEngine;
using System.Collections;

public class FlintDross : Characterhpj 
{
    public int mBoostAmount = 1;
	//Needs code for buffing any famliars with Runes on them    
    public override void Start()
    {
        base.Start();

        characterhpjDominion = CharacterhpjDominion.Rhavik;
    }

//    public override void BeginCombatPhase()
//    {
//		ApplyFlintRuneBoost();
//        base.BeginCombatPhase();
//    }


	public void ApplyFlintRuneBoost ()
	{
		Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
		string boostName = "FlintRuneBoost";
		
		foreach (Characterhpj characterhpj in characterhpjs)
		{
			boostName = "FlintRuneBoost";
			if ((characterhpj.controllingPlayer == this.controllingPlayer) && characterhpj.creaturehpj && GetComponent<NetworkView>().isMine)
			{
				//Make it so the boost name is based on the number of runes, so that each new rune is a new boost
				boostName += characterhpj.mNumberOfRunes.ToString();

				if (!characterhpj.mBoosts.Contains(boostName) && characterhpj.mNumberOfRunes > 0)
				{
					characterhpj.UpdateStrength(mBoostAmount);
					characterhpj.mBoosts.Add(boostName);
					characterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, characterhpj.strength, characterhpj.fortitude);
				}
				//I don't think this part will ever get called because Characterhpj's can't lose Runes ~Adam
				else if (characterhpj.mBoosts.Contains(boostName) && characterhpj.mNumberOfRunes <= 0)
				{
					characterhpj.UpdateStrength(-mBoostAmount);
					characterhpj.mBoosts.Remove(boostName);
					characterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, characterhpj.strength, characterhpj.fortitude);
				}
			}
		}

	}
}
