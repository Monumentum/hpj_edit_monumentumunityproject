﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Serialization.JsonFx;

namespace PlayFab.MonumentumIntegration
{
	public class CardExpansionChecker : MonoBehaviour 
	{
//		public bool mHasFlintExpansionCards = false;
//		public bool mHasRippleExpansionCards = false;
//		public bool mHasLarkExpansionCards = false;
		private  List<ItemInstance> mPlayerInventory;

		
		// Use this for initialization
		void Start () 
		{
			if (PlayFabData.AuthKey != null)
			{
				PlayFabClientAPI.GetUserInventory (new GetUserInventoryRequest(),OnGetUserInventory, OnPlayFabError);
			}
		}
		
		// Update is called once per frame
		void Update () 
		{
		
		}


		private void OnGetUserInventory(GetUserInventoryResult result)
		{
			mPlayerInventory = result.Inventory;
			bool addedRippleExpansion = false;
			bool addedLarkExpansion = false;
			bool addedFlintExpansion = false;

			for (int i = 0; i<mPlayerInventory.Count; i++)
			{
				switch(mPlayerInventory[i].ItemId)
				{
				case "irotar_pack_1":
					if (!addedRippleExpansion)
					{
						foreach(GameObject rippleExpansionCard in this.GetComponent<DeckBuilder>().mRippleExpansionCards)
						{
							this.GetComponent<DeckBuilder>().mAllAvailableCards.Add(rippleExpansionCard);
						}
						addedRippleExpansion = true;
					}
					break;
				case "vosira_pack_1":
					if(!addedLarkExpansion)
					{
						foreach(GameObject larkExpansionCard in this.GetComponent<DeckBuilder>().mLarkExpansionCards)
						{
							this.GetComponent<DeckBuilder>().mAllAvailableCards.Add(larkExpansionCard);
						}
						addedLarkExpansion = true;
					}
					break;
				case "rhavik_pack_1":
					if(!addedFlintExpansion)
					{
						foreach(GameObject flintExpansionCard in this.GetComponent<DeckBuilder>().mFlintExpansionCards)
						{
							this.GetComponent<DeckBuilder>().mAllAvailableCards.Add(flintExpansionCard);
						}
						addedFlintExpansion = true;
					}
					break;
				default:
					break;
				}
			}
		}
		void OnPlayFabError(PlayFabError error)
		{
			Debug.Log ("Got an error: " + error.ErrorMessage);
		}

	}
}
