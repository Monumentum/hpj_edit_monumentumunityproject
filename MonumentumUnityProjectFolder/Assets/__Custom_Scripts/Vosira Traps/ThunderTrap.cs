﻿using UnityEngine;
using System.Collections;

//Trap: When activated by target enemy characterhpj, target suffers -2F.
public class ThunderTrap : Trap 
{
    public int mDecreaseAmount = -2;
	// Use this for initialization



    public override void ActivateTrap(Characterhpj victim)
    {
		if (GetComponent<NetworkView>().isMine)
		{
	        victim.ModifyFortitude(mDecreaseAmount);
	        victim.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.All, victim.strength, victim.fortitude);

	        this.mAssociatedCard.cardState = Card.CardState.Cooldown;
		}

        Destroy(this.gameObject);
    }

    //void OnTriggerEnter(Collider other)
    //{
    //    Characterhpj characterhpj = other.GetComponent<Characterhpj>();
    //    if (characterhpj && !characterhpj.networkView.isMine)
    //    {
    //        if (characterhpj.controllingPlayer != this.mControllingPlayer)
    //        {
    //            ActivateTrap(characterhpj);
    //        }
    //    }
    //}
}
