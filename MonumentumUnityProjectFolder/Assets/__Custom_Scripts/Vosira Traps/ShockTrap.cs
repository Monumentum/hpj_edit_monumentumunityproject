﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Trap: When activated by target enemy characterhpj, all enemy characterhpjs in AOE(2) suffer a 6S strike.
public class ShockTrap : Trap 
{
    public int mAOERange = 2;
    public int mAOEDamage = 6;
	// Use this for initialization

    public override void ActivateTrap(Characterhpj victim)
    {
		if (GetComponent<NetworkView>().isMine)
		{
	        List<Characterhpj> aoeTargets = new List<Characterhpj>();

	        for (int i = 1; i<= mAOERange; i++)
	        {
	            if (i == 1)
	            {
	                foreach (BoardSquare potentialCombatTarget in victim.currentSpace.GetComponent<BoardSquare>().neighborSquares)
	                {
	                    potentialCombatTarget.hasAttackableTarget = true;
	                }
	            }
	            else
	            {
	                BoardSquare[] highlightedCombatTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
	                List<BoardSquare> highlightedSquareList = new List<BoardSquare>();
	                foreach (BoardSquare newCombatTarget in highlightedCombatTargets)
	                {
	                    if (newCombatTarget.hasAttackableTarget == true)
	                    {
	                        highlightedSquareList.Add(newCombatTarget);
	                    }
	                }
	                foreach (BoardSquare listedSqaure in highlightedSquareList)
	                {
	                    foreach (BoardSquare potentialCombatTarget in listedSqaure.neighborSquares)
	                    {
	                        if (listedSqaure.occupier == null || listedSqaure.occupier.GetComponent<Characterhpj>() != null)
	                        {
	                            if (Vector3.Distance(potentialCombatTarget.gameObject.transform.position, victim.currentSpace.gameObject.transform.position) == i)
	                            {
	                                potentialCombatTarget.hasAttackableTarget = true;
	                            }
	                        }
	                    }
	                }
	            }
	            if (i == mAOERange)
	            {
	                //Debug.Log("Done Highlighting squares");
	                BoardSquare[] combatTargetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
	                foreach (BoardSquare aoeCombatTarget in combatTargetToUnHighlight)//Unhighlight any squares that are Highlighted
	                {
	                    if (aoeCombatTarget.occupier != null && aoeCombatTarget.occupier.GetComponent<Characterhpj>() != null
	                        && aoeCombatTarget.occupier.GetComponent<Characterhpj>().controllingPlayer != mControllingPlayer
	                        && aoeCombatTarget.occupier.GetComponent<Characterhpj>().tag != "VoidBreacher")
	                    {
	                        aoeTargets.Add(aoeCombatTarget.occupier.GetComponent<Characterhpj>());
	                    }
	                    aoeCombatTarget.hasAttackableTarget = false;

	                }

	            }
	         }

	        foreach (Characterhpj aoeTargetedCharacterhpj in aoeTargets)
	        {
	            Characterhpj targetedCharacterhpj = aoeTargetedCharacterhpj;
	            bool didIHit = false;
	            //Roll 2d6 and add your Strength
	            int thisAttackPower = mAOEDamage;
	            int rollBonus = 0;

	            for (int i = 0; i < 2; i++)
	            {
	                int dieRoll = 0;
	                dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
	                dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));

	                if (dieRoll > rollBonus)
	                {
	                    rollBonus = dieRoll;
	                }
	            }
	            thisAttackPower += rollBonus;

	            if (targetedCharacterhpj.tag == "VoidBreacher")
	            {
	                //do nothing to a VoidBreacher
	            }
	            else if (thisAttackPower == 4 + 2)//Automatically miss if you roll a 1 on both dice
	            {
	                Debug.Log(this.name + " missed " + targetedCharacterhpj.name);
	                targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, false);
	            }
	            else if (thisAttackPower > targetedCharacterhpj.fortitude || thisAttackPower == 4 + 12) //hit if you roll a 6 on both dice and/or roll higher than the target's Fortitude
	            {
	                targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, true);
	                didIHit = true;
	                Debug.Log(this.name + " hit " + targetedCharacterhpj.name + " leaving it with " + targetedCharacterhpj.hitPoints + " HP");
	            }
	            else//Miss if you roll lower than the target's Fortitude
	            {
	                Debug.Log(this.name + " missed " + targetedCharacterhpj.name);
	                targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, false);
	            }

	            //Done attacking now

	            //if you took the target down to 0 HP, kill it and move into its space if it was adjacent
	            if (targetedCharacterhpj.hitPoints == 0)
	            {
	                targetedCharacterhpj.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All); //kill the target
	            }

	        }//End of attacking the AOE targets

	        this.mAssociatedCard.cardState = Card.CardState.Cooldown;
		}

        Destroy(this.gameObject);
    }

    //void OnTriggerEnter(Collider other)
    //{
    //    Characterhpj characterhpj = other.GetComponent<Characterhpj>();
    //    if (characterhpj && !characterhpj.networkView.isMine)
    //    {
    //        if (characterhpj.controllingPlayer != this.mControllingPlayer)
    //        {
    //            ActivateTrap(characterhpj);
    //        }
    //    }
    //}
}
