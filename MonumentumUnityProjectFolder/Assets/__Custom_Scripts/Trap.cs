﻿using UnityEngine;
using System.Collections;

public class Trap : MonoBehaviour 
{
    public BoardSquare mOccupiedSquare;
    public int mControllingPlayer;
    protected Characterhpj mVoidBreacher;
    public Card mAssociatedCard;
	MouseOverInfoManager mMouseOverInfoManager;

	string mTrapDescription = "";


	// Use this for initialization
	public virtual void Start () 
    {
		mMouseOverInfoManager = FindObjectOfType(typeof(MouseOverInfoManager)) as MouseOverInfoManager;

	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public virtual void ActivateTrap(Characterhpj victim)
    {

    }

	[RPC] public virtual void SetTrap(int myPlayerNumber, string trapName, string description)
	{
		mControllingPlayer = myPlayerNumber;
        Characterhpj[] voidBreachersInPlay = FindObjectsOfType(typeof(Characterhpj)) as Characterhpj[];
        foreach (Characterhpj voidBreacher in voidBreachersInPlay)
        {
            if (voidBreacher.tag == "VoidBreacher" && voidBreacher.controllingPlayer == mControllingPlayer)
            {
                mVoidBreacher = voidBreacher;
            }
        }
		name = trapName;
		mTrapDescription = description;
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        Characterhpj characterhpj = other.GetComponent<Characterhpj>();
        if (characterhpj)
        {
            if (characterhpj.controllingPlayer != this.mControllingPlayer)
            {
                ActivateTrap(characterhpj);
            }
        }
//        else if (characterhpj && characterhpj.networkView.isMine)
//        {
//            if (characterhpj.controllingPlayer != this.mControllingPlayer)
//            {
//                Destroy(this.gameObject);
//            }
//        }
    }

	void OnMouseEnter()
	{
		mMouseOverInfoManager.SetCharacterhpjToolTip(true, name + "\n" + mTrapDescription);
	}//END of OnMouseEnter()
	
	void OnMouseExit()
	{
		mMouseOverInfoManager.SetCharacterhpjToolTip(false, "");		
	}//END of OnMouseExit()

}
