﻿using UnityEngine;
using System.Collections;

//Target characterhpj 'breaks combat' and may move up to 2 squares.

public class CardSprint : Card 
{
	TurnPhaseManager mPhaseManager;

	// Use this for initialization
	void Start () 
	{
		cardName = "Sprint";
		mPhaseManager = FindObjectOfType(typeof(TurnPhaseManager)) as TurnPhaseManager;
	}
	
	public override void CardEffect()
	{
		mTargetedCharacterhpj = handClickController.selectedCharacterhpj;
		HighlightSquaresOnlyinRangeofCharacterhpj(mTargetedCharacterhpj, 2);
		handManager.mCurrentCard = this;
		//handManager.mTarget = mTargetedCharacterhpj;
		handManager.mActivatingActionCard = true;
		handManager.playingActionCard = false;
	}
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCreaturehpjs();
	}


	public override void ActivateOnTarget()
	{
		//mMovingCharacterhpj.transform.position = handClickController.mClickedSquare.transform.position + new Vector3(0, 0.5f, 0);
		// mMovingCharacterhpj.networkView.RPC("UpdatePosition", RPCMode.All, mMovingCharacterhpj.transform.position);
		if (mTargetedCharacterhpj.lockedInCombat && mPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
		{
			mTargetedCharacterhpj.canMove = true;
		}
		//mTargetedCharacterhpj.lockedInCombat = false;
		mTargetedCharacterhpj.GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
		mTargetedCharacterhpj.mPathSquares.Clear();
		handClickController.SetCharacterhpjMovePath(handClickController.mClickedSquare, mTargetedCharacterhpj);
		mTargetedCharacterhpj.MoveToSpace();
		handManager.mActivatingActionCard = false;
		handManager.actionCardToPlay = null;
		cardState = CardState.Cooldown;
	}

}
