﻿using UnityEngine;
using System.Collections;

//All friendly creaturehpjs gain +2S until the end of this turn.
public class FavourableWinds : Card 
{
    public TurnPhaseManager mTurnPhaseManager;
    public int mBoostAmount = 2;

    // Use this for initialization
    void Start()
    {
        mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
        cardName = "FavourableWinds";
    }

    // Update is called once per frame
    void Update()
    {
        if (mTurnPhaseManager && cardState == CardState.Table)
        {
            if (mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
            {
                OnConclusionPhase();
            }
        }
    }

    public override void CardEffect()
    {
        Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
        string boostName = cardName + "boost";

        foreach (Characterhpj characterhpj in characterhpjs)
        {
            if ((characterhpj.controllingPlayer == this.playerNumber) && characterhpj.creaturehpj)
            {

                if (!characterhpj.mBoosts.Contains(boostName))
                {

                    characterhpj.UpdateStrength(mBoostAmount);
                    characterhpj.mBoosts.Add(boostName);
                    characterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, characterhpj.strength, characterhpj.fortitude);
                }
            }
        }



        //finish up the action and send this card to the Cooldown pile
        handManager.playingActionCard = false;
        handManager.mTargetting = false;
        handManager.actionCardToPlay = null;

    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        CardEffect();
    }

    public void OnConclusionPhase()
    {
        Debug.Log("Tidal Force has reached it's Conclusion.");
        Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
        string boostName = cardName + "boost";

        foreach (Characterhpj characterhpj in characterhpjs)
        {
            if ((characterhpj.controllingPlayer == this.playerNumber) && characterhpj.creaturehpj)
            {
                Debug.Log("Tidal Force has found " + characterhpj.name);
                if (characterhpj.mBoosts.Contains(boostName))
                {
                    Debug.Log("Tidal Force is removing the boost from " + characterhpj.name);
                    characterhpj.UpdateStrength(-mBoostAmount);
                    characterhpj.mBoosts.Remove(boostName);
                    characterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, characterhpj.strength, characterhpj.fortitude);
                }
            }
        }
        cardState = CardState.Cooldown;
    }
}
