﻿using UnityEngine;
using System.Collections;

//Target creaturehpj gains an additional attack this turn.
public class MultiShot : Card 
{
    public int mExtraAttacksGiven = 1;
	// Use this for initialization
	void Start () 
    {
        cardName = "Multi-Shot";
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public override void CardEffect()
    {
        mTargetedCharacterhpj = handClickController.selectedCharacterhpj;
        mTargetedCharacterhpj.mNumberOfAttacksLeft = mTargetedCharacterhpj.mNumberOfAttacksLeft + mExtraAttacksGiven;
        handManager.mCurrentCard = this;
        handManager.playingActionCard = false;
        cardState = CardState.Cooldown;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCreaturehpjs();
    }
}
