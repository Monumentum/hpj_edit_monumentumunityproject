﻿using UnityEngine;
using System.Collections;

// Target characterhpj 'breaks combat' and may move up to 4 squares including through your opponent's characterhpjs but they cannot attack or interact with POI this turn.

public class CardPhaseToShadows : Card 
{
	TurnPhaseManager mPhaseManager;
	
	// Use this for initialization
	void Start () 
	{
		cardName = "Phase To Shadows";
		mPhaseManager = FindObjectOfType(typeof(TurnPhaseManager)) as TurnPhaseManager;
	}
	
	public override void CardEffect()
	{
		mTargetedCharacterhpj = handClickController.selectedCharacterhpj;
		HighlightSquaresThroughEnemiesFromCharacterhpj(mTargetedCharacterhpj, 4);
		handManager.mCurrentCard = this;
		//handManager.mTarget = mTargetedCharacterhpj;
		handManager.mActivatingActionCard = true;
		handManager.playingActionCard = false;
	}
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCreaturehpjs();
	}
	
	
	public override void ActivateOnTarget()
	{
		bool wasAlreadyElusive = mTargetedCharacterhpj.mElusive;
		if (mTargetedCharacterhpj.lockedInCombat && mPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
		{
			mTargetedCharacterhpj.canMove = true;
		}
		//mTargetedCharacterhpj.lockedInCombat = false;
		mTargetedCharacterhpj.GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
		mTargetedCharacterhpj.mElusive = true;
		mTargetedCharacterhpj.mPathSquares.Clear();
		handClickController.SetCharacterhpjMovePath(handClickController.mClickedSquare, mTargetedCharacterhpj);
		mTargetedCharacterhpj.MoveToSpace();
		mTargetedCharacterhpj.mNumberOfAttacksLeft = -50;
		mTargetedCharacterhpj.mElusive = wasAlreadyElusive;
		mTargetedCharacterhpj.canAttack = false;
		handManager.mActivatingActionCard = false;
		handManager.actionCardToPlay = null;
		cardState = CardState.Cooldown;
	}
	
}