﻿using UnityEngine;
using System.Collections;

//Target friendly creaturehpj gains +4S and +Range(5) until the end of this turn 
//but suffers -2F and 'crippled' until the end of your next turn.
public class PowerShot : Card 
{
    public TurnPhaseManager mTurnPhaseManager;
    public int mBoostAmount = 4;
    public int mRangeBoostAmount = 5;
    public int mFortitudeLossAmount = -2;
    public int mTurnsCharacterhpjWillBeWeakened = 1;
	// Use this for initialization
	void Start () 
    {
        mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
        cardName = "PowerShot";
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (mTurnPhaseManager && cardState == CardState.Table)
        {
            if (mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
            {
                OnConclusionPhase();
            }
        }
	}

    public override void CardEffect()
    {
        string boostName = cardName + "boost";
        mTargetedCharacterhpj = handClickController.selectedCharacterhpj;
        mTargetedCharacterhpj.UpdateStrength(mBoostAmount);
        mTargetedCharacterhpj.attackRange = mTargetedCharacterhpj.attackRange + mRangeBoostAmount;
        mTargetedCharacterhpj.mBoosts.Add(boostName);
        mTargetedCharacterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, mTargetedCharacterhpj.strength, mTargetedCharacterhpj.fortitude);
        mTargetedCharacterhpj.GetComponent<NetworkView>().RPC("BeWeakened", RPCMode.All, boostName, mFortitudeLossAmount, mTurnsCharacterhpjWillBeWeakened);
        handManager.mCurrentCard = this;
        handManager.playingActionCard = false;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCreaturehpjs();
    }

    public void OnConclusionPhase()
    {
        string boostName = cardName + "boost";
        if (mTargetedCharacterhpj.mBoosts.Contains(boostName))
        {
            mTargetedCharacterhpj.UpdateStrength(-mBoostAmount);
            mTargetedCharacterhpj.attackRange = mTargetedCharacterhpj.attackRange - mRangeBoostAmount;
            mTargetedCharacterhpj.mBoosts.Remove(boostName);
            mTargetedCharacterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, mTargetedCharacterhpj.strength, mTargetedCharacterhpj.fortitude);
        }
        cardState = CardState.Cooldown;
    }
}
