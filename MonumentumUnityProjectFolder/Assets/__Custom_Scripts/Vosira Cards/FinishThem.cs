﻿using UnityEngine;
using System.Collections;

//Target creaturehpj gains +4S when they are attacking a damaged creaturehpj until the end of this turn.
public class FinishThem : Card
{
    public TurnPhaseManager mTurnPhaseManager;
    public int mBoostAmount = 4;
    public bool mAttackedProperTarget = false;
	// Use this for initialization
	void Start () 
    {
        mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
        cardName = "FinishThem!";
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (mTurnPhaseManager && cardState == CardState.Table)
        {
            if (mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
            {
                OnConclusionPhase();
            }
        }
	}

    public override void CardEffect()
    {
        mTargetedCharacterhpj = handClickController.selectedCharacterhpj;
        mTargetedCharacterhpj.mCombatCards.Add(this);
        handManager.mCurrentCard = this;
        handManager.playingActionCard = false;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCreaturehpjs();
    }

    public void OnConclusionPhase()
    {
        mTargetedCharacterhpj.mCombatCards.Remove(this);
        cardState = CardState.Cooldown;
    }

    public override void BattleEffect(Characterhpj targetedCharacterhpj)
    {
        string boostName = cardName + "boost";
        if (targetedCharacterhpj.hitPoints < targetedCharacterhpj.startingHitPoints)
        {
            mAttackedProperTarget = true;
            mTargetedCharacterhpj.UpdateStrength(mBoostAmount);
            mTargetedCharacterhpj.mBoosts.Add(boostName);
            mTargetedCharacterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, mTargetedCharacterhpj.strength, mTargetedCharacterhpj.fortitude);
        }
    }

    public override void AfterBattleEffect()
    {
        string boostName = cardName + "boost";
        if (mAttackedProperTarget)
        {
            mTargetedCharacterhpj.UpdateStrength(-mBoostAmount);
            mTargetedCharacterhpj.mBoosts.Remove(boostName);
            mTargetedCharacterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, mTargetedCharacterhpj.strength, mTargetedCharacterhpj.fortitude);
        }
    }
}
