﻿using UnityEngine;
using System.Collections;

//Target friendly creaturehpj. Target's attacks cause a 4S attack to all enemy creaturehpjs within AOE(1).
public class ExplosiveShot : Card 
{
    public TurnPhaseManager mTurnPhaseManager;
	// Use this for initialization
	void Start () 
    {
        mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
        cardName = "Explosive Shot";
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (mTurnPhaseManager && cardState == CardState.Table)
        {
            if (mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
            {
                OnConclusionPhase();
            }
        }
	}

    public override void CardEffect()
    {
        mTargetedCharacterhpj = handClickController.selectedCharacterhpj;
        mTargetedCharacterhpj.mCombatCards.Add(this);
        handManager.mCurrentCard = this;
        handManager.playingActionCard = false;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCreaturehpjs();
    }

    public void OnConclusionPhase()
    {
        mTargetedCharacterhpj.mCombatCards.Remove(this);
        cardState = CardState.Cooldown;
    }

    public override void BattleEffect(Characterhpj targetedCharacterhpj)
    {
        foreach (BoardSquare aoeSquare in targetedCharacterhpj.currentSpace.GetComponent<BoardSquare>().neighborSquares)
        {
            if (aoeSquare.occupier.GetComponent<Characterhpj>().controllingPlayer != mTargetedCharacterhpj.controllingPlayer)
            {
                Characterhpj characterhpj = aoeSquare.occupier.GetComponent<Characterhpj>();
                //Roll 2d6 and add your Strength
                int thisAttackPower = 4;
                int rollBonus = 0;

                for (int i = 0; i < 2; i++)
                {
                    int dieRoll = 0;
                    dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
                    dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));

                    if (dieRoll > rollBonus)
                    {
                        rollBonus = dieRoll;
                    }
                }
                thisAttackPower += rollBonus;

                //Compare your results to the target's Fortitude
                if (characterhpj.tag == "VoidBreacher")//Automatically hit a VoidBreacher
                {
                    //do nothing to a VoidBreacher
                }
                else if (thisAttackPower == 4 + 2)//Automatically miss if you roll a 1 on both dice
                {
                    Debug.Log(this.name + " missed " + characterhpj.name);
                    characterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mTargetedCharacterhpj.GetComponent<NetworkView>().viewID, false);
                }
                else if (thisAttackPower > characterhpj.fortitude || thisAttackPower == 4 + 12) //hit if you roll a 6 on both dice and/or roll higher than the target's Fortitude
                {
                    characterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mTargetedCharacterhpj.GetComponent<NetworkView>().viewID, true);
                    Debug.Log(this.name + " hit " + characterhpj.name + " leaving it with " + characterhpj.hitPoints + " HP");
                }
                else//Miss if you roll lower than the target's Fortitude
                {
                    Debug.Log(this.name + " missed " + characterhpj.name);
                    characterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mTargetedCharacterhpj.GetComponent<NetworkView>().viewID, false);
                }

                //Done attacking now



                //if you took the target down to 0 HP, kill it and move into its space if it was adjacent
                if (characterhpj.hitPoints == 0)
                {
                    characterhpj.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All); //kill the target
                }
            }
        }
    }
}
