﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace Assets.CustomScripts 
{
    public class InformationManager : MonoBehaviour
    {
        public List<GameObject> mDeck = new List<GameObject>();
        public string mUser;
        // Use this for initialization
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {

        }

        void Awake()
        {
            DontDestroyOnLoad(transform.gameObject);
        }

        public void LoadDeck()
        {
            if (Network.isServer)
            {
                foreach (GameObject card in mDeck)
                {
                    GameObject cardSpawned = (GameObject)Network.Instantiate(card, 
                        Vector3.zero, Quaternion.identity, 0);
                    cardSpawned.GetComponent<Card>().playerNumber = 1;
                }
            }
            else if (Network.isClient)
            {
                foreach (GameObject card in mDeck)
                {
                    GameObject cardSpawned = (GameObject)Network.Instantiate(card,
                        Vector3.zero, Quaternion.identity, 0);
                    cardSpawned.GetComponent<Card>().playerNumber = 2;
                }
            }
        }
    }
}

