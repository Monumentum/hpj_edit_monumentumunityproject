﻿using UnityEngine;
using System.Collections;

public class Obliterate : Card 
{
	Characterhpj mVoidBreacher; 


	public int mTargetRange = 3;
	public Characterhpj mDamagedCharacterhpj;
	// Use this for initialization
	void Start () 
	{
        cardName = "Obliterate";
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	
	public override void CardEffect()
	{
		mTargetedCharacterhpj = handClickController.selectedCharacterhpj;
		TargetAllCreaturehpjsInLOS(mTargetRange);        
		handManager.mCurrentCard = this;
		//handManager.mTarget = mTargetedCharacterhpj;
		handManager.mTargetting = true;
		handManager.playingActionCard = false;
	}
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCreaturehpjs();
	}
	
	public override void TargetAcquired()
	{
		mDamagedCharacterhpj = handClickController.mLastSelectedCharacterhpj;
		//handManager.mActivatingActionCard = true;
		handManager.mTargetting = false;



		//Actually attack the enemy characterhpj
		Debug.Log("Doing effect of " + cardName);
		//Find your voidbreacher to use as the source of the attack/strike
		Characterhpj[] voidBreachersInPlay = FindObjectsOfType(typeof(Characterhpj)) as Characterhpj[];
		foreach (Characterhpj voidBreacher in voidBreachersInPlay)
		{
			if (voidBreacher.tag == "VoidBreacher" && voidBreacher.controllingPlayer == playerNumber)
			{
				mVoidBreacher= voidBreacher;
			}
		}

		Characterhpj targetedCharacterhpj = mDamagedCharacterhpj;


		bool didIHit = false;
		//Roll 2d6 and add your Strength
		int thisAttackPower = 6;
		int rollBonus = 0;
		
		for (int i = 0; i < 2; i++)
		{
			int dieRoll = 0;
			dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
			dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
			
			if (dieRoll > rollBonus)
			{
				rollBonus = dieRoll;
			}
		}
		thisAttackPower += rollBonus;
		
		//Compare your results to the target's Fortitude
		if (targetedCharacterhpj.tag == "VoidBreacher")//Automatically hit a VoidBreacher
		{
			//do nothing to a VoidBreacher
		}
		else if (thisAttackPower == 4+2)//Automatically miss if you roll a 1 on both dice
		{
			Debug.Log(this.name + " missed " + targetedCharacterhpj.name);
			targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, false);
		}
		else if (thisAttackPower > targetedCharacterhpj.fortitude || thisAttackPower==4+12) //hit if you roll a 6 on both dice and/or roll higher than the target's Fortitude
		{
			targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, true);
			didIHit = true;
			Debug.Log(this.name + " hit " + targetedCharacterhpj.name + " leaving it with " + targetedCharacterhpj.hitPoints + " HP");
		}
		else//Miss if you roll lower than the target's Fortitude
		{
			Debug.Log(this.name + " missed " + targetedCharacterhpj.name);
			targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, false);
		}
		
		//Done attacking now
		
		
		
		//if you took the target down to 0 HP, kill it and move into its space if it was adjacent
		if (targetedCharacterhpj.hitPoints == 0)
		{
			targetedCharacterhpj.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All); //kill the target
		}

		//finish up the action and send this card to the Cooldown pile
		handManager.mActivatingActionCard = false;
		handManager.actionCardToPlay = null;
		cardState = CardState.Cooldown;

	}//End of TargetAcquired()




}
