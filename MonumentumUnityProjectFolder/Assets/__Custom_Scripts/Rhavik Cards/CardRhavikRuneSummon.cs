﻿using UnityEngine;
using System.Collections;

public class CardRhavikRuneSummon : Card 
{
	Characterhpj myVoidBreacher;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}


	public override void CardEffect()
	{
		Debug.Log("Doing effect of " + cardName);
		SummonRune();

		//For applying Flint's Rune Boost
		Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
		foreach (Characterhpj characterhpj in characterhpjs)
		{
			if ((characterhpj.controllingPlayer == playerNumber) && characterhpj.tag == "VoidBreacher")
			{
				myVoidBreacher = characterhpj;
			}
		}
		if (myVoidBreacher.GetComponent<FlintDross>() != null)
		{
			myVoidBreacher.GetComponent<FlintDross>().ApplyFlintRuneBoost();
		}

	}
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCreaturehpjs();
	}
}
