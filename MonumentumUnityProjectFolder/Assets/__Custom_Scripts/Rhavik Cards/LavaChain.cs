﻿using UnityEngine;
using System.Collections;

//Target friendly creaturehpj. Target may pull another 
//creaturehpj within LOS(3) up to, 2 squares towards the target.
public class LavaChain : Card 
{
    public int mTargetRange = 3;
    public int mMovementRange = 2;
    public Characterhpj mMovingCharacterhpj;
	// Use this for initialization
	void Start () 
    {
        cardName = "LavaChain";
	}
	
	// Update is called once per frame
	void Update () 
    {
		if(cardState != CardState.Table)
		{
			mMovingCharacterhpj = null;
			mTargetedCharacterhpj = null;
		}
	}

    public override void CardEffect()
    {
        mTargetedCharacterhpj = handClickController.selectedCharacterhpj;
        TargetAllCreaturehpjsInLOS(mTargetRange);        
        handManager.mCurrentCard = this;
        //handManager.mTarget = mTargetedCharacterhpj;
        handManager.mTargetting = true;
        handManager.playingActionCard = false;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCreaturehpjs();
    }

    public override void TargetAcquired()
    {
        mMovingCharacterhpj = handClickController.mLastSelectedCharacterhpj;
		HighlightSquaresBetweenCharacterhpjs(mTargetedCharacterhpj, mMovingCharacterhpj, mMovementRange);
        handManager.mActivatingActionCard = true;
        handManager.mTargetting = false;
    }

    public override void ActivateOnTarget()
    {
        //mMovingCharacterhpj.transform.position = handClickController.mClickedSquare.transform.position + new Vector3(0, 0.5f, 0);
       // mMovingCharacterhpj.networkView.RPC("UpdatePosition", RPCMode.All, mMovingCharacterhpj.transform.position);
		mMovingCharacterhpj.GetComponent<NetworkView>().RPC("ForceMovement", RPCMode.All, handClickController.mClickedSquare.gameObject.name);
		mMovingCharacterhpj.GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
		Debug.Log("Pulled the target");
        handManager.mActivatingActionCard = false;
        handManager.actionCardToPlay = null;
        cardState = CardState.Cooldown;
    }
}
