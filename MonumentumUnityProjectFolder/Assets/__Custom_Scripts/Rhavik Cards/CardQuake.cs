﻿using UnityEngine;
using System.Collections;

//Target friendly creaturehpj. Target causes all enemey creaturehpjs within AOE(1) to suffer 'crippled'.

public class CardQuake : Card 
{

	// Use this for initialization
	void Start () 
	{
        cardName = "Quake";
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public override void CardEffect()
	{
		foreach(BoardSquare aoeSquare in handClickController.selectedCharacterhpj.currentSpace.GetComponent<BoardSquare>().neighborSquares)
		{
			if (aoeSquare.occupier != null && aoeSquare.occupier.GetComponent<Characterhpj>() != null
			    && aoeSquare.occupier.tag != "VoidBreacher" && aoeSquare.occupier.GetComponent<Characterhpj>().controllingPlayer != playerNumber)
			{
				aoeSquare.occupier.GetComponent<Characterhpj>().GetComponent<NetworkView>().RPC("GetCrippled", RPCMode.All);
			}
		}

		//finish up the action and send this card to the Cooldown pile
		handManager.playingActionCard = false;
		handManager.mTargetting = false;
		handManager.actionCardToPlay = null;
		cardState = CardState.Cooldown;

	}


	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCreaturehpjs();
	}

}
