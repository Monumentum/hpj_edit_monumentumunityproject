﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rune : MonoBehaviour 
{

    public enum RuneType { Attacking, Conclusion, IsAttacked, Summoning, OnTrigger, IsDamaged };

    public RuneType mType;
    public Characterhpj mEquippedCharacterhpj;
	public Card mAssociatedCard;
	public TurnPhaseManager mTurnManager;
	// Use this for initialization
	public virtual void Start () 
    {
		mTurnManager = GameObject.FindObjectOfType(typeof(TurnPhaseManager)) as TurnPhaseManager;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public virtual void ActivateRune(Characterhpj targetCharacterhpj)
    {

    }

    [RPC] public virtual void EquipRune(NetworkViewID characterhpjID)
    {
        mEquippedCharacterhpj = NetworkView.Find(characterhpjID).observed.GetComponent<Characterhpj>();
        List<Rune> runes;
        if (mEquippedCharacterhpj.mActiveRunes.TryGetValue(mType, out runes))
        {
            if (!runes.Contains(this))
            {
                runes.Add(this);
				mEquippedCharacterhpj.GetComponent<NetworkView>().RPC("AddRuneCount", RPCMode.All, 1);
            }
        }
    }

    [RPC] public virtual void DestroyRune()
    {
		Debug.Log(name + " destroyed.");
        //put in code to move card to cooldown pile
		mAssociatedCard.cardState = Card.CardState.Cooldown;

        Destroy(this.gameObject);
    }

    [RPC] public virtual void RemoveRune()
    {
        List<Rune> runes;
        if (mEquippedCharacterhpj.mActiveRunes.TryGetValue(mType, out runes))
        {
            runes.Remove(this);
            mEquippedCharacterhpj.mNumberOfRunes--;
        }
    }
}
