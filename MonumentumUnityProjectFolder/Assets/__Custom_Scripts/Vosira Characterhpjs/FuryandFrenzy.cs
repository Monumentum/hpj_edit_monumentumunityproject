﻿using UnityEngine;
using System.Collections;

public class FuryandFrenzy : Characterhpj 
{

	// Use this for initialization
    public override void Start()
    {
        base.Start();

        if (attackRange == 1)
        {
            attackRange = 5;
        }

        if (sourceStoneCost == 0)
        {
            sourceStoneCost = 6;
        }

        if (strength == 0)
        {
            strength = 4;
        }

        if (fortitude == 0)
        {
            fortitude = 10;
        }

        characterhpjDominion = CharacterhpjDominion.Vosira;
	}

    public override bool AttackTarget(Characterhpj targetedCharacterhpj, int numberOfRolls = 1)
    {
        bool didIHit = base.AttackTarget(targetedCharacterhpj, 3);

        return didIHit;
    }
}
