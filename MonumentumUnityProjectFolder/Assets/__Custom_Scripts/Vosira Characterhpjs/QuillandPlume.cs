﻿using UnityEngine;
using System.Collections;

//Characterhpj info: Range(5), Barbed Shot: This characterhpj's attacks cause 'crippled'.

public class QuillandPlume : Characterhpj 
{

	// Use this for initialization
    public override void Start()
    {
        base.Start();

		if (attackRange == 1)
		{
			attackRange = 5;
		}
		if (sourceStoneCost == 0)
		{
			sourceStoneCost = 6;
		}
		
		if (strength == 0)
		{
			strength = 6;
		}
		
		if (fortitude == 0)
		{
			fortitude = 10;
		}
		
		characterhpjDominion = CharacterhpjDominion.Vosira;
	}


	//Override the AttackTarget function to cripple the target on a hit
	public override bool AttackTarget(Characterhpj targetedCharacterhpj, int numberOfRolls = 1)
	{

		bool didIHit = base.AttackTarget(targetedCharacterhpj);

		if(didIHit && targetedCharacterhpj != null)
		{
			targetedCharacterhpj.GetComponent<NetworkView>().RPC("GetCrippled", RPCMode.All);
		}

		return didIHit;
	}
}
