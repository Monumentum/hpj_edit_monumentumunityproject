﻿using UnityEngine;
using System.Collections;

public class RendandRive : Characterhpj 
{

	// Use this for initialization
    public override void Start()
    {
        base.Start();

        if (sourceStoneCost == 0)
        {
            sourceStoneCost = 4;
        }

        if (strength == 0)
        {
            strength = 4;
        }

        if (fortitude == 0)
        {
            fortitude = 12;
        }

        characterhpjDominion = CharacterhpjDominion.Vosira;
	}
	

    public override bool AttackTarget(Characterhpj targetedCharacterhpj, int numberOfRolls = 1)
    {
        bool didIHit = base.AttackTarget(targetedCharacterhpj);

        if ((didIHit) && (targetedCharacterhpj != null) && targetedCharacterhpj.fortitude > 0)
        {
            targetedCharacterhpj.GetComponent<NetworkView>().RPC("ModifyFortitude", RPCMode.All, -1);
        }

        return didIHit;
    }
}
