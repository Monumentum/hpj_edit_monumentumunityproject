﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RuneoftheFurious : Rune 
{

    public int mExtraAttacksGiven = 1;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        mType = Rune.RuneType.OnTrigger;
    }
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public override void ActivateRune(Characterhpj targetCharacterhpj)
    {
        //base.ActivateRune();
        targetCharacterhpj.mMaxAttacksATurn = targetCharacterhpj.mMaxAttacksATurn + mExtraAttacksGiven;
    }

    [RPC]
    public override void EquipRune(NetworkViewID characterhpjID)
    {
        base.EquipRune(characterhpjID);
        ActivateRune(mEquippedCharacterhpj);
    }

    [RPC]
    public override void DestroyRune()
    {
        mEquippedCharacterhpj.mMaxAttacksATurn = mEquippedCharacterhpj.mMaxAttacksATurn - mExtraAttacksGiven;
        base.DestroyRune();
    }
}
