﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RuneofBurningEmbers : Rune
{
    public int mDecreaseAmount = 1;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        mType = Rune.RuneType.IsAttacked;
    }
	
	// Update is called once per frame
	void Update () 
    {
	    
	}

    public override void ActivateRune(Characterhpj targetCharacterhpj)
    {
        //base.ActivateRune();
        targetCharacterhpj.ModifyFortitude(-mDecreaseAmount);
        targetCharacterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, targetCharacterhpj.strength, targetCharacterhpj.fortitude);
    }
}
