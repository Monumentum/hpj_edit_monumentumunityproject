﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Rune: This creaturehpj may be used as a summoning stone. If this creaturehpj is used as a summoning stone, this creaturehpj is destroyed.

public class RuneofCharredAsh : Rune
{
	string mOriginalTag="Familiar";
	// Use this for initialization
	void Start ()
	{
		base.Start();
		mType = Rune.RuneType.Summoning;
	}

	
	// Update is called once per frame
	void Update () 
	{
		if (mTurnManager && mEquippedCharacterhpj && mTurnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning && mEquippedCharacterhpj.tag != "VoidBreacher")
		{
			mOriginalTag = mEquippedCharacterhpj.tag;
			mEquippedCharacterhpj.tag="VoidBreacher";
		}
		else if (mTurnManager && mEquippedCharacterhpj && mTurnManager.currentPhase != TurnPhaseManager.TurnPhase.Summoning && mEquippedCharacterhpj.tag == "VoidBreacher")
		{
			mEquippedCharacterhpj.tag = mOriginalTag;
		}
	}


	public override void ActivateRune(Characterhpj targetCharacterhpj)
	{
		//base.ActivateRune();
		//Destroy the equipped characterhpj if they just summoned a familiar (as opposed to a minion)
		if (targetCharacterhpj.carriedLimboCharacterhpjs[targetCharacterhpj.limboCharacterhpjLookingAt].tag == "Familiar")
		{
			targetCharacterhpj.BeKilled();
		}
	}


}
