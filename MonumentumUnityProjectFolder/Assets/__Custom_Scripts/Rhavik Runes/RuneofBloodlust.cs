﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RuneofBloodlust : Rune 
{
    public int mBoostAmount = 1;

	// Use this for initialization
	public override void Start () 
    {
        base.Start();
        mType = Rune.RuneType.Attacking;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public override void ActivateRune(Characterhpj targetCharacterhpj)
    {
        //base.ActivateRune();
        if (targetCharacterhpj.GetComponent<NetworkView>().isMine)
        {
            targetCharacterhpj.ModifyStrength(mBoostAmount);
            targetCharacterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, targetCharacterhpj.strength, targetCharacterhpj.fortitude);
        }
        
    }
}
