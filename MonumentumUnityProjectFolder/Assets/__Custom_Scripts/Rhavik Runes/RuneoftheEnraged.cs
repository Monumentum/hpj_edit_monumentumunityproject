﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RuneoftheEnraged : Rune 
{
    public int mBoostAmount = 2;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        mType = Rune.RuneType.IsDamaged;
    }
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public override void ActivateRune(Characterhpj targetCharacterhpj)
    {
        //base.ActivateRune();
        targetCharacterhpj.ModifyStrength(mBoostAmount);
        targetCharacterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, targetCharacterhpj.strength, targetCharacterhpj.fortitude);
    }
}
