﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This script, along with PlayerClickController serve as the two primary components of the PlayerController prefab
//It stores references to the game objects that constitute the cards in the player's hand/deck for the purposes of cycling them between in-hand, in-play, and recharging/cooldown.

public class PlayerHandManager : MonoBehaviour 
{
	public List<Card> deck = new List<Card>();

	TurnPhaseManager turnManager;
	PlayerClickController clickController;
	int playerNumber;
	//Whether or not the player is currently looking at the action cards in their hand
	bool viewActionCards = false;

	//For the several steps that playing a card goes through
	public bool playingActionCard = false;
    public bool mTargetting = false;
    public bool mActivatingActionCard = false;

	//The Action card whose effects are being played out
	public Card actionCardToPlay;
	//The card we're currently using/looking at
    public Card mCurrentCard;

	//Whether or not an animatino we don't want intterurrpted is happening
	public bool mAnimationPlaying = false;

	//Info for mouseover tooltip
	MouseOverInfoManager mMouseOverInfoManager;
	
	public GUIStyle mHandCardTooltipStyle;

	// Use this for initialization
	void Start () 
	{
		//set up the objects we'll be referencing with this script
		clickController = GetComponent<PlayerClickController>();
		turnManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
		playerNumber = clickController.playerNumber;
		
		mMouseOverInfoManager = FindObjectOfType(typeof(MouseOverInfoManager)) as MouseOverInfoManager;

	}//end of Start()
	
	// Update is called once per frame
	void Update () 
	{
	
	}//end of Update()


	void OnGUI ()
	{
		// only draw stuff on my turn and when nothing is animating
		if (turnManager.playerTurn == playerNumber && this.GetComponent<NetworkView>().isMine && !mAnimationPlaying) 
		{
			//Variables for setting up the UI positiong for displaying the cards in the player's hand
			float handGUIXPos;
			float handGUIYPos;
			int handGUINumber;
			Card currentCardForGUI;

			//hide other GUI things if I'm in the middle of playing an action card, but provide a cancel button
			if (playingActionCard || mTargetting || mActivatingActionCard)
			{
				if (GUI.Button(new Rect(0, Screen.height*0.3f, Screen.width*0.1f, Screen.height*0.1f), new GUIContent("Cancel Action Card", "Cancel Action")))
				{
					playingActionCard = false;
					mTargetting = false;
					mActivatingActionCard = false;
					actionCardToPlay.cardState = Card.CardState.Hand;
					actionCardToPlay.mTargetedCharacterhpj = null;
					clickController.sourceStoneCount += actionCardToPlay.sourceStoneCost;
					actionCardToPlay = null;
					UnHighlightSquares();

				}

			}
			else
			{
				//Showing/Hiding Action Cards
				if (viewActionCards)
				{
					//Button to hide Action Cards
					if (GUI.Button(new Rect(0, Screen.height*0.3f, Screen.width*0.1f, Screen.height*0.1f), new GUIContent("Hide Action Cards in Hand", "Hide your hand")))
					{
						viewActionCards = false;
					}

					//Display the Action cards in your hand to the GUI
					handGUIXPos = Screen.width*0.1f;
					handGUIYPos = Screen.height*0.5f;
					handGUINumber=1;
					for (int i = 0; i < deck.Count; i++)
					{
						currentCardForGUI = deck[i].GetComponent<Card>();
						if(deck[i].GetComponent<Card>().cardState == Card.CardState.Hand)
						{
							if (currentCardForGUI.cardType == Card.CardType.Action)
							{
								if(GUI.Button(new Rect(handGUIXPos,handGUIYPos, Screen.width*0.1f, Screen.height*0.1f), new GUIContent(currentCardForGUI.cardName + ": " + currentCardForGUI.sourceStoneCost, currentCardForGUI.cardName)))
//								                                                                                                       currentCardForGUI.mCardDescription)) )
								{
									//Play the action card this button is for and do its effects if the player has enough source stones to pay its cost
									if (clickController.sourceStoneCount >= currentCardForGUI.sourceStoneCost)
									{
										clickController.sourceStoneCount -= currentCardForGUI.sourceStoneCost;
										viewActionCards = false;
										playingActionCard = true;
										currentCardForGUI.CardTarget();
										actionCardToPlay = currentCardForGUI;
									}
								}
								//Adjust the GUI positioning for the next card's button
								handGUIXPos += Screen.width*0.11f;
								if ((handGUINumber)%6==0)
								{
									handGUIXPos = Screen.width*0.1f;
									handGUIYPos += Screen.height*0.11f;
								}
								handGUINumber++;

							}
						}
					}



				}
				else
				{
					//Button to view Action Cards
					if (GUI.Button(new Rect(0, Screen.height*0.3f, Screen.width*0.1f, Screen.height*0.1f), new GUIContent("View Action Cards in Hand", "View your hand")))
					{
						viewActionCards = true;
						UnHighlightSquares();
					}
				}//End of showing/hiding action cards



				//What to do on the Preparation Phase
				if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation)
				{

				}//End of Preparation Phase

				//What to do on the Summoning Phase
				if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
				{
					//display the Familiars/Minions in your hand to the GUI during the Summoning Phase
					handGUIXPos = Screen.width*0.1f;
					handGUIYPos = Screen.height*0.7f;
					handGUINumber = 1;
					for (int i = 0; i < deck.Count; i++)
					{
						currentCardForGUI = deck[i].GetComponent<Card>();
						if(deck[i].GetComponent<Card>().cardState == Card.CardState.Hand)
						{
							if (currentCardForGUI.cardType == Card.CardType.Familiar || currentCardForGUI.cardType == Card.CardType.Minion)
							{
								if(GUI.Button(new Rect(handGUIXPos,handGUIYPos, Screen.width*0.1f, Screen.height*0.1f), new GUIContent(currentCardForGUI.cardName + ": " + currentCardForGUI.sourceStoneCost, currentCardForGUI.cardName)))
//								                                                                                                       "Strength: " + currentCardForGUI.summonedGameObject.GetComponent<Characterhpj>().strength
//								                                                                                                       +"\nFortitude: " + currentCardForGUI.summonedGameObject.GetComponent<Characterhpj>().fortitude
//								                                                                                                       +"\n"+ currentCardForGUI.mCardDescription)) )
								{
									if (clickController.sourceStoneCount >= currentCardForGUI.sourceStoneCost)
									{
										clickController.ReadyGameObjectToSummon(deck[i].summonedGameObject, i);

										Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();

										//Highlight Spaces containing characterhpjs that you can tell to carry the characterhpj in preparation for summoning it
										foreach (Characterhpj characterhpj in characterhpjs)
										{
											if (characterhpj.controllingPlayer == this.playerNumber)
											{
												if (currentCardForGUI.cardType == Card.CardType.Familiar && characterhpj.tag == "VoidBreacher")
												{
													characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
												}
												else if (currentCardForGUI.cardType == Card.CardType.Minion && characterhpj.tag == "Familiar")
												{
													characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
												}
												else
												{
													characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
												}
											}
										}
									}
								}
								handGUIXPos += Screen.width*0.11f;
								if ((handGUINumber)%6==0)
								{
									handGUIXPos = Screen.width*0.1f;
									handGUIYPos += Screen.height*0.11f;
								}
								handGUINumber++;

							}
						}
					}
				}//End of Summoning Phase

				//What to do on the Movement Phase
				if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
				{
					
				}//End of Movement Phase
				
				//What to do on the Combat Phase
				if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
				{
					
				}//End of Combat Phase
				
				//What to do on the Conclusion Phase
				if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
				{
					
				}//End of Conclusion Phase
			}
		}

		if (Event.current.type == EventType.Repaint)
		{
			//Dispaly the card art/info for the card whose button is currently being moused-over
			if (GUI.tooltip != "")
			{
				clickController.mouseOverGUI = true;

				foreach(Card cardImage in deck)
				{
					if(GUI.tooltip == cardImage.cardName)
					{
						mMouseOverInfoManager.mCardImage = cardImage.mCardArt;
						mMouseOverInfoManager.mStrengthStatus = 0;
						mMouseOverInfoManager.mFortitudeStatus = 0;
					}
				}

			}
			else
			{
			}
		}
	}//end of OnGUI

	//find the cards and put them in your hand at the start of the game
	public void CreateHand ()
	{
		clickController = GetComponent<PlayerClickController>();

		playerNumber = clickController.playerNumber;
		Debug.Log("Creating Hand for Player " + playerNumber);

		Card[] cardsToFind = FindObjectsOfType<Card>();
		//Because the prefabs of the card game objects do not have network view components, but get instatiated for both players, 
		//assign the proper player number to both sets of cards
		foreach (Card myCard in cardsToFind)
		{
			if (myCard.playerNumber == 0)
			{
				if(Network.isServer)
				{
					myCard.playerNumber = 2;
				}
				else if (Network.isClient)
				{
					myCard.playerNumber = 1;
				}
			}
			//Add the cards that belong to this player to this player's hand/deck
			if (myCard.playerNumber == playerNumber)
			{
				deck.Add(myCard);
				myCard.handNumber = deck.Count-1;
				myCard.handManager = this;
				myCard.handClickController = this.clickController;

			}
		}

	}//End of CreateHand()


	void UnHighlightSquares ()
	{
		BoardSquare[] squaresToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
		foreach (BoardSquare unHighlightTarget in squaresToUnHighlight)
		{
			unHighlightTarget.inMovementRange = false;
			unHighlightTarget.hasAttackableTarget = false;
			unHighlightTarget.validSummonSpot = false;
		}
	}//End of UnHighlightSquares()
}
