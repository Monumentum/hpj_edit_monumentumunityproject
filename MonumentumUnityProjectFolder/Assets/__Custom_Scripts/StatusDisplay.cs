﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Script for showing status and health icons above a characterhpj's model during play
//It is attached as a component of the various characterhpj prefabs

public class StatusDisplay : MonoBehaviour 
{
	//The characterhpj this is showing the display for
	[SerializeField] private Characterhpj mCharacterhpj;
	//The characterhpj's default strength/fortitude so we can show whether its current strength/fortitude is higher or lower
	[HideInInspector] public int mBaseStrength;
	[HideInInspector] public int mBaseFortitude;


	//An array of all the status icons we're using
	[SerializeField] private Texture2D[] mStatusMarkers;
	/*	
		Markers should have the following indices:
		0: Health
		1: Strength Up
		2: Strength Down
		3: Fortitude Up
		4: Fortitude Down
		5: Crippled
		6: Immobilized
		7: Rune Applied
	*/

	//A list of the currently applied status effects
	[SerializeField] private List<Texture2D> mCurrentStatusEffects = new List<Texture2D>();


	// Use this for initialization
	void Start () 
	{
		mCharacterhpj = GetComponent<Characterhpj>();
		//Set the base stats
		mBaseStrength = mCharacterhpj.strength;
		mBaseFortitude = mCharacterhpj.fortitude;

	}//END of Start()
	
	// Update is called once per frame
	void Update () 
	{
		//Reset the current status effects
		mCurrentStatusEffects.Clear();
		//Re-apply current staus effects
		
		//Check difference between Strength and BaseStrength
		if(mCharacterhpj.strength > mBaseStrength)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[1]);
		}
		else if(mCharacterhpj.strength < mBaseStrength)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[2]);
		}
		
		//Check difference between Fortitude and BaseFortitude
		if(mCharacterhpj.fortitude > mBaseFortitude)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[3]);
		}
		else if(mCharacterhpj.fortitude < mBaseFortitude)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[4]);
		}
		
		//Check if Crippled/Immobilized
		if(mCharacterhpj.crippled == 1)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[5]);
		}
		else if(mCharacterhpj.crippled == 2)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[6]);
		}
		
		//Check if the characterhpj has any Runes
		if(mCharacterhpj.mNumberOfRunes > 0)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[7]);
		}
		
	}//END of Update()

	void LateUpdate()
	{
	}//END of LateUpdate()


	void OnGUI()
	{
		//Find where the characterhpj is on screen so we can use that as the origin for our GUI positioning
		Vector3 characterhpjScreenPos = Camera.main.WorldToScreenPoint(this.gameObject.transform.position);

		characterhpjScreenPos = new Vector3(characterhpjScreenPos.x, Screen.height-characterhpjScreenPos.y, characterhpjScreenPos.z);
		//Offsets for GUI elements
		float healthXOffset = Screen.width*(-0.01f*(mCurrentStatusEffects.Count+mCharacterhpj.hitPoints)/2f);
		float healthYOffset = Screen.height*-0.055f;

//		if(mCurrentStatusEffects.Count<=0)
//		{
//			healthYOffset = Screen.height*-0.043f;
//		}
		float statusXOffset;// = Screen.width*(-0.03f*mCurrentStatusEffects.Count/2f);
		float statusYOffset = Screen.height*-0.055f;
		//For determining the width of the box to put behind all this
		float backgroundBoxWidth =  mCharacterhpj.hitPoints+mCurrentStatusEffects.Count;
		//Put down a box as a background for the icons
		GUI.Box(new Rect(characterhpjScreenPos.x+healthXOffset, characterhpjScreenPos.y+healthYOffset, Screen.height*0.015f*backgroundBoxWidth, Screen.height*0.015f),"");
		//Display Health icons
		for(int i = 1; i <= mCharacterhpj.hitPoints; i++)
		{
			GUI.DrawTexture(new Rect(characterhpjScreenPos.x+healthXOffset, characterhpjScreenPos.y+healthYOffset, Screen.height*0.015f, Screen.height*0.015f), mStatusMarkers[0]);
			//GUI.Box(new Rect(characterhpjScreenPos.x+healthXOffset, characterhpjScreenPos.y+healthYOffset, Screen.width*0.1f, Screen.height*0.1f), mCharacterhpj.name+","+characterhpjScreenPos);

			healthXOffset+=Screen.height*0.015f;
		}
		statusXOffset = healthXOffset;
		//Display status effects
		if(mCurrentStatusEffects.Count > 0)
		{
			for (int k = 0; k< mCurrentStatusEffects.Count; k++)
			{
				GUI.DrawTexture(new Rect(characterhpjScreenPos.x+statusXOffset, characterhpjScreenPos.y+statusYOffset, Screen.height*0.015f, Screen.height*0.015f), mCurrentStatusEffects[k]);
				//GUI.Box(new Rect(characterhpjScreenPos.x+statusXOffset, characterhpjScreenPos.y+statusYOffset, Screen.width*0.1f, Screen.height*0.1f), mCharacterhpj.name);

				statusXOffset+=Screen.height*0.015f;

			}
		}

	}//END of OnGUI
}
