﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

public class BoardSquare : MonoBehaviour 
{
	[SerializeField] private bool shouldExist = false;
	public List<BoardSquare>  neighborSquares;
	public GameObject occupier;
	public bool inMovementRange = false;
	public bool hasAttackableTarget = false;
	public bool validSummonSpot = false;
	public bool voidBreacherStartSpot = false;
	public int playerStartSpot = 0;
	public int mDistanceFromMover = 99;


	private TurnPhaseManager turnManager;
	// Use this for initialization
	void Start () 
	{
		turnManager = FindObjectOfType<TurnPhaseManager>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Change HighlightColors
		if (inMovementRange)
		{
			GetComponent<Renderer>().material.color = Color.blue;
			GetComponent<Renderer>().material.color = new Vector4 (GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.g, 0.3f);
		}
		else if (hasAttackableTarget)
		{
			GetComponent<Renderer>().material.color = Color.red;
			GetComponent<Renderer>().material.color = new Vector4 (GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.g, 0.3f);
		}
		else if (validSummonSpot)
		{
			GetComponent<Renderer>().material.color = Color.blue;
			GetComponent<Renderer>().material.color = new Vector4 (GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.g, 0.3f);
		}
		else if (voidBreacherStartSpot && turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation && turnManager.playerTurn == playerStartSpot)
		{
			GetComponent<Renderer>().material.color = Color.blue;
			GetComponent<Renderer>().material.color = new Vector4 (GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.g, 0.3f);
		}
		else
		{
			GetComponent<Renderer>().material.color = Color.white;
			GetComponent<Renderer>().material.color = new Vector4 (GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.g, 0.3f);
		}
	}//End of Update()

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "BoardInitializer")
		{
			shouldExist = true;

		}
		if (other.GetComponent<Characterhpj>() != null && occupier == null)
		{
			other.GetComponent<Characterhpj>().currentSpace = this.gameObject;
			occupier = other.gameObject;
		}
		if (other.GetComponent<Trap>() != null)
		{
			other.GetComponent<Trap>().mOccupiedSquare = this;
		}

	}//End of OnTriggerEnter()
	void OnTriggerExit (Collider other)
	{
		if (other.GetComponent<Characterhpj>() != null && other.gameObject == occupier)
		{
			occupier = null;
		}

	}//End of OnTriggerExit

	public void DeleteIfUnused ()
	{
		if (!shouldExist)
		{
			Destroy(this.gameObject);
		}
	}//End of DeleteIfUnused()
	public void FindNeighbors ()
	{
		BoardSquare[] adjacentSquares = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
		foreach (BoardSquare square in adjacentSquares)
		{
			if (Vector3.Distance(transform.position, square.transform.position) == 1f)
				neighborSquares.Add(square);
		}
	}//End of FindNeighbors()

}
