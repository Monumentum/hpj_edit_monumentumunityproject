﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WailingandMiserySiren : Characterhpj
{
	
	public int mBoostAmount = 1;
	public int warCryRange = 3;
	public List<Characterhpj> warCryTargets = new List<Characterhpj>();

	// Use this for initialization
	public override void Start()
	{
		base.Start();
		
		if (sourceStoneCost == 0)
		{
			sourceStoneCost = 6;
		}
		
		if (strength == 0)
		{
			strength = 6;
		}
		
		if (fortitude == 0)
		{
			fortitude = 10;
		}
		
		characterhpjDominion = CharacterhpjDominion.Irotar;
	}
	


	public override void BeginCombatPhase()
	{
		//Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
		string boostName = characterhpjName + "WarCryBoost";

		foreach (Characterhpj characterhpj in warCryTargets)
		{
			if ((characterhpj != null && characterhpj.controllingPlayer == this.controllingPlayer) && (characterhpj.tag == "Minion") && (characterhpj.mBoosts.Contains(boostName))&& GetComponent<NetworkView>().isMine)
			{
				characterhpj.ModifyStrength(-mBoostAmount);
				characterhpj.mBoosts.Remove(boostName);
			}
		}

		warCryTargets.Clear();
		FindWarCryTargets();
		foreach (Characterhpj characterhpj in warCryTargets)
		{
			if (characterhpj != null && characterhpj.controllingPlayer == this.controllingPlayer && GetComponent<NetworkView>().isMine)
			{
				if (!characterhpj.mBoosts.Contains(boostName))
				{
					//characterhpj.networkView.RPC("ModfiyStrength", RPCMode.All, mBoostAmount);
                    characterhpj.UpdateStrength(mBoostAmount);
                    characterhpj.mBoosts.Add(boostName);
                    characterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, characterhpj.strength, characterhpj.fortitude);
				}
			}
		}
		
		base.BeginCombatPhase();
	}
	
	[RPC] public override void BeKilled()
	{
		//Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
		string boostName = characterhpjName + "WarCryBoost";
		
		foreach (Characterhpj characterhpj in warCryTargets)
		{
			if ((characterhpj != null && characterhpj.controllingPlayer == this.controllingPlayer) && (characterhpj.tag == "Minion") 
			    && characterhpj!= this && (characterhpj.mBoosts.Contains(boostName)) && GetComponent<NetworkView>().isMine)
			{
				Debug.Log(characterhpj.name);
                if (GetComponent<NetworkView>().isMine)
                {
                    characterhpj.UpdateStrength(-mBoostAmount);
                    characterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, characterhpj.strength, characterhpj.fortitude);
                }
                characterhpj.mBoosts.Remove(boostName);
			}
		}
		base.BeKilled();
	}

	public void FindWarCryTargets()
	{
		Debug.Log("I'm selected an ready to move");
		for (int i = 1; i <= warCryRange; i++)
		{
			//highlight adjacent squares
			if (i == 1)
			{
				//Debug.Log("Highlighting the first set of squares");
				foreach(BoardSquare potentialWarCryTarget in currentSpace.GetComponent<BoardSquare>().neighborSquares)
				{
					potentialWarCryTarget.hasAttackableTarget = true;
				}
			}
			//Highlight other squares in range
			else
			{
				//Debug.Log("Highlighting another set of squares");
				BoardSquare[] highlightedWarCryTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
				foreach (BoardSquare newWarCryTarget in highlightedWarCryTargets)//Add any already highlighted squares to the list
				{
					if (newWarCryTarget.hasAttackableTarget == true)
					{
						highlightedSquareList.Add(newWarCryTarget);
					}
				}
				foreach(BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
				{
					foreach(BoardSquare potentialWarCryTarget in listedSqaure.neighborSquares)
					{
						if (Vector3.Distance(potentialWarCryTarget.gameObject.transform.position, currentSpace.gameObject.transform.position) == i)
						{
							potentialWarCryTarget.hasAttackableTarget = true;
						}
		
					}
				}
					
			}
			//add the valid occupiers of highlighted squares to the warCryTargets 
			if (i == warCryRange)
			{
				BoardSquare[] warCryTargetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				foreach (BoardSquare warCryTargetSquare in warCryTargetToUnHighlight)//Unhighlight any squares that are Highlighted
				{
					if (warCryTargetSquare.occupier == null)
					{
						warCryTargetSquare.hasAttackableTarget = false;
					}
					else if (warCryTargetSquare.occupier != null)
					{
						if (warCryTargetSquare.occupier.tag == "Minion" && warCryTargetSquare.hasAttackableTarget && warCryTargetSquare.occupier.GetComponent<Characterhpj>().controllingPlayer == this.controllingPlayer)
						{
						warCryTargets.Add(warCryTargetSquare.occupier.GetComponent<Characterhpj>());
						}
						warCryTargetSquare.hasAttackableTarget = false;

					}
				}
					
			}
		}
	} //End of FindBoostTargets()

}
