﻿using UnityEngine;
using System.Collections;

public class SwirlingandSurgingMaw : Characterhpj
{

    public int mBoostAmount = 1;
    // Use this for initialization
    public override void Start()
    {
        base.Start();

        if (sourceStoneCost == 0)
        {
            sourceStoneCost = 4;
        }

        if (strength == 0)
        {
            strength = 4;
        }

        if (fortitude == 0)
        {
            fortitude = 10;
        }

        characterhpjDominion = CharacterhpjDominion.Irotar;
    }


    public override void BeginCombatPhase()
    {
        Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
        string boostName = characterhpjName + "boost";

        foreach (Characterhpj characterhpj in characterhpjs)
        {
			if ((characterhpj.controllingPlayer == this.controllingPlayer) && characterhpj.creaturehpj && GetComponent<NetworkView>().isMine)
            {
                if (!characterhpj.mBoosts.Contains(boostName))
                {
                    //characterhpj.networkView.RPC("ModifyFortitude", RPCMode.All, mBoostAmount);
                    characterhpj.UpdateFortitude(mBoostAmount);
                    characterhpj.mBoosts.Add(boostName);
                    characterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, characterhpj.strength, characterhpj.fortitude);
                }
            }
        }

        base.BeginCombatPhase();
    }

    [RPC]
    public override void BeKilled()
    {
        Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
        string boostName = characterhpjName + "boost";

        foreach (Characterhpj characterhpj in characterhpjs)
        {
			if ((characterhpj.controllingPlayer == this.controllingPlayer) && characterhpj.creaturehpj && GetComponent<NetworkView>().isMine && characterhpj != this)
            {
                if (characterhpj.mBoosts.Contains(boostName))
                {
                    if (GetComponent<NetworkView>().isMine)
                    {
                        characterhpj.UpdateFortitude(-mBoostAmount);
                        characterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, characterhpj.strength, characterhpj.fortitude);
                    }
                    characterhpj.mBoosts.Remove(boostName);
                }
            }
        }
        base.BeKilled();
    }
}
