﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RippleElshara : Characterhpj 
{
    private TurnPhaseManager mTurnManager;
    private PlayerClickController mPlayer;
    
	// Use this for initialization
	public override void Start () 
    {
        base.Start();

        mTurnManager = FindObjectOfType<TurnPhaseManager>();
        PlayerClickController[] players = FindObjectsOfType<PlayerClickController>();
        foreach(PlayerClickController player in players)
        {
            if (player.playerNumber == this.controllingPlayer)
            {
                mPlayer = player;
                break;
            }
        }

        characterhpjDominion = CharacterhpjDominion.Irotar;
	}
	


    void OnGUI()
    {
        if ((mTurnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion) && (GetComponent<NetworkView>().isMine)
            && (!mPlayer.mCanSwap) && (mTurnManager.playerTurn == this.controllingPlayer))
        {
            if (GUI.Button(new Rect((Screen.width * 0.4f), Screen.height * 0.45f, Screen.width * 0.2f, Screen.height * 0.1f), "Swap Two Characterhpjs You Control?"))
            {
                mPlayer.mCanSwap = true;
                Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
                foreach (Characterhpj characterhpj in characterhpjs)
                {
                    if (characterhpj.controllingPlayer == this.controllingPlayer)
                    {
                        characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
                    }
                }
            }
        }
    }
}
