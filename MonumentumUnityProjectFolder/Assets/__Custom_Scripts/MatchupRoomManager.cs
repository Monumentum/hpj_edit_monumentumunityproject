﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MatchupRoomManager : MonoBehaviour 
{
	public int mPlayerCount = 0;
	public int mReadyPlayers = 0;

	List<string> mSelectedVoidBreacherNames = new List<string>();

//	public NetworkView nv;
	// Use this for initialization
	void Start () 
	{
		//find any network managers that have not found this object yet
		Assets.CustomScripts.NetworkManager[] networkManagers = FindObjectsOfType(typeof(Assets.CustomScripts.NetworkManager)) as Assets.CustomScripts.NetworkManager[];
		foreach(Assets.CustomScripts.NetworkManager netman in networkManagers)
		{
			netman.FindMatchupRoomManager();
		}
	}
	
	void OnLevelWasLoaded()
	{
		//nv = gameObject.AddComponent("NetworkView") as NetworkView;

	}
	// Update is called once per frame
	void Update () 
	{
		if(mReadyPlayers == mPlayerCount && mReadyPlayers != 0)
		{
			GameObject.Find("NetworkManager").GetComponent<Assets.CustomScripts.NetworkManager>().StartMatch();
		}
	}

	void OnGUI()
	{
		for (int i = 0; i < mSelectedVoidBreacherNames.Count; i++)
		{
			GUI.Box(new Rect(Screen.width * 0.775f, Screen.height * (0.3f+(0.1f*i)), Screen.width * 0.1f, Screen.height * 0.1f), "Player " + (i+1) + ": " + mSelectedVoidBreacherNames[i]);
		}
	}

	[RPC]
	public void UpdatePlayerCount()
	{
		mPlayerCount++;
	}

	[RPC]
	public void UpdateReadyCount()
	{
		mReadyPlayers++;
		Debug.Log("Ready Players: " + mReadyPlayers);
	}


	[RPC]
	public void UpdateVoidBreacherNames(string tempName)
	{
		mSelectedVoidBreacherNames.Insert(0,tempName);
	}
}
