﻿using UnityEngine;
using System.Collections;

public class ChangeSceneButton : MonoBehaviour 
{
	[SerializeField] private string mSceneName;
	[SerializeField] private string mButtonText;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnGUI()
	{
		if (GUI.Button(new Rect(Screen.width*0.01f, Screen.height*0.875f, Screen.width*0.2f, Screen.height*0.1f), mButtonText))
		{
			Application.LoadLevel(mSceneName);
		}
	}
}
