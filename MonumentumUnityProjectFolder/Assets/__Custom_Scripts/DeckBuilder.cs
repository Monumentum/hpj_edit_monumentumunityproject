﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Serialization.JsonFx;

public class DeckBuilder : MonoBehaviour 
{
    public enum CardTypePicking { VoidBreacher, Familiar, Action , End};
    
    public List<GameObject> mVoidBreacher = new List<GameObject>();
    public List<GameObject> mFamiliars = new List<GameObject>();
    public List<GameObject> mActionCards = new List<GameObject>();

    public CardTypePicking mPhase = CardTypePicking.VoidBreacher;
    public Card.CardDominion mDeckType;

    public List<GameObject> mAllAvailableCards = new List<GameObject>();
    public List<GameObject> mFlintExpansionCards = new List<GameObject>();
    public List<GameObject> mRippleExpansionCards = new List<GameObject>();
    public List<GameObject> mLarkExpansionCards = new List<GameObject>();
    

    public float mGUIStartPosX;
    public float mGUIStartPosY;

    private bool mReadyToMoveForward = false;
    [SerializeField] private bool mStillSelecting = true;
    public Assets.CustomScripts.NetworkManager mNetworkManager;
    public Assets.CustomScripts.InformationManager mInformationManager;
	
	//Strings for text fields for saving/loading decks via strings of numeric ID codes
	private string mDeckCodeInput ="";
	private string mDeckCodeOutput = "";


	//For keeping track of what scene we are currently in
	public enum DeckBuilderScene{Lobby, Matchup, Game}; 
	public DeckBuilderScene mDeckBuilderCurrentScene;

	[SerializeField] private GUIStyle mCardDisplayStyle;
	// Use this for initialization
	void Start () 
    {

		if (mInformationManager == null)
		{
			mInformationManager = FindObjectOfType(typeof(Assets.CustomScripts.InformationManager)) as Assets.CustomScripts.InformationManager;
		}
		if (mNetworkManager == null)
		{
			mNetworkManager = FindObjectOfType(typeof(Assets.CustomScripts.NetworkManager)) as Assets.CustomScripts.NetworkManager;
		}
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (Application.loadedLevelName == "CardPurchaseMenu")
		{
			Destroy(transform.gameObject);
		}

	}


	void OnLevelWasLoaded()
	{
		mStillSelecting = true;

		if(Application.loadedLevelName == "Lobby")
		{
			mDeckBuilderCurrentScene = DeckBuilderScene.Lobby;
		}
		if(Application.loadedLevelName == "Matchup")
		{
			mDeckBuilderCurrentScene = DeckBuilderScene.Matchup;


		}


	}

	//For telling the Room Manager in Matchup to update voidbreacher names.
	public void CallToUpdateVoidBreacherNames()
	{
		MatchupRoomManager roomManager = FindObjectOfType(typeof(MatchupRoomManager)) as MatchupRoomManager;
		roomManager.GetComponent<NetworkView>().RPC("UpdateVoidBreacherNames", RPCMode.All, mVoidBreacher[0].GetComponent<Card>().cardName);

	}

	//For not deleting on moving to a new scene
	void Awake()
	{
		if (mDeckBuilderCurrentScene != DeckBuilderScene.Game)
		{
			DontDestroyOnLoad(transform.gameObject);
		}
		else
		{
			Destroy(transform.gameObject);
		}
	}

    void OnGUI()
    {
		if (mStillSelecting)
		{
			//Where we display the Voidbreacher that's been chosen
	        GUI.Label(new Rect(Screen.width * 0.23f, Screen.height * 0.10f, Screen.width * 0.40f, Screen.height * 0.20f), "Chosen VoidBreacher:");
			float spacing = 0.05f;
			//Display all the Voidbreachers (there should be a max of 1) that have been chosen for the deck
			foreach (GameObject card in mVoidBreacher)
			{
				GUI.Label(new Rect(Screen.width * 0.23f, Screen.height * (0.10f + spacing), Screen.width * 0.40f, Screen.height * (0.20f + spacing)),  new GUIContent(card.GetComponent<Card>().cardName, 
				                                                                                                                                                      card.GetComponent<Card>().cardName));
			}
			//Where we display the Familiars that have been chosen
			GUI.Label(new Rect(Screen.width * 0.43f, Screen.height * 0.10f, Screen.width * 0.60f, Screen.height * 0.20f), "Chosen Familiars:");
			spacing = 0.05f;
			//Display all the Familiars (there should be a max of 6) that have been chosen for the deck
			foreach (GameObject card in mFamiliars)
			{
				GUI.Label(new Rect(Screen.width * 0.43f, Screen.height * (0.10f + spacing), Screen.width * 0.60f, Screen.height * (0.20f + spacing)), new GUIContent(card.GetComponent<Card>().cardName, 
				                                                                                                                                                     card.GetComponent<Card>().cardName));
//				                                                                                                                                                     + "\nDominion: " + card.GetComponent<Card>().cardDominion 
//					                                                                                                                                                     +"\nSource Stone Cost: " + card.GetComponent<Card>().sourceStoneCost+ "\n" + card.GetComponent<Card>().mCardDescription));
				spacing += 0.05f;
			}
			//Where we display the Action Cards that have been chosen
			GUI.Label(new Rect(Screen.width * 0.63f, Screen.height * 0.10f, Screen.width * 0.80f, Screen.height * 0.20f), "Chosen Action Cards:");
			spacing = 0.05f;
			//Display all the Action Cards (there should be a max of 6) that have been chosen for the deck
			foreach (GameObject card in mActionCards)
			{
				GUI.Label(new Rect(Screen.width * 0.63f, Screen.height * (0.10f + spacing), Screen.width * 0.80f, Screen.height * (0.20f + spacing)), new GUIContent(card.GetComponent<Card>().cardName, 
				                                                                                                                                                     card.GetComponent<Card>().cardName));
//				                                                                                                                                                     + "\nDominion: " + card.GetComponent<Card>().cardDominion 
//					                                                                                                                                                     +"\nSource Stone Cost: " + card.GetComponent<Card>().sourceStoneCost+ "\n" + card.GetComponent<Card>().mCardDescription));
				spacing += 0.05f;
			}
			mGUIStartPosX = Screen.width * 0.1f;
			mGUIStartPosY = Screen.height * 0.5f;

			int guiCardNumber = 1;
			
			//A switch that checks which section of deck construction we are on (Picking Voidbreachers, Familiars, or Action Cards)
	        switch(mPhase)
	        {
			//Picking Voidbreachers
			case CardTypePicking.VoidBreacher:
				if (mDeckBuilderCurrentScene == DeckBuilderScene.Lobby)
				{
					foreach (GameObject card in mAllAvailableCards)
					{
		            	if (card.GetComponent<Card>().cardType == Card.CardType.VoidBreacher)
						{
							if (GUI.Button(new Rect(mGUIStartPosX, mGUIStartPosY, Screen.width * 0.1f, Screen.height * 0.1f), new GUIContent(card.GetComponent<Card>().cardName, 
							                                                                                                                 card.GetComponent<Card>().cardName)))
//							                                                                                                                 + "\nDominion: " + card.GetComponent<Card>().cardDominion 
//							                                                                                                                 +"\nSource Stone Cost: " + card.GetComponent<Card>().sourceStoneCost+ "\n" + card.GetComponent<Card>().mCardDescription)))
							{
								//Deselect the current voidbreacher if you clicked on the one you already selected
								if (mVoidBreacher.Contains(card))
								{
									mVoidBreacher.Remove(card);
									mReadyToMoveForward = false;
								}
								//Switch Voidbreachers if you already have one selected
								else if (mVoidBreacher.Count > 0)
								{
									//Clear all your selections if you select a voidbreacher of a different dominion
									if (mDeckType != card.GetComponent<Card>().cardDominion)
									{
										mFamiliars.Clear();
										mActionCards.Clear();
									}
		                            mVoidBreacher.Clear();
									mVoidBreacher.Add(card);
									mDeckType = card.GetComponent<Card>().cardDominion;
								}
								else
								{
									//Clear all your selections if you select a voidbreacher of a different dominion than the other cards you have selected
									if (mDeckType !=null && mDeckType != card.GetComponent<Card>().cardDominion)
									{
										mFamiliars.Clear();
										mActionCards.Clear();
									}
									mVoidBreacher.Add(card);
									mDeckType = card.GetComponent<Card>().cardDominion;
								}
							}
							mGUIStartPosX += Screen.width * 0.11f;
							if (guiCardNumber % 6 == 0)
							{
								mGUIStartPosX = Screen.width * 0.1f;
								mGUIStartPosY += Screen.height * 0.11f;
							}
						}
					}
					if (mVoidBreacher.Count == 1)
					{
						mReadyToMoveForward = true;
					}
				}
				break; //End of picking Voidbreachers

			//Picking familiars
			case CardTypePicking.Familiar:
				foreach (GameObject card in mAllAvailableCards)
				{
					if ((card.GetComponent<Card>().cardType == Card.CardType.Familiar) && card.GetComponent<Card>().cardDominion == mDeckType)
					{
						if (GUI.Button(new Rect(mGUIStartPosX, mGUIStartPosY, Screen.width * 0.1f, Screen.height * 0.1f), new GUIContent(card.GetComponent<Card>().cardName, 
						                                                                                                                 card.GetComponent<Card>().cardName)))
//									                                                                                                                 + "\nDominion: " + card.GetComponent<Card>().cardDominion 
//						                                                                                                                 +"\nSource Stone Cost: " + card.GetComponent<Card>().sourceStoneCost+ "\n" + card.GetComponent<Card>().mCardDescription)))
						{
							if (mFamiliars.Contains(card))
							{
								mFamiliars.Remove(card);
								if (mFamiliars.Count < 6)
								{
									mReadyToMoveForward = false;
								}
							}
							else if (mFamiliars.Count < 6)
							{
								mFamiliars.Add(card);
							}
						}
						mGUIStartPosX += Screen.width * 0.11f;
						if (guiCardNumber % 6 == 0)
						{
							mGUIStartPosX = Screen.width * 0.1f;
							mGUIStartPosY += Screen.height * 0.11f;
						}
						guiCardNumber++;
					}
				}
				if (mFamiliars.Count == 6)
				{
					mReadyToMoveForward = true;
				}
				break;//End of picking familiars

				//Picking action cards
			case CardTypePicking.Action:
				foreach (GameObject card in mAllAvailableCards)
				{
					if ((card.GetComponent<Card>().cardType == Card.CardType.Action || card.GetComponent<Card>().cardType == Card.CardType.Minion) && card.GetComponent<Card>().cardDominion == mDeckType)
					{
						if (GUI.Button(new Rect(mGUIStartPosX, mGUIStartPosY, Screen.width * 0.1f, Screen.height * 0.1f), new GUIContent(card.GetComponent<Card>().cardName, 
						                                                                                                                 card.GetComponent<Card>().cardName)))
//									                                                                                                                 + "\nDominion: " + card.GetComponent<Card>().cardDominion 
//						                                                                                                                 +"\nSource Stone Cost: " + card.GetComponent<Card>().sourceStoneCost+ "\n" + card.GetComponent<Card>().mCardDescription)))
						{
							if (mActionCards.Contains(card))
							{
								mActionCards.Remove(card);
								if (mActionCards.Count < 6)
								{
									mReadyToMoveForward = false;
								}
							}
							else if (mActionCards.Count < 6)
							{
								mActionCards.Add(card);
							}
						}
						mGUIStartPosX += Screen.width * 0.11f;
						if (guiCardNumber % 6 == 0)
						{
							mGUIStartPosX = Screen.width * 0.1f;
							mGUIStartPosY += Screen.height * 0.11f;
						}
						guiCardNumber++;
					}
				}
				if (mActionCards.Count == 6)
				{
					mReadyToMoveForward = true;
				}
				break;//End of picking action cards
			}

			//Buttons for going forward and back during the lobby
			if(mDeckBuilderCurrentScene == DeckBuilderScene.Lobby)
			{
				//Button to go to the next section once cards have been selected
				if (mReadyToMoveForward && mPhase != CardTypePicking.End)
				{
					if (GUI.Button(new Rect(Screen.width * 0.5f, Screen.height * 0.75f, Screen.width * 0.1f, Screen.height * 0.1f), new GUIContent("Move To Next Selection Set")))
					{
						mPhase++;
						mReadyToMoveForward = false;
					}
				}
				//Once all the cards have been picked, have a button to add them to the deck and move to the lobby
				else if (mPhase == CardTypePicking.End)
				{
					if (GUI.Button(new Rect(Screen.width * 0.5f, Screen.height * 0.5f, Screen.width * 0.1f, Screen.height * 0.1f), new GUIContent("Move To Lobby")))
					{
						SendDeckToInformationManager();
					}
				}
				//Button to go back a step of picking
				if (mPhase != CardTypePicking.VoidBreacher)
				{
					if (GUI.Button(new Rect(Screen.width * 0.2f, Screen.height * 0.75f, Screen.width * 0.1f, Screen.height * 0.1f), new GUIContent("Move To Previous Selection Set")))
					{
						mPhase--;
					}
				}
			}//End of Lobby scene forward/back buttons

			//Buttons for choosing to edit Familiar and Action cards in the Matchup scene
			if(mDeckBuilderCurrentScene == DeckBuilderScene.Matchup)
			{
				//Button to go to do last-minute editing of Familiars
				if (GUI.Button(new Rect(Screen.width * 0.2f, Screen.height * 0.75f, Screen.width * 0.1f, Screen.height * 0.1f), new GUIContent("Edit Familiars")))
				{
					mPhase = CardTypePicking.Familiar;
					mReadyToMoveForward = false;
				}
				//Button to go to do last-minute editing of Action Cards
				if (GUI.Button(new Rect(Screen.width * 0.5f, Screen.height * 0.75f, Screen.width * 0.1f, Screen.height * 0.1f), new GUIContent("Edit Action Cards")))
				{
					mPhase = CardTypePicking.Action;
					mReadyToMoveForward = false;
				}

				//Once all the cards have been picked, have a button to add them to the deck and lock in the deck
				else if (mFamiliars.Count == 6 && mActionCards.Count == 6 && mVoidBreacher.Count == 1 && mNetworkManager.mFoundMatchupManager)
				{
					if (GUI.Button(new Rect(Screen.width * 0.775f, Screen.height * 0.5f, Screen.width * 0.1f, Screen.height * 0.1f), new GUIContent("Lock in Deck")))
					{
						mNetworkManager.mReadyToStartMatch = true;
						mStillSelecting = false;
						SendDeckToInformationManager();
						mDeckBuilderCurrentScene = DeckBuilderScene.Game;

						MatchupRoomManager roomManager = FindObjectOfType(typeof(MatchupRoomManager)) as MatchupRoomManager;
						roomManager.GetComponent<NetworkView>().RPC("UpdateReadyCount", RPCMode.All);
						Destroy(transform.gameObject);
					}
				}
			}
		


			//Tooltip that explains what card button you are hovering over
			//GUI.Label(new Rect(Screen.width*0.01f, Screen.height*0.01f, Screen.width*0.15f, Screen.height*0.4f), GUI.tooltip);
			foreach(GameObject cardImage in mAllAvailableCards)
			{
				if(GUI.tooltip == cardImage.GetComponent<Card>().cardName)
				{
					GUI.Label(new Rect(Screen.width*0.01f, Screen.height*0.01f, Screen.width*0.2f, Screen.height*0.45f), cardImage.GetComponent<Card>().mCardArt, mCardDisplayStyle);
				}
			}
		

			//Buttons and Text fields for saving/loading decks via ID Codes
			//Load Deck
			mDeckCodeInput = GUI.TextField(new Rect(Screen.width*0.775f, Screen.height*0.72f, Screen.width*0.2f, Screen.height*0.1f), mDeckCodeInput, 117);
			if(GUI.Button(new Rect(Screen.width*0.625f, Screen.height*0.72f, Screen.width*0.15f, Screen.height*0.1f), "Load deck from code:"))
			{
				BuildDeckFromIDCode();
			}
			//Save Deck
			mDeckCodeOutput = GUI.TextField(new Rect(Screen.width*0.775f, Screen.height*0.84f, Screen.width*0.2f, Screen.height*0.1f), mDeckCodeOutput, 117);
			if(GUI.Button(new Rect(Screen.width*0.625f, Screen.height*0.84f, Screen.width*0.15f, Screen.height*0.1f), "Generate code for this deck:"))
			{
				GenerateDeckIDCode();
			}
			//End of Buttons and Text Fields for saving/loading decks via ID Codes

		}

    }//END of OnGUI()

	//Send the deck info from the DeckBuilder to the InformationManager that will actually go to the game scene
	void SendDeckToInformationManager()
	{
		mInformationManager.mDeck.Clear();
		mInformationManager.mDeck.Add(mVoidBreacher[0]);
		foreach (GameObject card in mFamiliars)
		{
			mInformationManager.mDeck.Add(card);
		}
		foreach (GameObject card in mActionCards)
		{
			mInformationManager.mDeck.Add(card);
		}
		
		if (mInformationManager.mDeck.Count == 13)
		{
			mNetworkManager.mShowLobby = true;
			mStillSelecting = false;
		}
	}//END of SendDeckToInformationManager

	//Take in a string and build a deck from it
	void BuildDeckFromIDCode()
	{
		//create an array of strings by splitting up the ID code and taking out the hyphens
		string[] deckCardCodes = mDeckCodeInput.Split(new Char [] {'-'});
		//Clear the existing deck
		mFamiliars.Clear();
		mActionCards.Clear();

		//Only have codes edit the Voidbreacher and deck type if you are in the lobby scene
		if(mDeckBuilderCurrentScene == DeckBuilderScene.Lobby)
		{
			mVoidBreacher.Clear();
			mPhase = CardTypePicking.VoidBreacher;

			//find the Voidbreacher in the deck code, add it to the deck, and set the deck type to match
			for (int i = 0; i < deckCardCodes.Length; i++)
			{
				foreach(GameObject availableCard in mAllAvailableCards)
				{
					if (availableCard.GetComponent<Card>().mCardIDCode == deckCardCodes[i] && availableCard.GetComponent<Card>().cardType == Card.CardType.VoidBreacher)
					{
						if (mVoidBreacher.Count == 0)
						{
							mVoidBreacher.Add(availableCard);
							mDeckType = availableCard.GetComponent<Card>().cardDominion;
						}
					}
				}
			}
		}
		//Add the rest of the cards if you have a Void breacher
		if (mVoidBreacher.Count == 1)
		{
			//Find the rest of the cards in the deck code
			for (int i = 0; i < deckCardCodes.Length; i++)
			{
				foreach(GameObject availableCard in mAllAvailableCards)
				{
					//Add Familiars
					if (availableCard.GetComponent<Card>().mCardIDCode == deckCardCodes[i] && availableCard.GetComponent<Card>().cardType == Card.CardType.Familiar)
					{
						if (mFamiliars.Count < 6 && mDeckType == availableCard.GetComponent<Card>().cardDominion)
						{
							mFamiliars.Add(availableCard);
						}
					}
					//Add Action Cards
					else if (availableCard.GetComponent<Card>().mCardIDCode == deckCardCodes[i] && availableCard.GetComponent<Card>().cardType != Card.CardType.VoidBreacher)
					{
						if (mActionCards.Count < 6 && mDeckType == availableCard.GetComponent<Card>().cardDominion)
						{
							mActionCards.Add(availableCard);
						}
					}
				}
			}
		}
	}//END of BuildDeckFromIDCode()

	//Read the current deck and output a string that can be saved and pasted in later
	void GenerateDeckIDCode()
	{
		//reset the output code
		mDeckCodeOutput = "";


		if(mVoidBreacher.Count > 0)
		{
			mDeckCodeOutput += mVoidBreacher[0].GetComponent<Card>().mCardIDCode + "-";
		}
		if(mFamiliars.Count > 0)
		{
			for (int i = 0; i < mFamiliars.Count; i ++)
			{
				mDeckCodeOutput += mFamiliars[i].GetComponent<Card>().mCardIDCode + "-";
			}
		}
		if(mActionCards.Count > 0)
		{
			for (int i = 0; i < mActionCards.Count; i ++)
			{
				mDeckCodeOutput += mActionCards[i].GetComponent<Card>().mCardIDCode + "-";
			}
		}
	}//END of GenerateDeckIDCode()
}
