﻿using UnityEngine;
using System.Collections;

public class BoardCreation : MonoBehaviour
{
    private bool boardHasInitialized = false;
    public bool squaresDeleted = false;
    private bool neighborsAssigned = false;
    private bool firstStepInitiated = false;
    private bool runThroughUpdateOnce = false;
    [SerializeField]
    Transform[] tilePrefabs;
    [SerializeField]
    Vector3[] tilePositions;
    TurnPhaseManager turnManager;
    Assets.CustomScripts.NetworkManager networkManager;
    // Use this for initialization
    void Start()
    {
        turnManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
        networkManager = FindObjectOfType<Assets.CustomScripts.NetworkManager>();
    }

    [RPC]
    public void StartBoardCreation()
    {
		//Giving every square on the board a unique name that is consistent on both sides of the network ~Adam
		int squareCounter = 0;
		BoardSquare[] boardSquaresToName = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
		foreach (BoardSquare squareToName in boardSquaresToName)
		{
			squareToName.gameObject.name += (" " + squareCounter.ToString());
			squareCounter++;
		}

        int corridorTilePosition = networkManager.mCorridorTilePosition;
        Debug.Log(corridorTilePosition);
        int juncitonTilePosition = networkManager.mJunctionTilePosition;
        Debug.Log(juncitonTilePosition);


        for (int i = 0; i < tilePositions.Length; i++)
        {
            float tileYRotation = networkManager.mTileRotations[i];
            //Debug.Log(tileYRotation);
            Quaternion tileRotation = Quaternion.identity;
            tileRotation.eulerAngles = new Vector3(0, tileYRotation, 0);
            if (i == corridorTilePosition)
            {
                Instantiate(tilePrefabs[0], tilePositions[i], tileRotation);
            }
            else if (i == juncitonTilePosition)
            {
                Instantiate(tilePrefabs[1], tilePositions[i], tileRotation);
            }
            else
            {
                Instantiate(tilePrefabs[2], tilePositions[i], tileRotation);
            }

        }

        firstStepInitiated = true;
    }

    void OnGUI()
    {
		//if (!firstStepInitiated)
		//{
		//	if (GUI.Button(new Rect(100, 100, 250, 100), "Start Board Creation"))
		//	{
		//		//StartBoardCreation();
		//		networkView.RPC("StartBoardCreation", RPCMode.All);
		//	}
		//}
        
    }

    // Update is called once per frame
    void Update()
    {
        if (firstStepInitiated)
        {
            if (runThroughUpdateOnce)
            {
                if (!boardHasInitialized)
                {
                    if (squaresDeleted && neighborsAssigned)
                    {
                        //We're done intializing the board
                        boardHasInitialized = true;
                    }
                    if (squaresDeleted && !neighborsAssigned)
                    {
                        //Set the neighbors for each of the remaining squares
                        BoardSquare[] remainingSquares = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
                        foreach (BoardSquare extantSquare in remainingSquares)
                        {
                            extantSquare.FindNeighbors();
                        }
                        neighborsAssigned = true;
                    }
                    if (!squaresDeleted)
                    {
                       //delete any unused board squares
                        BoardSquare[] boardSquares = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
                        foreach (BoardSquare square in boardSquares)
                        {
                            square.DeleteIfUnused();
                        }
//                        //delete the initilization tiles
//                        GameObject[] tilesToDelete = GameObject.FindGameObjectsWithTag("BoardTile");
//                        foreach (GameObject tile in tilesToDelete)
//                        {
//                           // Destroy(tile);
//                        }
//                        squaresDeleted = true;
                    }
                }
            }
            else
            {
                runThroughUpdateOnce = true;
            }
            
        }
        
    }//END of Update()

}
