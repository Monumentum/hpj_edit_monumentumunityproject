﻿using UnityEngine;
using System.Collections;

namespace Assets.CustomScripts
{
    public class NetworkManager : MonoBehaviour
    {

        private const string typeName = "Monumentum";
        private const string gameName = "Monumentum Room";

        public GameObject mPlayerPrefab;
        public GameObject mVoidBreacher;
        public bool mMapRandomized = false;
        public bool mShowLobby = false;

        private HostData[] hostList;

        public int mCorridorTilePosition;
        public int mJunctionTilePosition;
        public float[] mTileRotations = new float[14];
     //   public bool mInLobby = true;

        public InformationManager mInfoManager;

		//For keeping track of what scene we are currently in
		public enum NetworkManagerScene{Lobby, Matchup, Game}; 
		public NetworkManagerScene mNetManCurrentScene;

		//For instantiating the room manager in the Matchup scene
		[SerializeField] private GameObject mMatchupManager;
		public bool mFoundMatchupManager = false;

		//For making sure both players are ready before starting
		public bool mReadyToStartMatch = false;

        // Use this for initialization
        void Start()
        {
			mReadyToStartMatch = false;

			if (mInfoManager == null)
			{
				mInfoManager = FindObjectOfType(typeof(InformationManager)) as InformationManager;
			}
        }

        // Update is called once per frame
        void Update()
        {
			if (Application.loadedLevelName == "CardPurchaseMenu")
			{
				Destroy(transform.gameObject);
			}

        }

        void Awake()
        {
			DontDestroyOnLoad(transform.gameObject);
			mReadyToStartMatch = false;


        }

        private void StartServer()
        {
            //Network.InitializeServer(4, 25000, !Network.HavePublicAddress());
            Network.InitializeServer(2, 26543, !Network.HavePublicAddress());
            MasterServer.RegisterHost(typeName, gameName);
        }

        void OnServerInitialized()
        {
            //mInLobby = false;
			mNetManCurrentScene = NetworkManagerScene.Matchup;
            Debug.Log("Server Initializied");
           // Network.isMessageQueueRunning = false;
            Application.LoadLevel("Matchup");
            //SpawnPlayer();
        }

        private void SpawnPlayer()
        {
            Network.Instantiate(mPlayerPrefab, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
        }

        void OnGUI()
        {
            if (mShowLobby)
            {
                if (!Network.isClient && !Network.isServer)
                {
                    if (GUI.Button(new Rect(100, 100, 250, 100), "Start Server"))
                    {
                        StartServer();
                    }


                    if (GUI.Button(new Rect(100, 250, 250, 100), "Refresh Hosts"))
                    {
                        RefreshHostList();
                    }

                    if (hostList != null)
                    {
                        for (int i = 0; i < hostList.Length; i++)
                        {
                            if (GUI.Button(new Rect(400, 100 + (110 * i), 300, 100), hostList[i].gameName))
							{
                                JoinServer(hostList[i]);
							}
                        }
                    }


                }

//				if (mNetManCurrentScene == NetworkManagerScene.Matchup)
//				{
//					if (GUI.Button(new Rect(400, 210, 300, 100), "Lock In Deck"))
//					{
//						mReadyToStartMatch = true;
//					}
//
//				}

				//else
				//{
				//	if (!mMapRandomized)
				//	{
				//		if (GUI.Button(new Rect(400, 400, 250, 100), "Randomize Board"))
				//		{
				//			RandomizeLocations();
				//			this.networkView.RPC("SetMapSettings", RPCMode.All, mCorridorTilePosition, mJunctionTilePosition, mTileRotations);
				//		}
				//	}

				//}
            }
            
            
        }

        private void RefreshHostList()
        {
            MasterServer.RequestHostList(typeName);
        }

        void OnMasterServerEvent(MasterServerEvent msEvent)
        {
            if (msEvent == MasterServerEvent.HostListReceived)
                hostList = MasterServer.PollHostList();
        }

        private void JoinServer(HostData hostData)
        {
            Network.Connect(hostData);
        }

        void OnConnectedToServer()
        {
           // mInLobby = false;
			mNetManCurrentScene = NetworkManagerScene.Matchup;
			Debug.Log("Server Joined");
          //  Network.isMessageQueueRunning = false;
			Application.LoadLevel("Matchup");
            //SpawnPlayer();
        }

		public void StartMatch()
		{
			MatchupRoomManager roomMan = FindObjectOfType(typeof(MatchupRoomManager)) as MatchupRoomManager;
			if (roomMan != null)
			{
				Destroy(roomMan.gameObject);
			}
			mNetManCurrentScene = NetworkManagerScene.Game;
			Application.LoadLevel("GridScene");

		}

        void OnLevelWasLoaded()
        {
			mFoundMatchupManager = false;

			if (mNetManCurrentScene == NetworkManagerScene.Matchup)
			{
				Network.isMessageQueueRunning = true;

				if (Network.isClient)
				{
					mFoundMatchupManager = true;
					GameObject roomManager = (GameObject)Network.Instantiate(mMatchupManager, transform.position, transform.rotation, 0);
					Debug.Log("Instantiating matchup manager " + roomManager.GetComponent<NetworkView>().viewID.ToString());
//					NetworkViewID roomManagerViewID = Network.AllocateViewID();
//					//roomManagerViewID = Network.AllocateViewID();
//					Debug.Log("Room Manger ID: " + roomManagerViewID.ToString());
//					roomManager.networkView.viewID = roomManagerViewID;
					roomManager.GetComponent<NetworkView>().RPC("UpdatePlayerCount", RPCMode.AllBuffered);

					DeckBuilder myDeck = GameObject.Find("DeckBuilder").GetComponent<DeckBuilder>();
					myDeck.CallToUpdateVoidBreacherNames();

				}

			}

			if (mNetManCurrentScene == NetworkManagerScene.Game)
            {
                Network.isMessageQueueRunning = true;
                mInfoManager.LoadDeck();
                SpawnPlayer();
            }
        }

		//For finding the Matchup Room Manager as the Server
		public void FindMatchupRoomManager()
		{
			if(!mFoundMatchupManager)
			{
				MatchupRoomManager roomManager = FindObjectOfType(typeof(MatchupRoomManager)) as MatchupRoomManager;
				roomManager.GetComponent<NetworkView>().RPC("UpdatePlayerCount", RPCMode.All);
				
				DeckBuilder myDeck = GameObject.Find("DeckBuilder").GetComponent<DeckBuilder>();
				myDeck.CallToUpdateVoidBreacherNames();
				mFoundMatchupManager = true;
			}
		}

        public void RandomizeLocations()
        {
            mCorridorTilePosition = Mathf.RoundToInt(Random.Range(0f, 13f));
            mJunctionTilePosition = Mathf.RoundToInt(Random.Range(0f, 13f));

            while (mCorridorTilePosition == mJunctionTilePosition)
            {
                mJunctionTilePosition = Mathf.RoundToInt(Random.Range(0f, 14f));
            }

            for (int i = 0; i < mTileRotations.Length; i++)
            {
                mTileRotations[i] = Mathf.RoundToInt(Random.Range(0f, 3f)) * 90f;
            }

        }

        //[RPC]
        //void LoadLevel()
        //{
        //    Application.LoadLevel("GridScene");
        //}

        [RPC]
        void SetMapSettings(int corridor, int junction, float[] tileRotations)
        {
            this.mCorridorTilePosition = corridor;
            this.mJunctionTilePosition = junction;
            for (int i = 0; i < mTileRotations.Length; i++)
            {
                mTileRotations[i] = tileRotations[i];
            }
            mMapRandomized = true;
        }

        [RPC]
        public void ReturnToLobby()
        {
           // mInLobby = true;
			mNetManCurrentScene = NetworkManagerScene.Lobby;

            mShowLobby = false;
            if (Network.isServer)
            {
                GetComponent<NetworkView>().RPC("GoToLobby", RPCMode.Others);
            }
            Network.Disconnect();
            MasterServer.UnregisterHost();
            MasterServer.ClearHostList();
			Network.isMessageQueueRunning = false;
            Application.LoadLevel("Lobby");
			Destroy(this.gameObject);
            //if (Network.isClient)
            //{
            //    Application.LoadLevel("Lobby");
            //    Network.CloseConnection(Network.connections[0], true);
            //}
            //else if(Network.isServer)
            //{
            //    foreach (NetworkPlayer player in Network.connections)
            //    {
            //        Network.CloseConnection(player, true);
            //        Application.LoadLevel("Lobby");
            //    }
            //}
        }

        [RPC]
        public void GoToLobby()
        {
           // mInLobby = true;
			mNetManCurrentScene = NetworkManagerScene.Lobby;

            mShowLobby = false;
            mInfoManager.mDeck.Clear();
            Network.Disconnect();
            MasterServer.UnregisterHost();
            MasterServer.ClearHostList();
            Application.LoadLevel("Lobby");
			Destroy(this.gameObject);

        }

        void OnDisconnectedFromServer(NetworkDisconnection info)
        {
            //if (Network.isServer)
            //{
            //    Debug.Log("Local server connection disconnected");
            //}
            //else
            //{
            //    if (info == NetworkDisconnection.LostConnection)
            //    {
            //        Debug.Log("Lost connection to the server");
            //    } 
            //    else
            //    {
            //        Debug.Log("Successfully diconnected from the server");
            //    }
            //    GoToLobby(); 
            //}
                
        }
    }

}
