﻿using UnityEngine;
using System.Collections;

public class MouseOverInfoManager : MonoBehaviour 
{
	//for characterhpj tooltip info
	public bool mShowCharacterhpjInfoTooltip = false;
	public string mCharacterhpjTooltipInfo = "";

	public bool mShowMonumentInfoTooltip = false;
	public string mMonumentTooltipInfo = "";

	public bool mShowTrapInfoTooltip = false;
	public string mTrapTooltipInfo = "";

	public GUIStyle mMouseOverStyle;
	public GUIStyle mCardDisplayStyle;

	//The image of the card we display
	public Texture2D mCardImage;
	bool mExpandCardImage = false;
	//The images we display to overlay to show changes in Strength/Fortitude, there should be two items in each of these arrays
	[SerializeField] private Texture2D[] mStrengthOverlay;
	[SerializeField] private Texture2D[] mFortitudeOverlay;
	//Ints telling us to show a bonus, penalty, or default to Strength/Fortitude
	public int mStrengthStatus = 0;
	public int mFortitudeStatus = 0;
	//Ints for what the characterhpj's current Strength/Fortitude is
	public int mCurrentStrength = 0;
	public int mCurrentFortitude = 0;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//mTooltipXPos = Input.mousePosition.x)
		if(Input.GetKeyDown("tab"))
		{
			mExpandCardImage = !mExpandCardImage;
		}
	}

	void OnGUI ()
	{
		if (mShowCharacterhpjInfoTooltip)
		{
			GUI.Box(new Rect(Input.mousePosition.x+Screen.width*0.01f, Screen.height-Input.mousePosition.y, Screen.width*0.2f, Screen.height*0.2f), mCharacterhpjTooltipInfo, mMouseOverStyle);
		}
		if (mShowMonumentInfoTooltip)
		{
			GUI.Box(new Rect(Input.mousePosition.x+Screen.width*0.01f, Screen.height-Input.mousePosition.y, Screen.width*0.2f, Screen.height*0.2f), mMonumentTooltipInfo, mMouseOverStyle);
		}
		if (mShowTrapInfoTooltip)
		{
			GUI.Box(new Rect(Input.mousePosition.x+Screen.width*0.01f, Screen.height-Input.mousePosition.y, Screen.width*0.2f, Screen.height*0.2f), mTrapTooltipInfo, mMouseOverStyle);
		}

		//Display the card of the last thing you had your mouse over in the top left corner of the screen in one of two possible sizes
		if(mCardImage != null)
		{
			if(!mExpandCardImage)
			{
				GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.01f,Screen.width*0.1f,Screen.width*0.15f), mCardImage);
				GUI.Box(new Rect(Screen.width*0.89f,Screen.height*.01f+Screen.width*0.15f, Screen.width*0.1f, Screen.height*0.05f), "Press TAB key to toggle card size.");
				DisplayCardOverlays();
			}
			else
			{
				GUI.Label(new Rect(Screen.width*0.69f,Screen.height*0.01f,Screen.width*0.2f,Screen.width*0.3f), mCardImage);
				GUI.Box(new Rect(Screen.width*0.69f,Screen.height*.01f+Screen.width*0.3f, Screen.width*0.2f, Screen.height*0.05f), "Press TAB key to toggle card size.");
				DisplayCardOverlays();
			}
		}

	}

	public void SetCharacterhpjToolTip(bool shouldShow, string infoToShow)
	{
		mShowCharacterhpjInfoTooltip = shouldShow;
		mCharacterhpjTooltipInfo = infoToShow;
	}

	public void SetMonumentToolTip(bool shouldShow, string infoToShow)
	{
		mShowMonumentInfoTooltip = shouldShow;
		mMonumentTooltipInfo = infoToShow;
	}

	public void SetTrapToolTip(bool shouldShow, string infoToShow)
	{
		mShowTrapInfoTooltip = shouldShow;
		mTrapTooltipInfo = infoToShow;
	}

	void DisplayCardOverlays()
	{
		//Small card overlays
		if(!mExpandCardImage)
		{
			mCardDisplayStyle.fontSize = Mathf.RoundToInt(Screen.width*0.01f);
			switch(mStrengthStatus)
			{
			case 1:
				GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.01f,Screen.width*0.1f,Screen.width*0.15f), mStrengthOverlay[0]);
				GUI.Label(new Rect(Screen.width*0.9075f,Screen.width*0.1275f,Screen.width*0.1f,Screen.width*0.1f), mCurrentStrength.ToString(), mCardDisplayStyle);
				break;
			case 2:
				GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.01f,Screen.width*0.1f,Screen.width*0.15f), mStrengthOverlay[1]);
				GUI.Label(new Rect(Screen.width*0.9075f,Screen.width*0.1275f,Screen.width*0.1f,Screen.width*0.1f), mCurrentStrength.ToString(), mCardDisplayStyle);
				break;
			default:
				break;
			}
			
			switch(mFortitudeStatus)
			{
			case 1:
				GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.01f,Screen.width*0.1f,Screen.width*0.15f), mFortitudeOverlay[0]);
				GUI.Label(new Rect(Screen.width*0.965f,Screen.width*0.1275f,Screen.width*0.1f,Screen.width*0.1f), mCurrentFortitude.ToString(), mCardDisplayStyle);
				break;
			case 2:
				GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.01f,Screen.width*0.1f,Screen.width*0.15f), mFortitudeOverlay[1]);
				GUI.Label(new Rect(Screen.width*0.965f,Screen.width*0.1275f,Screen.width*0.1f,Screen.width*0.1f), mCurrentFortitude.ToString(), mCardDisplayStyle);
				break;
			default:
				break;
			}
		}
		//Expanded card overlays
		else
		{
			mCardDisplayStyle.fontSize = Mathf.RoundToInt(Screen.width*0.015f);
			switch(mStrengthStatus)
			{
			case 1:
				GUI.Label(new Rect(Screen.width*0.69f,Screen.height*0.01f,Screen.width*0.2f,Screen.width*0.3f), mStrengthOverlay[0]);
				GUI.Label(new Rect(Screen.width*0.725f,Screen.width*0.251f,Screen.width*0.01f,Screen.width*0.01f), mCurrentStrength.ToString(), mCardDisplayStyle);
				break;
			case 2:
				GUI.Label(new Rect(Screen.width*0.69f,Screen.height*0.01f,Screen.width*0.2f,Screen.width*0.3f), mStrengthOverlay[1]);
				GUI.Label(new Rect(Screen.width*0.725f,Screen.width*0.251f,Screen.width*0.01f,Screen.width*0.01f), mCurrentStrength.ToString(), mCardDisplayStyle);
				break;
			default:
				break;
			}
			
			switch(mFortitudeStatus)
			{
			case 1:
				GUI.Label(new Rect(Screen.width*0.69f,Screen.height*0.01f,Screen.width*0.2f,Screen.width*0.3f), mFortitudeOverlay[0]);
				GUI.Label(new Rect(Screen.width*0.844f,Screen.width*0.251f,Screen.width*0.1f,Screen.width*0.1f), mCurrentFortitude.ToString(), mCardDisplayStyle);
				break;
			case 2:
				GUI.Label(new Rect(Screen.width*0.69f,Screen.height*0.01f,Screen.width*0.2f,Screen.width*0.3f), mFortitudeOverlay[1]);
				GUI.Label(new Rect(Screen.width*0.844f,Screen.width*0.251f,Screen.width*0.1f,Screen.width*0.1f), mCurrentFortitude.ToString(), mCardDisplayStyle);
				break;
			default:
				break;
			}
		}

	}//END of DisplayCardOverlays()

}
