﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This script, along with PlayerHandManager serve as the two primary components of the PlayerController prefab
//This script handles all the input from the player clicking on non-GUI objects in the scene, 
		//from summoning characterhpjs, to telling them to move, to declaring targets for attacks or card effects

public class PlayerClickController : MonoBehaviour 
{
	public int playerNumber;//which player this is
	public Characterhpj selectedCharacterhpj;
	public Characterhpj mVoidBreacher;//This player's VoidBreacher (like a main commander unit)
	public Characterhpj mLastSelectedCharacterhpj;
	TurnPhaseManager turnManager;
	[HideInInspector] public PlayerHandManager handManager;
	int selectedCardInHand;
	public int sourceStoneCount = 12;//Resource used for playing cards/summoning characterhpjs
	public bool mouseOverGUI = false;//Whether or not the mouse is hovering over a GUI element and should display tooltip text
	public int mRemainingMonuments = 3;
	public bool mGameOver = false;
	private List<Characterhpj> mSwappingCharacterhpjs = new List<Characterhpj>();//For a type of effect that swaps the position of two characterhpjs on the board
	public bool mCanSwap = false;
	public BoardSquare mClickedSquare;
	public Assets.CustomScripts.NetworkManager mNetworkManager;

	public bool mAnimationPlaying = false; //When a characterhpj is doing an action, like moving between spaces, don't let the player click on things or interfere
	public BoardCreation mBoardCreator;

	[SerializeField] private GameObject gameObjectToSummon; //Used for placing characterhpjs and traps onto the board

	bool hasSummonedVoidBreacher = false; //for keeping the player from exiting the Prepartion Phase without summoning their VoidBreacher

	//for whether or not to show the GUI for selecting which characterhpj in a shared space to move
	bool showLimboSelectGUI = false;


	//For setting the camera position
	[SerializeField] private Vector3 mPlayerOneCameraPos;
	[SerializeField] private Vector3 mPlayerOneCameraRot;
	[SerializeField] private Vector3 mPlayerTwoCameraPos;
	[SerializeField] private Vector3 mPlayerTwoCameraRot;


	// Use this for initialization
	void Start () 
	{
		//Finding other objects that are going to be referenced
		mNetworkManager = GameObject.Find("NetworkManager").GetComponent<Assets.CustomScripts.NetworkManager>();
		handManager = GetComponent<PlayerHandManager>();
		turnManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
		turnManager.UpdatePlayerCount(this);
		mBoardCreator = FindObjectOfType(typeof(BoardCreation)) as BoardCreation;


	}
	
	// Update is called once per frame
	void Update () 
	{
		if (mRemainingMonuments <= 1)
		{
			GetComponent<NetworkView>().RPC("RevealWinner", RPCMode.All);
		}
		//only do stuff on your turn
		if (turnManager.playerTurn == playerNumber && this.GetComponent<NetworkView>().isMine)
		{
			//clicking to select things
			if (Input.GetMouseButtonDown(0) && !mouseOverGUI && !mAnimationPlaying)
			{
				showLimboSelectGUI = false;
				//Raycasting from the camera to see what you clicked on
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				if (Physics.Raycast(ray, out hit))
				{
					//What to do if the player clicks on a Characterhpj
					if (hit.collider.GetComponent<Characterhpj>() != null)
					{
						Debug.Log("Clicked a characterhpj");
						//What to do if the player clicks on a Characterhpj they own
						if (hit.collider.GetComponent<Characterhpj>().controllingPlayer == playerNumber)
						{

							DeselectCharacterhpj();//Deselect whatever you were selecting already
							hit.collider.GetComponent<Characterhpj>().BeClickedOn();//select your characterhpj
							selectedCharacterhpj = hit.collider.GetComponent<Characterhpj>();

							//what to do if they clicked on that characterhpj as a part of an Action Card
							if(handManager.playingActionCard)
							{
								//Set that characterhpj as the origin for the action card's effect if you were on that step of playing the card
								if (selectedCharacterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange)
								{
									handManager.actionCardToPlay.UnTargetAllSpaces();
									handManager.actionCardToPlay.CardEffect();

								}
							}
							else if(handManager.mTargetting)
							{
								//Set that characterhpj as the target of the action card's effect if you were on that step of playing the card
								if (selectedCharacterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange || selectedCharacterhpj.currentSpace.GetComponent<BoardSquare>().hasAttackableTarget)
								{
									mLastSelectedCharacterhpj = selectedCharacterhpj;
									handManager.actionCardToPlay.UnTargetAllSpaces();
									Debug.Log("Clicked a characterhpj you own");
									handManager.actionCardToPlay.TargetAcquired();
								}
							}

							//what to do for clicking on that characterhpj outside of playing an action card
							else
							{
								//What to do during the Preparation Phase
								if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation)
								{
									
								}
								//What to do during the Summoning Phase
								if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
								{
									if (gameObjectToSummon != null)
									{
										//Familiar type characterhpjs are attached to VoidBreachers for summoning
										if (gameObjectToSummon.tag == "Familiar")
										{
											if (hit.collider.tag == "VoidBreacher")
											{
												//Tell the VoidBreacher that it is now holding a characterhpj to summon
												hit.collider.GetComponent<Characterhpj>().summonToLimbo(gameObjectToSummon, selectedCardInHand);
												//Pay the summoned characterhpj's cost
												sourceStoneCount -= gameObjectToSummon.GetComponent<Characterhpj>().sourceStoneCost;
												//Set it so that you are no longer preparing to summon a characterhpj
												gameObjectToSummon = null;
												//Put the game object for the summoned characterhpj's card onto the table
												handManager.deck[selectedCardInHand].cardState = Card.CardState.Table;
												//Unhighlight spaces
												Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
												foreach (Characterhpj characterhpj in characterhpjs)
												{
													if (characterhpj.controllingPlayer == this.playerNumber)
													{
														characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
													}
												}
											}
										}
										//Minion type characterhpjs are attached to Familiars for summoning
										else if (gameObjectToSummon.tag == "Minion")
										{
											if (hit.collider.tag == "Familiar")
											{
												//Tell the Familiar that it is now holding a characterhpj to summon
												hit.collider.GetComponent<Characterhpj>().summonToLimbo(gameObjectToSummon, selectedCardInHand);
												//Pay the summoned characterhpj's cost
												sourceStoneCount -= gameObjectToSummon.GetComponent<Characterhpj>().sourceStoneCost;
												//Set it so that you are no longer preparing to summon a characterhpj
												gameObjectToSummon = null;
												//Put the game object for the summoned characterhpj's card onto the table
												handManager.deck[selectedCardInHand].cardState = Card.CardState.Table;
												//Unhighlight spaces
												Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
												foreach (Characterhpj characterhpj in characterhpjs)
												{
													if (characterhpj.controllingPlayer == this.playerNumber)
													{
														characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
													}
												}
											}
										}

									}
								}//end of Summoning Phase

								//what to do with your characterhpj duing the Movement Phase
								if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
								{
									//if the clicked characterhpj isn't carrying any characterhpjs in Limbo for summoning, just move it
									if (selectedCharacterhpj.carriedLimboCharacterhpjs.Count == 0)
									{
										selectedCharacterhpj.HighlightMovableSquares();
									}
									//otherwise, select which characterhpj you want to move
									else
									{
										showLimboSelectGUI = true;
									}
								}//end of MovementPhase

								//What to do during the Combat Phase
								if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
								{
									//Highlight the squares conntaining targets the clicked characterhpj can attack
									selectedCharacterhpj.HighlightCombatSquares();
								}

								//What to do during the Conclusion Phase
								if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
								{
									//for action cards that allow movement during the conclusion phase
									if (selectedCharacterhpj.canMove && !mCanSwap)
									{
										selectedCharacterhpj.HighlightMovableSquares();
									}

									#region This segment of code was written by my partner on the project to take into account a specific creaturehpj's ability to swap the positions of characterhpj on the board
									//For Ripples position swap ability
									//Could be re-used if we introduce other cards that swap the poisitions of characterhpjs
	                                if (selectedCharacterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange && mCanSwap)
	                                {
	                                    if (mSwappingCharacterhpjs.Contains(selectedCharacterhpj))
	                                    {
	                                        mSwappingCharacterhpjs.Remove(selectedCharacterhpj);
	                                    }
	                                    else
	                                    {
	                                        mSwappingCharacterhpjs.Add(selectedCharacterhpj);
	                                        if ((mSwappingCharacterhpjs.Count == 2) && GetComponent<NetworkView>().isMine)
	                                        {
	                                            GetComponent<NetworkView>().RPC("SwapCreaturehpjs", RPCMode.All, mSwappingCharacterhpjs[0].GetComponent<NetworkView>().viewID, mSwappingCharacterhpjs[1].GetComponent<NetworkView>().viewID);
	                                            mSwappingCharacterhpjs.Clear();
	                                            Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
	                                            foreach (Characterhpj characterhpj in characterhpjs)
	                                            {
	                                                if (characterhpj.controllingPlayer == this.playerNumber)
	                                                {
	                                                    characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
	                                                }
	                                            }
	                                            
	                                        }
	                                    }
	                                }
									#endregion
								}
							}
						}

						//What to do if the player clicks on a Characterhpj that they don't own
						else
						{
							mLastSelectedCharacterhpj = hit.collider.GetComponent<Characterhpj>();
							//what to do if they clicked on that characterhpj as a part of an Action Card
							if (handManager.playingActionCard)
							{
								if (mLastSelectedCharacterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange)
								{
									//Set that characterhpj as the origin of the action card's effect if you were on that step of playing the card
									handManager.actionCardToPlay.UnTargetAllSpaces();
									handManager.actionCardToPlay.CardEffect();
								}
							}
							else if (handManager.mTargetting)
							{
								if (mLastSelectedCharacterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange || mLastSelectedCharacterhpj.currentSpace.GetComponent<BoardSquare>().hasAttackableTarget)
								{
									//Set that characterhpj as the target of the action card's effect if you were on that step of playing the card
									handManager.actionCardToPlay.UnTargetAllSpaces();
									Debug.Log("Clicked a characterhpj you don't own");
									handManager.actionCardToPlay.TargetAcquired();
								}
							}
							//What to do during the Preparation Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation)
							{
								
							}
							//What to do during the Summoning Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
							{
								
							}
							//what to do with the enemy characterhpj duing the Movement Phase
							if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
							{

							}
							//What to do during the Combat Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
							{
								//If you have one of your own characterhpjs selected and the characterhpj you just clicked on is in range, attack it
								if (selectedCharacterhpj != null)
								{
									if (selectedCharacterhpj.canAttack && hit.collider.GetComponent<Characterhpj>().currentSpace.GetComponent<BoardSquare>().hasAttackableTarget)
									{
										selectedCharacterhpj.AttackTarget(hit.collider.GetComponent<Characterhpj>());
									}
									else if (selectedCharacterhpj.mCanAttackMonument && selectedCharacterhpj.canAttack && hit.collider.GetComponent<Monument>() && !selectedCharacterhpj.lockedInCombat)
									{
										Debug.Log("Telling " + selectedCharacterhpj.name + " to attack " + hit.collider.name); 
										selectedCharacterhpj.GetComponent<NetworkView>().RPC("AttackMonument", RPCMode.All);
									}
								}
							}
							//What to do during the Conclusion Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
							{
								
							}

						}
					}//End of Clicking on Characterhpjs


					//What to do if the player clicks on a Monument
					if (hit.collider.GetComponent<Monument>() != null)
					{
						//What to do if the player clicks on a Monument they own
						if (hit.collider.GetComponent<Monument>().mOwner == playerNumber)
						{

							//What to do during the Preparation Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation)
							{
							}//END of Preparation Phase
							//What to do during the Summoning Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
							{
							}//END of Summoning Phase
							
							//what to do with your monument duing the Movement Phase
							if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
							{
							}//END of MovementPhase
							
							//What to do during the Combat Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
							{
							}//END of CombatPhase
							
							//What to do during the Conclusion Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
							{
							}
						}
						//What to do if the player clicks on a Monument that they don't own
						else
						{
							//What to do during the Preparation Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation)
							{
							}
							//What to do during the Summoning Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
							{
							}
							//what to do with the enemy monument duing the Movement Phase
							if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
							{
							}
							//What to do during the Combat Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
							{
								//If you have one of your characterhpjs selected and it's in range to attack the monument, then have it attack that monument
								if (selectedCharacterhpj != null)
								{
									if (selectedCharacterhpj.mCanAttackMonument && selectedCharacterhpj.canAttack && hit.collider.GetComponent<Monument>()==selectedCharacterhpj.mAttackableMonument)
									{
										Debug.Log("Telling " + selectedCharacterhpj.name + " to attack " + hit.collider.name); 
										selectedCharacterhpj.GetComponent<NetworkView>().RPC("AttackMonument", RPCMode.All);
									}
								}
							}
							//What to do during the Conclusion Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
							{
								
							}
							
						}
					}//End of Clicking on Monuments

					//What to do if the player clicks on a Square
					if (hit.collider.GetComponent<BoardSquare>() != null)
					{
						mClickedSquare = hit.collider.GetComponent<BoardSquare>();
						BoardSquare clickedSquare = hit.collider.GetComponent<BoardSquare>();
						//For when you click on a high-lighted square while playing an action card
						if (handManager.playingActionCard)
						{
							if (clickedSquare.hasAttackableTarget || clickedSquare.inMovementRange)
							{
								handManager.actionCardToPlay.UnTargetAllSpaces();
								if(clickedSquare.occupier.GetComponent<Characterhpj>() != null)
								{
									mLastSelectedCharacterhpj = clickedSquare.occupier.GetComponent<Characterhpj>();
									selectedCharacterhpj = mLastSelectedCharacterhpj;
								}

								handManager.actionCardToPlay.CardEffect();
							}
						}
						else if (handManager.mTargetting)
						{
							if (clickedSquare.hasAttackableTarget || clickedSquare.inMovementRange)
							{
								handManager.actionCardToPlay.UnTargetAllSpaces();
								mClickedSquare = clickedSquare;
								if(clickedSquare.occupier.GetComponent<Characterhpj>() != null)
								{
									mLastSelectedCharacterhpj = clickedSquare.occupier.GetComponent<Characterhpj>();
								}
								Debug.Log("Clicked a Square");
								handManager.actionCardToPlay.TargetAcquired();
							}
						}
						else if (handManager.mActivatingActionCard)
						{
							if (clickedSquare.hasAttackableTarget || clickedSquare.inMovementRange)
							{
								handManager.actionCardToPlay.UnTargetAllSpaces();
								mClickedSquare = clickedSquare;
								handManager.actionCardToPlay.ActivateOnTarget();
							}
						}

						//What to do during the Preparation Phase
						if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation)
						{
							//summon your VoidBreacher on the First Turn
							if (clickedSquare.playerStartSpot == playerNumber && playerNumber == turnManager.playerTurn && clickedSquare.voidBreacherStartSpot == true)
							{
								Card myVoidBreacherCard = handManager.deck[0];
								int voidBreacherCardNumber =0;
								//find the VoidBreacher in your deck
								for (int i=0; i < handManager.deck.Count; i++)
								{
									if (handManager.deck[i].cardType == Card.CardType.VoidBreacher)
									{
										myVoidBreacherCard = handManager.deck[i];
										voidBreacherCardNumber =  i;
									}
								}
								if (myVoidBreacherCard.cardState == Card.CardState.Hand)
								{
									SummonVoidBreacher(voidBreacherCardNumber, hit.collider.transform.position);
									//this.networkView.RPC("SummonVoidBreacher", RPCMode.All, voidBreacherCardNumber, hit.collider.transform.position);

								}
							}
						}
						//What to do during the Summoning Phase
						if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
						{
							
						}
						//What to do during the Movement Phase
						if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
						{
							if (selectedCharacterhpj)
							{
								//Telling your characterhpj that you have selected to move from one space to another
								if (selectedCharacterhpj.canMove && hit.collider.GetComponent<BoardSquare>().inMovementRange)
								{
									SetCharacterhpjMovePath(mClickedSquare, selectedCharacterhpj);
									selectedCharacterhpj.MoveToSpace();
									selectedCharacterhpj.mCanAttackMonument = false;
									selectedCharacterhpj.mAttackableMonument = null;
									selectedCharacterhpj.canMove = false;
									selectedCharacterhpj.BeUnclicked();
									//If the characterhpj you just moved had the Elusive property, unlock it from combat with other characterhpjs
									if (selectedCharacterhpj.mElusive)
									{
										foreach(Characterhpj combatRival in selectedCharacterhpj.mCombatLockTargets)
										{
											combatRival.GetComponent<NetworkView>().RPC("RemoveFromCombatLock", RPCMode.All, selectedCharacterhpj.GetComponent<NetworkView>().viewID);
										}
										selectedCharacterhpj.GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
									}
								}
							}
							//Tell a characterhpj that you're getting ready to summon to appear on the board and move out of its summoner's space
							if (gameObjectToSummon)
							{
								if (hit.collider.GetComponent<BoardSquare>().validSummonSpot)
								{
									SummonLimboCharacterhpj(hit.collider.transform.position);
								}
							}
						}

						//What to do during the Combat Phase
						if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
						{
							//for moving characterhpjs with the victory rush effect (for right now just the Minotaur), which lets them move a space after defeating another characterhpj
							if (selectedCharacterhpj) 
							{
								if (selectedCharacterhpj.canMove && hit.collider.GetComponent<BoardSquare>().inMovementRange)
								{
									SetCharacterhpjMovePath(mClickedSquare, selectedCharacterhpj);
									selectedCharacterhpj.MoveToSpace();
									selectedCharacterhpj.canMove = false;
									selectedCharacterhpj.BeUnclicked();
								}
							}

						}
						//What to do during the Conclusion Phase
						if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
						{
							//For cards that let you move during the Conclusion Phase
							if (selectedCharacterhpj)
							{
								if (selectedCharacterhpj.canMove && hit.collider.GetComponent<BoardSquare>().inMovementRange)
								{
									selectedCharacterhpj.transform.position = hit.collider.transform.position + new Vector3(0,0.5f,0);
									selectedCharacterhpj.mCanAttackMonument = false;
									selectedCharacterhpj.mAttackableMonument = null;
									selectedCharacterhpj.canMove = false;
									selectedCharacterhpj.BeUnclicked();
									if (selectedCharacterhpj.mElusive)
									{
										foreach(Characterhpj combatRival in selectedCharacterhpj.mCombatLockTargets)
										{
											combatRival.GetComponent<NetworkView>().RPC("RemoveFromCombatLock", RPCMode.All, selectedCharacterhpj.GetComponent<NetworkView>().viewID);
										}
										selectedCharacterhpj.GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
									}
								}
							}
						}
					}
				}
			}



		}
	}//End of Update()

	void OnGUI()
	{
		#region Segement of code for ending the game and going back to the title screen was written by my partner on the project
        if (mGameOver) //Game over stuff
        {
            if (mRemainingMonuments <= 1)
            {
                GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 50, 50), "You Lose");
            }
            else
            {
                GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 50, 50), "You Win");
            }

            if (GUI.Button(new Rect(Screen.width * 0.5f, Screen.height * 0.75f, Screen.width * 0.10f, Screen.height * 0.10f), new GUIContent("Return Players to Lobby")))
            {
                Debug.Log("This should be hitting");
                mNetworkManager.GetComponent<NetworkView>().RPC("ReturnToLobby", RPCMode.All);
            }
        }//end of game over stuff
		#endregion
		//only draw the GUI on my turn and when nothing is animating
		if (turnManager.playerTurn == playerNumber && this.GetComponent<NetworkView>().isMine && !mAnimationPlaying)
		{
			//Have a button to change to next phase if you're not in the middle of playing an action card
			if (!handManager.playingActionCard && !handManager.mTargetting  &&!handManager.mActivatingActionCard)
			{
				//Button to go to the next phase if not on the Conclusion Phase
				if (turnManager.currentPhase != TurnPhaseManager.TurnPhase.Conclusion)
				{
					if (hasSummonedVoidBreacher == true)
					{
						if (GUI.Button(new Rect(Screen.width * 0.9f, Screen.height * 0.9f, Screen.width * 0.1f, Screen.height * 0.1f), "Next Phase"))
						{
							//AdvancePhase
							turnManager.GetComponent<NetworkView>().RPC("AdvancePhase", RPCMode.All);
							if (!mBoardCreator.squaresDeleted)
							{
									//delete any unused board squares at the beginning of the match after tiles have been randomized
									BoardSquare[] boardSquares = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
									foreach (BoardSquare square in boardSquares)
									{
										square.DeleteIfUnused();
									}
									//delete the initilization tiles
									GameObject[] tilesToDelete = GameObject.FindGameObjectsWithTag("BoardTile");
									foreach (GameObject tile in tilesToDelete)
									{
										Destroy(tile);
									}
									mBoardCreator.squaresDeleted = true;

							}
							//Unhighlight spaces
							Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
							foreach (Characterhpj characterhpj in characterhpjs)
							{
								if (characterhpj.controllingPlayer == this.playerNumber)
								{
									characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
								}
							}
						}
					}
				}
				//Button to end Turn if on the Conclusion Phase
				else
				{
					if (GUI.Button(new Rect(Screen.width * 0.9f, Screen.height * 0.9f, Screen.width * 0.1f, Screen.height * 0.1f), "End Turn"))
					{
						//AdvancePhase
						turnManager.GetComponent<NetworkView>().RPC("AdvancePhase", RPCMode.All);
						//Unhighlight spaces
						Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
						foreach (Characterhpj characterhpj in characterhpjs)
						{
							if (characterhpj.controllingPlayer == this.playerNumber)
							{
								characterhpj.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
							}
						}
					}
				}
			}
			//Display GUI asking to resolve your Action Card before continuing to the next phase
			else
			{
				GUI.Box(new Rect(Screen.width * 0.9f, Screen.height * 0.9f, Screen.width * 0.1f, Screen.height * 0.1f), "Please Finish Your Action");
			}

			//draw the GUI to select which characterhpj in a shared space to move
			{
				if(showLimboSelectGUI)
				{
					if (selectedCharacterhpj.canMove)
					{
						//The button for moving the ceature already on the board
						if(GUI.Button(new Rect(Screen.width*0.5f, Screen.height*0.3f, Screen.width*0.1f, Screen.height*0.1f), new GUIContent(selectedCharacterhpj.characterhpjName, "Move this characterhpj")))
						{
							selectedCharacterhpj.HighlightMovableSquares();
							showLimboSelectGUI = false;
						}
					}
					//The buttons for the characterhpjs that the selected characterhpj is carrying
					for(int i = 0; i < selectedCharacterhpj.carriedLimboCharacterhpjs.Count; i++)
					{
						if(GUI.Button(new Rect(Screen.width*0.61f+Screen.width*0.11f*i, Screen.height*0.3f, Screen.width*0.1f, Screen.height*0.1f),  new GUIContent(selectedCharacterhpj.carriedLimboCharacterhpjs[i].GetComponent<Characterhpj>().characterhpjName, "Summon a characterhpj")))
						{
							gameObjectToSummon = selectedCharacterhpj.carriedLimboCharacterhpjs[i];
							selectedCharacterhpj.limboCharacterhpjLookingAt = i;
							selectedCharacterhpj.HighlightSummonSquares(gameObjectToSummon.GetComponent<Characterhpj>());
							showLimboSelectGUI = false;
						}
					}
				}
			}

		}
		//making it so mouse doesn't click the board when over a button
		if (Event.current.type == EventType.Repaint)
		{
			//Mouse over a button
			if (GUI.tooltip != "")
			{
				mouseOverGUI = true;
			}
			//Mouse not over a button
			else
			{
				mouseOverGUI = false;
			}//END of making it so mouse doesn't click the board when over a button

		}
	}//end of OnGUIO()


	void DeselectCharacterhpj()
	{
		if(selectedCharacterhpj != null)
		{
			selectedCharacterhpj.BeUnclicked();
			//selectedCharacterhpj = null;
		}
	}//End of DeselectCharacterhpj()

	public void RefillSourceStones()
	{
		sourceStoneCount = 12;
	}//End of RefillSourceStones()
	

	#region Functions for summoning Familiars/Minions/Traps.  Written by me, but extensive enough to deserve a region
	public void ReadyGameObjectToSummon(GameObject summonedObject, int handCardNumber)
	{
		gameObjectToSummon = summonedObject;
		selectedCardInHand = handCardNumber;
	}//end of ReadyGameObjectToSummon()

	//RPC call to summon Voidbreacher
	[RPC] void SummonVoidBreacher(int vbCardNumber, Vector3 vbSummonSquare)
	{
		//summon the VoidBreacher
		GameObject summonedVoidBreacher = (GameObject)Network.Instantiate(handManager.deck[vbCardNumber].summonedGameObject, vbSummonSquare + new Vector3(0,0.5f,0), Quaternion.identity,0);
		summonedVoidBreacher.GetComponent<NetworkView>().RPC("SummonPlayerInfo", RPCMode.All, playerNumber, vbCardNumber, handManager.deck[vbCardNumber].cardName, handManager.deck[vbCardNumber].mCardDescription);
		handManager.deck[vbCardNumber].cardState = Card.CardState.Table;
		//unhighlight the voidbreacher summon squares
		this.GetComponent<NetworkView>().RPC("ClearVoidBreacherSummonSquares", RPCMode.All);
		hasSummonedVoidBreacher = true;
		if (GetComponent<NetworkView>().isMine)
		{
			mVoidBreacher = summonedVoidBreacher.GetComponent<Characterhpj>();
		}
		Debug.Log("Clearing summon squares");
	}
	//Unhighlight squares specially associated with VoidBreacher summoning
	[RPC] void ClearVoidBreacherSummonSquares ()
	{
		BoardSquare[] summonSquares  = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
		foreach (BoardSquare summonSquareToUnhighlight in summonSquares)
		{
			if (summonSquareToUnhighlight.playerStartSpot == turnManager.playerTurn)
			{
				summonSquareToUnhighlight.playerStartSpot=0;
//				Debug.Log("Summon Square cleared");
			}
		}
	}
	//Summon a characterhpj into the space of the characterhpj that's carrying it, then move it to the space you told it to go to
	[RPC] void SummonLimboCharacterhpj (Vector3 lcSummonSquarePos)
	{
		GameObject newSummon = (GameObject)Network.Instantiate(gameObjectToSummon, selectedCharacterhpj.transform.position, Quaternion.identity,0);
		//Assign the newlyy summoned characterhpj all it's referenced information, such as the card it's associated with and who owns it
		newSummon.GetComponent<NetworkView>().RPC("SummonPlayerInfo", RPCMode.All, playerNumber, selectedCharacterhpj.limboCharacterhpjHandCardNumber[selectedCharacterhpj.limboCharacterhpjLookingAt], 
		                          handManager.deck[selectedCharacterhpj.limboCharacterhpjHandCardNumber[selectedCharacterhpj.limboCharacterhpjLookingAt]].cardName, 
		                          handManager.deck[selectedCharacterhpj.limboCharacterhpjHandCardNumber[selectedCharacterhpj.limboCharacterhpjLookingAt]].mCardDescription);
		SetCharacterhpjMovePath(mClickedSquare, newSummon.GetComponent<Characterhpj>());
		//Let the characterhpj that was already on the board know that it's no longer carrying the characterhpj you just summoned
		selectedCharacterhpj.summonFromLimbo();
		selectedCharacterhpj.BeUnclicked();

		gameObjectToSummon = null;
	}//END of SumonLimboCharacterhpj ()
	#endregion

	#region functions involved in ending the game not written by me
	[RPC]
	void UpdateMonumentCount(string destroyedMonumentName)
	{
		Monument monumentToDestroy = GameObject.Find(destroyedMonumentName).GetComponent<Monument>();
		if (monumentToDestroy.mOwner == playerNumber)
		{
			mRemainingMonuments -= 1;
		}
		monumentToDestroy.mOccupiedSquare.occupier = null;
		Destroy(GameObject.Find(destroyedMonumentName).gameObject);
	}//END of UpdateMonumentCount()
	#endregion

	[RPC]
	void RevealWinner()
	{
		mGameOver = true;
	}//END of RevealWinner()

	#region Swap characterhpjs function not written by me
    [RPC]
    void SwapCreaturehpjs(NetworkViewID swap1, NetworkViewID swap2)
    {
        GameObject characterhpj1 = NetworkView.Find(swap1).observed.gameObject;
        GameObject characterhpj2 = NetworkView.Find(swap2).observed.gameObject;
        Vector3 temp1 = characterhpj1.transform.position;
        Vector3 temp2 = characterhpj2.transform.position;

        characterhpj1.transform.position = temp2;
        characterhpj2.transform.position = temp1;

        GameObject tempSquare = characterhpj1.GetComponent<Characterhpj>().currentSpace;
        characterhpj1.GetComponent<Characterhpj>().currentSpace = characterhpj2.GetComponent<Characterhpj>().currentSpace;
        characterhpj2.GetComponent<Characterhpj>().currentSpace = tempSquare;

        characterhpj1.GetComponent<Characterhpj>().currentSpace.GetComponent<BoardSquare>().occupier = characterhpj1;
        characterhpj2.GetComponent<Characterhpj>().currentSpace.GetComponent<BoardSquare>().occupier = characterhpj2;

		characterhpj1.GetComponent<Characterhpj>().BreakFromCombatLock();
		characterhpj2.GetComponent<Characterhpj>().BreakFromCombatLock();
	}//END of SwapCreaturehpjs
	#endregion

	//For having a characterhpj physically move accross the board
	public void SetCharacterhpjMovePath(BoardSquare destinationSquare, Characterhpj characterhpjToMove)
	{
		characterhpjToMove.mPathSquares.Clear();
		//Tell the characterhpj it's final destination
		characterhpjToMove.mPathSquares.Add(destinationSquare.gameObject);

		//Tell the characterhpj all the squares it needs to move to
		for (int i = destinationSquare.mDistanceFromMover; i>0; i--)
		{
			bool hasAddedToPath = false;
			foreach(BoardSquare neighbor in characterhpjToMove.mPathSquares[0].GetComponent<BoardSquare>().neighborSquares)
			{
				if (neighbor.mDistanceFromMover >= characterhpjToMove.mPathSquares[0].GetComponent<BoardSquare>().mDistanceFromMover)
				{
					neighbor.mDistanceFromMover = 99;
				}
				else if (hasAddedToPath)
				{
					neighbor.mDistanceFromMover = 99;
				}
				else
				{
					characterhpjToMove.mPathSquares.Insert(0,neighbor.gameObject);
					hasAddedToPath = true;
				}
			}
		}
	}//END of SetCharacterhpjMovePath

	//Make it so the players view the board from different angles, as if they were sitting on opposite sides of a physical board game
	public void SetCameraPos()
	{
		if (this.GetComponent<NetworkView>().isMine)
		{
			if(playerNumber == 1)
			{
				Camera.main.transform.position = mPlayerOneCameraPos;
				Camera.main.transform.rotation = Quaternion.Euler(mPlayerOneCameraRot);
				Debug.Log(this.name + GetComponent<NetworkView>().viewID + "is setting the main camera to the player 1 position.");
			}
			if(playerNumber == 2)
			{
				Camera.main.transform.position = mPlayerTwoCameraPos;
				Camera.main.transform.rotation = Quaternion.Euler(mPlayerTwoCameraRot);
				Debug.Log(this.name  + GetComponent<NetworkView>().viewID + "is setting the main camera to the player 2 position.");
			}
		}
	}//END of SetCameraPos

}
