﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Characterhpj : MonoBehaviour 
{
	//A master class that gets applied to anything capable of moving, including Familiars, Minions, AND Voidbreachers
	//Specific characterhpjs get their own scripts that inherit from this one and add/modify additional behaviors
	//Whether a characterhpj is counted as a Familiar, Minion, or Voidbreacher is determined by its GameObject's tag, which is set on its prefab in-editor

	//basic info
	public int controllingPlayer; //Who owns this characterhpj
	public enum CharacterhpjDominion{Irotar,Vosira,Rhavik}; //The characterhpj's faction
	public CharacterhpjDominion characterhpjDominion;
	public bool creaturehpj = false;
	public GameObject currentSpace;
	protected bool selectedByPlayer = false;
	public string characterhpjName;
	public int sourceStoneCost = 0;
	public PlayerHandManager handManager;
	public PlayerClickController clickManager;

	//Summoning Related Info
	public List<GameObject> carriedLimboCharacterhpjs = new List<GameObject>();//Have a list of characterhpjs summoned to Limbo sharing this space
	public List<int> limboCharacterhpjHandCardNumber = new List<int>();//Have a list of associated hand card numbers of  Limbo characterhpjs sharing this space
	public int handCardNumber = 0;
	public bool inLimbo = false;
	public int limboCharacterhpjLookingAt;

	//Movement related info
	public bool canMove = false;
    public int mTurnsStunned = 0;
	public int moveSpeed = 4; //The number of squares this characterhpj can move at once
	protected bool movementSquaresAreHighlighted = false;
	public bool lockedInCombat = false;
	public int crippled = 0;
    public bool mTemporarilyWeakened = false;
	public Vector3 mNavMeshDestination;
	public GameObject mDestinationSpace;
	public bool mIsMoving = false;
	public List<GameObject> mPathSquares;
	private Vector3 mOffsetFromSquare = new Vector3(0, 0.5f, 0);

	//Status effect info
    public List<string> mBoosts = new List<string>();
    public Dictionary<string, int> mTempBoosts = new Dictionary<string, int>();
    public List<string> mWeakeningBoosts = new List<string>();
    public List<Card> mCombatCards = new List<Card>();

	//Combat Related info
	public int strength = 0;
	public int fortitude = 0;
	public bool canAttack;
    public int mMaxAttacksATurn = 1;
    public int mNumberOfAttacksLeft = 1;
	public int attackRange = 1;
	public int hitPoints = 2;
	bool combatSquaresAreHighlighted = false;
	[HideInInspector] public int startingHitPoints = 2;
	public List<Characterhpj> mCombatLockTargets = new List<Characterhpj>();

    public bool mElusive = false;

	//monument related info
	public bool mCanAttackMonument = false;
	public Monument mAttackableMonument;

	#region the Rune mechanics were written by Sean Lambidn
	//Rune info
	public Dictionary<Rune.RuneType, List<Rune>> mActiveRunes = new Dictionary<Rune.RuneType,List<Rune>>();
    public int mNumberOfRunes = 0;

	protected TurnPhaseManager turnManager;
    public List<Rune> mAttackRunes = new List<Rune>();
    public List<Rune> mConclusionRunes = new List<Rune>();
    public List<Rune> mAttackedRunes = new List<Rune>();
    public List<Rune> mSummoningRunes = new List<Rune>();
    public List<Rune> mTriggerRunes = new List<Rune>();
    public List<Rune> mDamagedRunes = new List<Rune>();
	#endregion

	//Info for mouseover tooltip
	MouseOverInfoManager mMouseOverInfoManager;
	public string mCharacterhpjDescription = "";


	// Use this for initialization
	public virtual void Start () 
	{
		//Find the various controllers/managers that need referencing
		turnManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
		clickManager = turnManager.playerControllers[controllingPlayer-1];
		handManager = clickManager.gameObject.GetComponent<PlayerHandManager>();
		
		//For moving a characterhpj as it is summoned
		if (mPathSquares.Count>0)
		{
			MoveToSpace();
		}
       
		#region the Rune mechanics were written by Sean Lambidn
		//set up Rune lists
        mActiveRunes.Add(Rune.RuneType.Attacking, mAttackRunes);
        mActiveRunes.Add(Rune.RuneType.Conclusion, mConclusionRunes);
        mActiveRunes.Add(Rune.RuneType.IsAttacked, mAttackedRunes);
        mActiveRunes.Add(Rune.RuneType.Summoning, mSummoningRunes);
        mActiveRunes.Add(Rune.RuneType.OnTrigger, mTriggerRunes);
        mActiveRunes.Add(Rune.RuneType.IsDamaged, mDamagedRunes);
		#endregion

		//decide whether or not it is a "Creaturehpj" (meaning Familiar or Minion)
		if (gameObject.tag == "Familiar" || gameObject.tag == "Minion")
		{
			creaturehpj = true;
		}
		if (gameObject.tag == "VoidBreacher" || gameObject.tag == "Minion")
		{
			hitPoints = 1;
		}

		startingHitPoints = hitPoints;
		mMouseOverInfoManager = FindObjectOfType(typeof(MouseOverInfoManager)) as MouseOverInfoManager;

		#region Temporary code to make it possible to tell the difference between units while still using placeholder artwork
		//Changing color based on Dominion.  TAKE THIS OUT ONCE WE HAVE ART
		switch(characterhpjDominion)
		{
		case CharacterhpjDominion.Rhavik:
			this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
			if(tag == "VoidBreacher")
			{
				this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.magenta);
			}
			break;
		case CharacterhpjDominion.Irotar:
			this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
			if(tag == "VoidBreacher")
			{
				this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.cyan);
			}
			break;
		case CharacterhpjDominion.Vosira:
			this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
			if(tag == "VoidBreacher")
			{
				this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
			}
			break;
		default:
			break;
		}
		#endregion

	}//End of Start()
	
	// Update is called once per frame
	public virtual void Update () 
	{
		//For moving the characterhpj to the space you told it to go to
		if (mIsMoving)
		{

			if (Vector3.Distance(transform.position, mPathSquares[0].transform.position+mOffsetFromSquare) > 0.1f)
			{
				transform.position = Vector3.Lerp(transform.position, mPathSquares[0].transform.position+mOffsetFromSquare, 0.1f);

			}
			else if (Vector3.Distance(transform.position, mPathSquares[0].transform.position+mOffsetFromSquare) <= 0.1f  && mPathSquares.Count > 1)
			{
				mPathSquares[0].GetComponent<BoardSquare>().mDistanceFromMover = 99;
				mPathSquares.RemoveAt(0);
			}
			else if (Vector3.Distance(transform.position, mPathSquares[0].transform.position+mOffsetFromSquare) <= 0.1f  && mPathSquares.Count == 1)
			{
				LerpPathArrived();
			}
		}

	}//End of Update()

	//Providing a tooltip about the characterhpj when a player mouses over it
	void OnMouseEnter()
	{
		mMouseOverInfoManager.SetCharacterhpjToolTip(true, name+"\nHP: " + hitPoints + "/" + startingHitPoints 
		                                         + "\nStength: " + strength + "\nFortitude: " + fortitude 
		                                         + "\n Speed: " + moveSpeed + "\n\n" + mCharacterhpjDescription);
		mMouseOverInfoManager.mCardImage = handManager.deck[handCardNumber].mCardArt;
		//Send the current Strength/Fortitude
		mMouseOverInfoManager.mCurrentStrength = strength;
		mMouseOverInfoManager.mCurrentFortitude = fortitude;
		//Decide whether or not to use Strength overlays
		if(strength > GetComponent<StatusDisplay>().mBaseStrength)
		{
			mMouseOverInfoManager.mStrengthStatus = 1;
		}
		else if(strength < GetComponent<StatusDisplay>().mBaseStrength)
		{
			mMouseOverInfoManager.mStrengthStatus = 2;
		}
		else
		{
			mMouseOverInfoManager.mStrengthStatus = 0;
		}
		//Decide whether or not to use Fortitude overlays
		if(fortitude > GetComponent<StatusDisplay>().mBaseFortitude)
		{
			mMouseOverInfoManager.mFortitudeStatus = 1;
		}
		else if(fortitude < GetComponent<StatusDisplay>().mBaseFortitude)
		{
			mMouseOverInfoManager.mFortitudeStatus = 2;
		}
		else
		{
			mMouseOverInfoManager.mFortitudeStatus = 0;
		}
	}//END of OnMouseEnter()

	void OnMouseExit()
	{
		mMouseOverInfoManager.mStrengthStatus = 0;
		mMouseOverInfoManager.mFortitudeStatus = 0;
		mMouseOverInfoManager.SetCharacterhpjToolTip(false, "");		
	}//END of OnMouseExit()





	//for getting selected/deselected by the player
	public void BeClickedOn()
	{
		selectedByPlayer = true;
	}//End of BeClickedOn()

	public void BeUnclicked()
	{
		selectedByPlayer = false;
		if (movementSquaresAreHighlighted || combatSquaresAreHighlighted)
        {
            UnHighlightSquares();
        }
			
	}//End of BeUnclicked

	//Highlight squares you can move to when clicked during the Movmement Phase
	public void HighlightMovableSquares()
	{
		Debug.Log("Highlighting Squares");
		if (selectedByPlayer && canMove && !movementSquaresAreHighlighted)
		{
			int moveRange = moveSpeed;
			//check for crippled status
			switch (crippled)
			{
				case 0:
					moveRange = moveSpeed;
					break;
				case 1:
					moveRange = 2;
					break;
				case 2:
					moveRange = 0;
					break;
				default:
					moveRange = moveSpeed;
					break;
			}
			Debug.Log("I'm selected an ready to move");
			if (moveRange == 0)
			{
				canMove = false;
			}
			else
			{
				for (int i = 1; i <= moveRange; i++)
				{
					//highlight adjacent squares
					if (i == 1)
					{
						foreach(BoardSquare potentialMoveTarget in currentSpace.GetComponent<BoardSquare>().neighborSquares)
						{
							if (potentialMoveTarget.occupier == null || (potentialMoveTarget.occupier.GetComponent<Characterhpj>()!= null && potentialMoveTarget.occupier.GetComponent<Characterhpj>().controllingPlayer == controllingPlayer))
							{
								potentialMoveTarget.inMovementRange = true;
								potentialMoveTarget.mDistanceFromMover = i;
							}
						}
					}
					//Highlight other squares in range
					else
					{
						BoardSquare[] highlightedMoveTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
						List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
						foreach (BoardSquare newMoveTarget in highlightedMoveTargets)//Add any already highlighted squares to the list
						{
							if (newMoveTarget.inMovementRange == true)
							{
								highlightedSquareList.Add(newMoveTarget);
							}
							else
							{
								newMoveTarget.mDistanceFromMover = 99;
							}
						}
						foreach(BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
						{
							foreach(BoardSquare potentialMoveTarget in listedSqaure.neighborSquares)
							{
								if (potentialMoveTarget.occupier == null || (potentialMoveTarget.occupier.GetComponent<Characterhpj>()!= null && potentialMoveTarget.occupier.GetComponent<Characterhpj>().controllingPlayer == controllingPlayer))
								{
									potentialMoveTarget.inMovementRange = true;
									if (potentialMoveTarget.mDistanceFromMover == 99)
									{
										potentialMoveTarget.mDistanceFromMover = i;
									}
								}
							}
						}

					}
					//unhighlight occupied squares in range
					if (i == moveRange)
					{
						movementSquaresAreHighlighted = true;
						BoardSquare[] moveTargetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
						foreach (BoardSquare invalidMoveTarget in moveTargetToUnHighlight)//Unhighlight any squares that are Highlighted
						{
							if (invalidMoveTarget.occupier != null)
							{
								invalidMoveTarget.inMovementRange = false;
							}
						}
			

					}
				}
			}
		}
	} //End of HighlightMovableSquares()

	//Highlight squares characterhpjs carried by this characterhpj can be told to move to upon being summoned to the board
	public void HighlightSummonSquares(Characterhpj characterhpjToSummon)
	{
		Debug.Log("Highlighting Squares");
		if (selectedByPlayer && !movementSquaresAreHighlighted)
		{
			Debug.Log("I'm selected an ready to move");
			for (int i = 1; i <= characterhpjToSummon.moveSpeed; i++)
			{
				//highlight adjacent squares
				if (i == 1)
				{
					foreach(BoardSquare potentialSummonTarget in currentSpace.GetComponent<BoardSquare>().neighborSquares)
					{
						if (potentialSummonTarget.occupier == null || (potentialSummonTarget.occupier.GetComponent<Characterhpj>()!=null && potentialSummonTarget.occupier.GetComponent<Characterhpj>().controllingPlayer == controllingPlayer))
						{
							potentialSummonTarget.validSummonSpot = true;
							potentialSummonTarget.mDistanceFromMover = i;

						}
					}
				}
				//Highlight other squares in range
				else
				{
					BoardSquare[] highlightedSummonTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
					List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
					foreach (BoardSquare newSummonTarget in highlightedSummonTargets)//Add any already highlighted squares to the list
					{
						if (newSummonTarget.validSummonSpot == true)
						{
							highlightedSquareList.Add(newSummonTarget);
						}
						else
						{
							newSummonTarget.mDistanceFromMover = 99;
						}

					}
					foreach(BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
					{
						foreach(BoardSquare potentialSummonTarget in listedSqaure.neighborSquares)
						{
							if (potentialSummonTarget.occupier == null || (potentialSummonTarget.occupier.GetComponent<Characterhpj>()!=null && potentialSummonTarget.occupier.GetComponent<Characterhpj>().controllingPlayer == controllingPlayer))
							{
								potentialSummonTarget.validSummonSpot = true;
								if (potentialSummonTarget.mDistanceFromMover == 99)
								{
									potentialSummonTarget.mDistanceFromMover = i;
								}

							}
						}
					}
					
				}
				//unhighlight occupied squares in range
				if (i == characterhpjToSummon.moveSpeed)
				{
					movementSquaresAreHighlighted = true;
					BoardSquare[] summonTargetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
					foreach (BoardSquare invalidSummonTarget in summonTargetToUnHighlight)//Unhighlight any squares that are Highlighted
					{
						if (invalidSummonTarget.occupier != null)
						{
							invalidSummonTarget.validSummonSpot = false;
						}
					}


				}
			}
		}
	}//End of HighlightSummonSquares

	//Highlighting squares occupied by targets this characterhpj can attack
	public void HighlightCombatSquares()
	{
		Debug.Log("Highlighting Squares");
		if (selectedByPlayer && canAttack && !combatSquaresAreHighlighted)
		{
			Debug.Log("I'm selected an ready to move");
			for (int i = 1; i <= attackRange; i++)
			{
				//highlight adjacent squares
				if (i == 1)
				{
					foreach(BoardSquare potentialCombatTarget in currentSpace.GetComponent<BoardSquare>().neighborSquares)
					{
						potentialCombatTarget.hasAttackableTarget = true;
					}
				}
				//Highlight other squares in range
				else
				{
					BoardSquare[] highlightedCombatTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
					List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
					foreach (BoardSquare newCombatTarget in highlightedCombatTargets)//Add any already highlighted squares to the list
					{
						if (newCombatTarget.hasAttackableTarget == true)
						{
							highlightedSquareList.Add(newCombatTarget);
						}
					}
					foreach(BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
					{
						foreach(BoardSquare potentialCombatTarget in listedSqaure.neighborSquares)
						{
							if (listedSqaure.occupier == null || 
							    (listedSqaure.occupier.GetComponent<Characterhpj>() !=null && listedSqaure.occupier.GetComponent<Characterhpj>().controllingPlayer == controllingPlayer))
							{
                                if (Vector3.Distance(potentialCombatTarget.gameObject.transform.position, currentSpace.gameObject.transform.position) == i)
                                {
                                    potentialCombatTarget.hasAttackableTarget = true;
                                }
							}
						}
					}
					
				}
				//unhighlight unoccupied/friendly squares in range
				if (i == attackRange)
				{
					combatSquaresAreHighlighted = true;
					BoardSquare[] combatTargetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
                    foreach (BoardSquare invalidCombatTarget in combatTargetToUnHighlight)//Unhighlight any squares that are Highlighted
					{
                        if (invalidCombatTarget.occupier == null || (invalidCombatTarget.occupier.GetComponent<Characterhpj>() != null && invalidCombatTarget.occupier.GetComponent<Characterhpj>().controllingPlayer == controllingPlayer) 
                            || (lockedInCombat && invalidCombatTarget.occupier.GetComponent<Characterhpj>() != null && invalidCombatTarget.occupier.GetComponent<Characterhpj>().tag == "VoidBreacher"))
						{
                            invalidCombatTarget.hasAttackableTarget = false;
						}
					}
					
				}
			}
		}
	} //End of HighlightCombatSquares()

	//Function for attacking a target
	public virtual bool AttackTarget(Characterhpj targetedCharacterhpj, int numberOfRolls = 1)
	{
        if (mCombatCards.Count > 0)
        {
            foreach (Card card in mCombatCards)
            {
                card.BattleEffect(targetedCharacterhpj);
            }
        }
        List<Rune> attackRunes;
        if (mActiveRunes.TryGetValue(Rune.RuneType.Attacking, out attackRunes))
        {
            foreach (Rune rune in attackRunes)
            {
				Debug.Log("DOING THE ATTACKING RUNE " + rune.name);

                rune.ActivateRune(this);
            }
        }
        bool didIHit = false;
		//Roll 2d6 and add your Strength
		int thisAttackPower = strength;
        int rollBonus = 0;

        for (int i = 0; i < numberOfRolls; i++)
        {
            int dieRoll = 0;
            dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
            dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));

            if (dieRoll > rollBonus)
            {
                rollBonus = dieRoll;
            }
        }
        thisAttackPower += rollBonus;

		//Compare your results to the target's Fortitude
		if (targetedCharacterhpj.tag == "VoidBreacher")//Automatically hit a VoidBreacher
		{
			targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, GetComponent<NetworkView>().viewID, true);
            didIHit = true;
			Debug.Log(this.name + " hit " + targetedCharacterhpj.name + " leaving it with " + targetedCharacterhpj.hitPoints + " HP");
		}
		else if (thisAttackPower == strength+2)//Automatically miss if you roll a 1 on both dice
		{
			Debug.Log(this.name + " missed " + targetedCharacterhpj.name);
            targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, GetComponent<NetworkView>().viewID, false);
		}
		else if (thisAttackPower > targetedCharacterhpj.fortitude || thisAttackPower==strength+12) //hit if you roll a 6 on both dice and/or roll higher than the target's Fortitude
		{
			targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, GetComponent<NetworkView>().viewID, true);
            didIHit = true;
			Debug.Log(this.name + " hit " + targetedCharacterhpj.name + " leaving it with " + targetedCharacterhpj.hitPoints + " HP");
		}
		else//Miss if you roll lower than the target's Fortitude
		{
			Debug.Log(this.name + " missed " + targetedCharacterhpj.name);
            targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, GetComponent<NetworkView>().viewID, false);
		}

		//Done attacking now
        mNumberOfAttacksLeft--;
        if (mNumberOfAttacksLeft <= 0)
        {
            canAttack = false;
            UnHighlightSquares();
        }

        if (mCombatCards.Count > 0)
        {
            foreach (Card card in mCombatCards)
            {
                card.AfterBattleEffect();
            }
        }

		//if you took the target down to 0 HP, kill it and move into its space if it was adjacent
		if (targetedCharacterhpj.hitPoints == 0)
		{
            if (Vector3.Distance(this.transform.position, targetedCharacterhpj.transform.position) == 1)
            {
                //transform.position = targetedCharacterhpj.transform.position; 
				//Victory Rush into the target's vacated space
				mPathSquares.Clear();
				mPathSquares.Add(targetedCharacterhpj.currentSpace.gameObject);
				MoveToSpace();
                currentSpace.GetComponent<BoardSquare>().occupier = null;
                currentSpace = targetedCharacterhpj.currentSpace;
                foreach(Characterhpj combatRival in mCombatLockTargets)
                {
                    combatRival.GetComponent<NetworkView>().RPC("RemoveFromCombatLock", RPCMode.All, GetComponent<NetworkView>().viewID);
                }

                GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
            }
			targetedCharacterhpj.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All); //kill the target
			Debug.Log(this.name + " killed " + targetedCharacterhpj.name);
		}

        return didIHit;
	}//End of AttackTarget()

	[RPC] public virtual void TakeDamage(NetworkViewID whoHitMe, bool didHit = true)
	{
		if (whoHitMe != null && Vector3.Distance(this.transform.position, NetworkView.Find(whoHitMe).observed.transform.position) == 1)
        {
            List<Rune> attackedRunes;
            if (mActiveRunes.TryGetValue(Rune.RuneType.IsAttacked, out attackedRunes))
            {
                foreach (Rune rune in attackedRunes)
                {
					Debug.Log("DOING THE IS ATTACKED RUNE " + rune.name);
					rune.ActivateRune(NetworkView.Find(whoHitMe).observed.GetComponent<Characterhpj>());
                }
            }
            lockedInCombat = true;
            this.mCombatLockTargets.Add(NetworkView.Find(whoHitMe).observed.GetComponent<Characterhpj>());
            NetworkView.Find(whoHitMe).observed.GetComponent<Characterhpj>().lockedInCombat = true;
            NetworkView.Find(whoHitMe).observed.GetComponent<Characterhpj>().mCombatLockTargets.Add(this);
        }
        if (didHit)
        {
            List<Rune> damagedRunes;
            if (mActiveRunes.TryGetValue(Rune.RuneType.IsDamaged, out damagedRunes))
            {
                foreach (Rune rune in damagedRunes)
                {
					Debug.Log("DOING THE DAMAGED RUNE " + rune.name);
                    rune.ActivateRune(this);
                }
            }
            hitPoints--;
        }
	}

	//Function for removing a combat lock rival from this characterhpj's list
    [RPC] public void RemoveFromCombatLock(NetworkViewID combatRival)
    {
		if (NetworkView.Find(combatRival) != null && NetworkView.Find(combatRival).observed.GetComponent<Characterhpj>()!=null)
		{
	        Characterhpj removedRival = NetworkView.Find(combatRival).observed.GetComponent<Characterhpj>();
	        while (mCombatLockTargets.Contains(removedRival))
	        {
	            mCombatLockTargets.Remove(removedRival);
	        }
	        if (mCombatLockTargets.Count <= 0)
	        {
	            lockedInCombat = false;
				if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
				{
					canMove = true;
				}

	        }
		}
    }//END of RemoveFromCobatLock()

	//Function for leaving combat lock altogether, and removing itself from its rivals combat lock lists
    [RPC] public void BreakFromCombatLock()
    {
		Debug.Log("Breaking from Combat");
		foreach (Characterhpj combatRival in mCombatLockTargets)
		{
			Debug.Log("Removing myself from rival's combat lock");
			combatRival.RemoveFromCombatLock(this.GetComponent<NetworkView>().viewID);
		}

        mCombatLockTargets.Clear();
        lockedInCombat = false;
		if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
		{
			canMove = true;
		}
		Debug.Log("Done breaking from combat");
    }//END of BreakFromCombatLock()

	//Function for getting killed by being reduced to 0 Hitpoints
	[RPC] public virtual void BeKilled()
	{
		if (tag == "Familiar" || tag == "Minion"  && GetComponent<NetworkView>())
		{
            lockedInCombat = false;
			handManager.deck[handCardNumber].cardState = Card.CardState.Cooldown;
		}
        foreach (Characterhpj combatRival in mCombatLockTargets)
        {
            combatRival.RemoveFromCombatLock(this.GetComponent<NetworkView>().viewID);
        }
        foreach (List<Rune> runesList in mActiveRunes.Values)
        {
            for (int i = 0; i < runesList.Count; i++)
            {
                runesList[i].DestroyRune();
            }
        }
        if (tag == "VoidBreacher")
        {
            if (GetComponent<NetworkView>().isMine)
            {
                clickManager.mRemainingMonuments = 0;
            }
            clickManager.GetComponent<NetworkView>().RPC("RevealWinner", RPCMode.All);
        }
		//Make it so the game doesn't freeze up if the characterhpj dies while moving (like, from a trap)
		if(mIsMoving)
		{
			handManager.mAnimationPlaying = false;
			clickManager.mAnimationPlaying = false;
		}
        Destroy(this.gameObject);
	}//END of BeKilled()


	//Function for forcing a characterhpj to move (for things like Lava Chain) ~Adam
	[RPC] public void ForceMovement(string targetSpaceName)
	{
		mPathSquares.Clear();
		mPathSquares.Add(GameObject.Find(targetSpaceName));
		MoveToSpace();

	}

	public void UnHighlightSquares ()
	{
		BoardSquare[] squaresToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
		foreach (BoardSquare unHighlightTarget in squaresToUnHighlight)
		{
			unHighlightTarget.inMovementRange = false;
			unHighlightTarget.hasAttackableTarget = false;
			unHighlightTarget.validSummonSpot = false;
		}
		movementSquaresAreHighlighted = false;
		combatSquaresAreHighlighted = false;
	}//End of UnHighlightSquares()

	//Functions for beginning each phase

	public virtual void BeginPreparationPhase()
	{
		canMove = false;
        currentSpace.GetComponent<BoardSquare>().occupier = this.gameObject;
	}//End of BeginPreparationPhase
	public virtual void BeginSummoningPhase()
	{
        
	}//End of BeginSummoningPhase
	public virtual void BeginMovementPhase()
	{
		if (!lockedInCombat || mElusive)
		{
            if (mTurnsStunned <= 0)
            {
                canMove = true;
            }
		}
	}//End of BeginMovementPhase

	public virtual void BeginCombatPhase()
	{
		//Debug.Log(handManager.name);
		canMove = false;
		if (movementSquaresAreHighlighted)
		{
			UnHighlightSquares();
		}
		if (tag != "VoidBreacher" && mTurnsStunned <= 0)
		{
			canAttack = true;
		}

		//Refund any Familiars/Minions still in limbo (being carried) that didn't get summoned
		if (limboCharacterhpjHandCardNumber.Count > 0)
		{
			for (int i = 0; i < limboCharacterhpjHandCardNumber.Count; i++)
			{
				Debug.Log("Refunding unsummoned characterhpj");
				handManager.deck[limboCharacterhpjHandCardNumber[i]].cardState = Card.CardState.Hand;
				clickManager.sourceStoneCount += handManager.deck[limboCharacterhpjHandCardNumber[i]].sourceStoneCost;
			}

			carriedLimboCharacterhpjs.Clear();
			limboCharacterhpjHandCardNumber.Clear();
		}
	}//End of BeginCombatPhase

	public virtual void BeginConclusionPhase()
	{
        List<Rune> conclusionRunes;
        if (mActiveRunes.TryGetValue(Rune.RuneType.Conclusion, out conclusionRunes))
        {
            foreach (Rune rune in conclusionRunes)
            {
				Debug.Log("DOING THE CONCLUSION RUNE " + rune.name);

                rune.ActivateRune(this);
            }
        }
		//Clean up anything leftover from the combat phase
		if (combatSquaresAreHighlighted)
		{
			UnHighlightSquares();
		}
		canAttack = false;

		//Get rid of temporary status effects
        if (mTemporarilyWeakened)
        {
            mTemporarilyWeakened = false;
        }
        else
        {
            crippled = 0;
            int temporaryBoost = 0;
            foreach (string weakeningBoost in mWeakeningBoosts)
            {
                if (mTempBoosts.TryGetValue(weakeningBoost, out temporaryBoost))
                {
                    UpdateFortitude(-temporaryBoost);
                    GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, strength, fortitude);
                    mTempBoosts.Remove(weakeningBoost);
                }
            }
            mWeakeningBoosts.Clear();
        }
		//Reset the remaing attacks this characterhpj can do
        mNumberOfAttacksLeft = mMaxAttacksATurn;

		//Unstun the characterhpj
        if (mTurnsStunned > 0)
        {
            mTurnsStunned--;
            if (mTurnsStunned <= 0)
            {
                mTurnsStunned = 0;
                int temporaryBoost = 0;
                if (mTempBoosts.TryGetValue("Freezeboost", out temporaryBoost))
                {
                    UpdateFortitude(-temporaryBoost);
                    mTempBoosts.Remove("Freezeboost");
                    GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, strength, fortitude);
                }
            }
        }
	}//End of BeginConclusionPhase


	//Functions related to summoning characterhpjs from this space
	public void summonToLimbo (GameObject limboSummon, int limboCardNumber) //add to the list of characterhpj sharing this space in Limbo
	{
		carriedLimboCharacterhpjs.Add(limboSummon);
		limboCharacterhpjHandCardNumber.Add(limboCardNumber);
	}//End of summonToLimbo()

	public void summonFromLimbo()//Move a characterhpj out of Limbo and onto the board;
	{
		List<Rune> summonRunes;
		if (mActiveRunes.TryGetValue(Rune.RuneType.Summoning, out summonRunes))
		{
			foreach (Rune rune in summonRunes)
			{
				Debug.Log("DOING THE SUMMONING RUNE " + rune.name);

				rune.ActivateRune(this);
			}
		}
		carriedLimboCharacterhpjs.RemoveAt(limboCharacterhpjLookingAt);
		limboCharacterhpjHandCardNumber.RemoveAt(limboCharacterhpjLookingAt);
		UnHighlightSquares();
	}//End of summonFromLimbo()

	[RPC] public void SummonPlayerInfo(int summonerPlayerNumber, int summonerCardNumber, string properName, string description)
	{
		controllingPlayer = summonerPlayerNumber;
		handCardNumber = summonerCardNumber;
		name = properName;
		mCharacterhpjDescription = description;
	}//End of SummonPlayerInfo()

	[RPC]
	public void AttackMonument()
	{
		Debug.Log(name + " Attacked a monument");
		mAttackableMonument.mAttacksLeftToWithstand--;
		canAttack = false;
		if (mAttackableMonument.mAttacksLeftToWithstand <= 0)
		{
			mCanAttackMonument = false;
			mAttackableMonument = null;
		}
		UnHighlightSquares();
	}//End of AttackMonument ()

    [RPC] public void ModifyFortitude(int amount)
    {
        fortitude = fortitude + amount;
    }//End of ModifyFortitude()

    [RPC] public void ModifyStrength(int amount)
    {
        strength = strength + amount;
    }//End of ModifyFortitude()

	[RPC] public void GetCrippled()
	{
		crippled++;
	}//End of GetCrippled

    [RPC] public void SendStrengthandFortitude(int newStrength, int newFortitude)
    {
        strength = newStrength;
        fortitude = newFortitude;
    }//End of SendStrengthandFortitude

	//For making it so that the rune count will be right on both side for updating status icons,
	//but not double up on rune effects, which would happen if we actually applied runes on both sides ~Adam
	[RPC] public void AddRuneCount(int runeAmmount)
	{
		mNumberOfRunes += runeAmmount;
	}//END of AddRuneCount()

    public void UpdateStrength(int amount)
    {
        strength = strength + amount;
    }//End of UpdateStrength

    public void UpdateFortitude(int amount)
    {
		fortitude = fortitude + amount;
    }//End of UpdateFortitude

    [RPC] public void ReduceHealth(int amount)
    {
        hitPoints = hitPoints - amount;
        if (hitPoints <= 0)
        {
            BeKilled();
        }
    }//End of ReduceHealth

    [RPC] public void UpdatePosition(Vector3 Position)
    {
        transform.position = Position;
    }//End of UpdatePosition

    [RPC] public void BeFrozen(string freeze, int amount, int timer)
    {
        if (!mTempBoosts.ContainsKey(freeze) && mTurnsStunned <= 0 && GetComponent<NetworkView>().isMine)
        {
            UpdateFortitude(amount);
            mTempBoosts.Add(freeze, amount);
			canMove = false;
            GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, strength, fortitude);
            mTurnsStunned = timer;
        }
    }//End of BeFrozen

    [RPC] public void BeWeakened(string weakener, int amount, int timer)
    {
        if (!mTempBoosts.ContainsKey(weakener) && GetComponent<NetworkView>().isMine)
        {
            mTemporarilyWeakened = true;
            UpdateFortitude(amount);
            mTempBoosts.Add(weakener, amount);
            mWeakeningBoosts.Add(weakener);
            GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, strength, fortitude);
            crippled = timer;
        }
    }//End of BeWeakened

	public void MoveToSpace()
	{
		if(mElusive)
		{
			foreach(Characterhpj combatRival in mCombatLockTargets)
			{
				combatRival.GetComponent<NetworkView>().RPC("RemoveFromCombatLock", RPCMode.All, GetComponent<NetworkView>().viewID);
			}
			GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
		}
		mIsMoving = true;
		handManager.mAnimationPlaying = true;
		clickManager.mAnimationPlaying = true;
	}//End of MoveToSpace()

	//Called when a characterhpj reaches the end of its movement
	public void LerpPathArrived()
	{
		transform.position = mPathSquares[0].transform.position+mOffsetFromSquare;
		mPathSquares[0].GetComponent<BoardSquare>().mDistanceFromMover = 99;
		mIsMoving = false;
		handManager.mAnimationPlaying = false;
		clickManager.mAnimationPlaying = false;
	}//End of LerpPathArrived()
    
}
