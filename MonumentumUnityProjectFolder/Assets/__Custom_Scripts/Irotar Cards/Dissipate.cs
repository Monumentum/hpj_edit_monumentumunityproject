﻿using UnityEngine;
using System.Collections;


//All friendly characterhpjs 'break combat'.
public class Dissipate : Card 
{
	public TurnPhaseManager mTurnPhaseManager;

	// Use this for initialization
	void Start () 
	{
		cardName = "Dissipate";
		mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public override void CardEffect()
	{
		Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
		string boostName = cardName + "boost";
		
		foreach (Characterhpj characterhpj in characterhpjs)
		{
			if ((characterhpj.controllingPlayer == this.playerNumber) && characterhpj.creaturehpj)
			{
				if(characterhpj.lockedInCombat == true && mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
				{
					characterhpj.canMove = true;
				}
				characterhpj.GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
			}
		}
		
		
		
		//finish up the action and send this card to the Cooldown pile
		cardState = CardState.Cooldown;
		handManager.playingActionCard = false;
		handManager.mTargetting = false;
		handManager.actionCardToPlay = null;
		
	}
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		CardEffect();
	}
}
