﻿using UnityEngine;
using System.Collections;

//Target a creaturehpj within LOS(3) of a friendly creaturehpj, target gains +4F 
//but cannot move and cannot attack until the end of your next turn.
public class Freeze : Card 
{
    public int mBoostAmount = 4;
    public int mTurnsCharacterhpjWillBeStunned = 1;
    public int mTargetRange = 3;
    public Characterhpj mFrozenCharacterhpj;
	// Use this for initialization
	void Start () 
    {
        cardName = "Freeze";
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public override void CardEffect()
    {
        mTargetedCharacterhpj = handClickController.selectedCharacterhpj;
        TargetAllCreaturehpjsInLOS(mTargetRange);
		mTargetedCharacterhpj.currentSpace.GetComponent<BoardSquare>().hasAttackableTarget = true;
        handManager.mCurrentCard = this;
        //handManager.mTarget = mTargetedCharacterhpj;
        handManager.mTargetting = true;
        handManager.playingActionCard = false;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCreaturehpjs();
    }

    public override void TargetAcquired()
    {
        string boostName = cardName + "boost";
        mFrozenCharacterhpj = handClickController.mLastSelectedCharacterhpj;
        mFrozenCharacterhpj.GetComponent<NetworkView>().RPC("BeFrozen", RPCMode.All, boostName, mBoostAmount, mTurnsCharacterhpjWillBeStunned);
        handManager.mTargetting = false;
        handManager.actionCardToPlay = null;
        cardState = CardState.Cooldown;
    }
}
