﻿using UnityEngine;
using System.Collections;

//Target friendly creaturehpj gains +4S but is destroyed during your next conclusion phase.

public class Overflow : Card 
{
    public TurnPhaseManager mTurnPhaseManager;
    public int mBoostAmount = 4;

	// Use this for initialization
	void Start () 
    {
        mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
        cardName = "Overflow";
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (mTurnPhaseManager && cardState == CardState.Table)
        {
            if (mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
            {
                OnConclusionPhase();
            }
        }
	}

    public override void CardEffect()
    {
        mTargetedCharacterhpj = handClickController.selectedCharacterhpj;
        string boostName = cardName + "boost";

        if ((mTargetedCharacterhpj.controllingPlayer == this.playerNumber) && mTargetedCharacterhpj.creaturehpj)
        {
            mTargetedCharacterhpj.UpdateStrength(mBoostAmount);
            mTargetedCharacterhpj.mBoosts.Add(boostName);
            mTargetedCharacterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, mTargetedCharacterhpj.strength, mTargetedCharacterhpj.fortitude);
        }

        handManager.playingActionCard = false;
        handManager.actionCardToPlay = null;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCreaturehpjs();
    }

    public void OnConclusionPhase()
    {
        mTargetedCharacterhpj.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All);
        cardState = CardState.Cooldown;
    }
}
