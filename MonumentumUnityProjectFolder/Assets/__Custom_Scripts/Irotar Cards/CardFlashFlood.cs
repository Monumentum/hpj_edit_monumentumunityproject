﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//During your conclusion phase, all friendly creaturehpjs may move 1 square. This does not 'break combat'.

public class CardFlashFlood : Card 
{
	public TurnPhaseManager mTurnPhaseManager;

	public List<Characterhpj> mMyCharacterhpjs = new List<Characterhpj>();
	public List<int> mMyCharacterhpjSpeeds = new List<int>();


	[SerializeField] bool mHasLetCharacterhpjsMove = false;

	// Use this for initialization
	void Start () 
	{
		mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (mTurnPhaseManager && cardState == CardState.Table)
		{
			if (mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion && !mHasLetCharacterhpjsMove)
			{
				Debug.Log("Ready to do Flash Floods conclusion phase effect");
				OnConclusionPhase();
			}
		}
		if (mTurnPhaseManager && cardState == CardState.Table)
		{
			if (mTurnPhaseManager.playerTurn != playerNumber)
			{
				Debug.Log("Ready to do Flash Floods next turn effect");
				OnOpponentTurn();
			}
		}

	}

	public override void CardEffect()
	{
		Characterhpj[] characterhpjs = FindObjectsOfType<Characterhpj>();
		
		foreach (Characterhpj characterhpj in characterhpjs)
		{
			if ((characterhpj.controllingPlayer == this.playerNumber) && characterhpj.creaturehpj)
			{
				mMyCharacterhpjs.Add(characterhpj);
				mMyCharacterhpjSpeeds.Add(characterhpj.moveSpeed);
			}
		}

		//finish up the action and send this card to the Cooldown pile
		handManager.playingActionCard = false;
		handManager.mTargetting = false;
		handManager.actionCardToPlay = null;
		
	}
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		CardEffect();
	}
	
	public void OnConclusionPhase()
	{
		Debug.Log("Doing Flash Floods conclusion phase effect");

		for (int i = 0; i < mMyCharacterhpjs.Count; i++)
		{
			if (mMyCharacterhpjs[i] != null && !mMyCharacterhpjs[i].lockedInCombat)
			{
				Debug.Log("Letting the following characterhpj move: " + mMyCharacterhpjs[i].name);

				mMyCharacterhpjs[i].canMove = true;
				mMyCharacterhpjs[i].moveSpeed = 1;
			}
		}
		mHasLetCharacterhpjsMove = true;
	}

	public void OnOpponentTurn()
	{
		Debug.Log("Doing Flash Floods next turn effect");

		for (int i = 0; i < mMyCharacterhpjs.Count; i++)
		{
			if (mMyCharacterhpjs[i] != null)
			{
				Debug.Log("Reseting speed for the following characterhpj: " + mMyCharacterhpjs[i].name);
				if (mMyCharacterhpjSpeeds[i] != null)
				{
					mMyCharacterhpjs[i].moveSpeed = mMyCharacterhpjSpeeds[i];
				}
				else
				{
					mMyCharacterhpjs[i].moveSpeed = 4;
				}
				mMyCharacterhpjs[i].canMove = false;

			}
		}
		mMyCharacterhpjs.Clear();
		mMyCharacterhpjSpeeds.Clear();
		cardState = CardState.Cooldown;
		mHasLetCharacterhpjsMove = false;
	}


}
