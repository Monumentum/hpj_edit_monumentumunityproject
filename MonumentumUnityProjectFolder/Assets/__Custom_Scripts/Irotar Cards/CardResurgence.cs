﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Target creaturehpj gains +nS where 'n' is the number of minions you have in your cooldown and recharge piles.

public class CardResurgence : Card 
{
	int resurgenceBonus = 0;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public override void CardEffect()
	{
		resurgenceBonus = 0;
		Card[] minionCardList = FindObjectsOfType(typeof(Card)) as Card[];

		foreach (Card deadMinion in minionCardList)
		{
			if (deadMinion.cardType == CardType.Minion && deadMinion.playerNumber == playerNumber
			    &&(deadMinion.cardState == CardState.Cooldown || deadMinion.cardState == CardState.Recharge) )
			{
				resurgenceBonus++;
			}
		}

		handClickController.selectedCharacterhpj.UpdateStrength(resurgenceBonus);
		handClickController.selectedCharacterhpj.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.All, handClickController.selectedCharacterhpj.strength, handClickController.selectedCharacterhpj.fortitude);

		//finish up the action and send this card to the Cooldown pile
		handManager.playingActionCard = false;
		handManager.mTargetting = false;
		handManager.actionCardToPlay = null;
		cardState = CardState.Cooldown;

	}

	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCreaturehpjs();
	}


}
