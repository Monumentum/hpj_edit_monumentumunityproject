﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Target friendly creaturehpj, all enemy creaturehpjs within AOE(3) of target suffer a 6S attack. Target is destroyed during your next conclusion phase.

public class CardGeyser : Card 
{
	Characterhpj mVoidBreacher; 
	public TurnPhaseManager mTurnPhaseManager;

	// Use this for initialization
	void Start () 
	{
		mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (mTurnPhaseManager && cardState == CardState.Table)
		{
			if(mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
			{
				OnConclusionPhase();
			}
		}

	}


	public override void CardEffect()
	{
		//For keeping track of the characterhpj to kill it on the conclusion phase
		mTargetedCharacterhpj = handClickController.selectedCharacterhpj;



		Characterhpj[] voidBreachersInPlay = FindObjectsOfType(typeof(Characterhpj)) as Characterhpj[];
		foreach (Characterhpj voidBreacher in voidBreachersInPlay)
		{
			if (voidBreacher.tag == "VoidBreacher" && voidBreacher.controllingPlayer == playerNumber)
			{
				mVoidBreacher= voidBreacher;
			}
		}
		
		
		Debug.Log("Doing effect of " + cardName);
		Characterhpj originCharacterhpj = handClickController.selectedCharacterhpj;
		int attackRange =3;
		//find the targets of the AOE 
		
		List<Characterhpj> aoeTargets = new List<Characterhpj>();
		
		for (int i = 1; i <= attackRange; i++)
		{
			//highlight adjacent squares
			if (i == 1)
			{
				//Debug.Log("Highlighting the first set of squares");
				foreach(BoardSquare potentialCombatTarget in originCharacterhpj.currentSpace.GetComponent<BoardSquare>().neighborSquares)
				{
					potentialCombatTarget.hasAttackableTarget = true;
				}
			}
			//Highlight other squares in range
			else
			{
				//Debug.Log("Highlighting another set of squares");
				BoardSquare[] highlightedCombatTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
				foreach (BoardSquare newCombatTarget in highlightedCombatTargets)//Add any already highlighted squares to the list
				{
					if (newCombatTarget.hasAttackableTarget == true)
					{
						highlightedSquareList.Add(newCombatTarget);
					}
				}
				foreach(BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
				{
					foreach(BoardSquare potentialCombatTarget in listedSqaure.neighborSquares)
					{
						if (listedSqaure.occupier == null || listedSqaure.occupier.GetComponent<Characterhpj>() != null)
						{
							if (Vector3.Distance(potentialCombatTarget.gameObject.transform.position, originCharacterhpj.currentSpace.gameObject.transform.position) == i)
							{
								potentialCombatTarget.hasAttackableTarget = true;
							}
						}
					}
				}
				
			}
			//add enemy characterhpjs in range to the list and then unhighlight all the squares
			if (i == attackRange)
			{
				//Debug.Log("Done Highlighting squares");
				BoardSquare[] combatTargetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				foreach (BoardSquare aoeCombatTarget in combatTargetToUnHighlight)//Unhighlight any squares that are Highlighted
				{
					if (aoeCombatTarget.occupier != null && aoeCombatTarget.occupier.GetComponent<Characterhpj>() != null 
					    && aoeCombatTarget.occupier.GetComponent<Characterhpj>().controllingPlayer != playerNumber 
					    && aoeCombatTarget.occupier.GetComponent<Characterhpj>().tag != "VoidBreacher")
					{
						aoeTargets.Add(aoeCombatTarget.occupier.GetComponent<Characterhpj>());
					}
					aoeCombatTarget.hasAttackableTarget = false;
					
				}
				
			}
		}//End of finding the AOE targets
		
		
		//Attack the enemy characterhpjs in AOE range
		
		foreach(Characterhpj aoeTargetedCharacterhpj in aoeTargets)
		{
			Characterhpj targetedCharacterhpj = aoeTargetedCharacterhpj;
			bool didIHit = false;
			//Roll 2d6 and add your Strength
			int thisAttackPower = 6;
			int rollBonus = 0;
			
			for (int i = 0; i < 2; i++)
			{
				int dieRoll = 0;
				dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
				dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
				
				if (dieRoll > rollBonus)
				{
					rollBonus = dieRoll;
				}
			}
			thisAttackPower += rollBonus;
			
			//Compare your results to the target's Fortitude
			if (targetedCharacterhpj.tag == "VoidBreacher")//Automatically hit a VoidBreacher
			{
				//do nothing to a VoidBreacher
			}
			else if (thisAttackPower == 4+2)//Automatically miss if you roll a 1 on both dice
			{
				Debug.Log(this.name + " missed " + targetedCharacterhpj.name);
				targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, false);
			}
			else if (thisAttackPower > targetedCharacterhpj.fortitude || thisAttackPower==4+12) //hit if you roll a 6 on both dice and/or roll higher than the target's Fortitude
			{
				targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, true);
				didIHit = true;
				Debug.Log(this.name + " hit " + targetedCharacterhpj.name + " leaving it with " + targetedCharacterhpj.hitPoints + " HP");
			}
			else//Miss if you roll lower than the target's Fortitude
			{
				Debug.Log(this.name + " missed " + targetedCharacterhpj.name);
				targetedCharacterhpj.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, false);
			}
			
			//Done attacking now
			
			
			
			//if you took the target down to 0 HP, kill it and move into its space if it was adjacent
			if (targetedCharacterhpj.hitPoints == 0)
			{
				targetedCharacterhpj.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All); //kill the target
			}
			
		}//End of attacking the AOE targets
		
		//finish up the action and send this card to the Cooldown pile
		handManager.playingActionCard = false;
		handManager.mTargetting = false;
		handManager.actionCardToPlay = null;
	}
	
	
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCreaturehpjs();
	}


	public void OnConclusionPhase()
	{
		mTargetedCharacterhpj.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All);
		cardState = CardState.Cooldown;
	}

}
